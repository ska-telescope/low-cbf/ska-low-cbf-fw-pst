"""Configuration file for Sphinx."""
import os
import sys

sys.path.insert(0, os.path.abspath("../../src"))

# General information about the project.
project = u'SKA Low CBF Firmware PST'
copyright = u'2019-2024, David Humphrey, Giles Babich, Andrew Bolin, Grant Hampson'
author = u'David Humphrey, Giles Babich, Andrew Bolin, Grant Hampson'
# The short X.Y version.
version = u'1.0.0'
# The full version, including alpha/beta/rc tags.
release = u'1.0.0'

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx_rtd_theme",
]

templates_path = ["_templates"]
exclude_patterns = []

html_css_files = [
    "css/custom.css",
]

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]
html_logo = "_static/img/logo.png"
html_favicon = "_static/img/favicon_mono.ico"

html_theme_options = {"logo_only": True}


