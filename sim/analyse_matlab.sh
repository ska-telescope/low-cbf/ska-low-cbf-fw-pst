#!/bin/bash

# Verify simulation result
#
# Arguments:
# 1: if "false" run rtl_model_compare(0)
#    otherwise, run rtl_model_compare(1)

cd $CI_PROJECT_DIR

MY_DIR="$CI_PROJECT_DIR/common/scripts"
#shellcheck source=util.sh
source "$MY_DIR/util.sh"

if [[ -z "$EXTRA_MATLAB_MODEL_DIR" ]]; then
    2>&1 echo "Error: EXTRA_MATLAB_MODEL_DIR variable is required for matlab job"
    exit 2
fi

cd "$(envsubst <<< "${EXTRA_MATLAB_MODEL_DIR}")" || exit 2

# This needs to be paramterised against REPOs.
# currently setup for PST.
matlab -batch "create_config('run13',0,3)"
matlab -batch "create_config('run14',0,3)"

if (($# > 1)); then
   2>&1 echo "Error: number of arguments for matlab job should be either 0 or 1"
   exit 2
elif (($# == 1)); then
  if [ "$1" == "false" ]; then
     matlab -batch "rtl_model_compare(0)"
     EXIT_CODE=$?
     exit $EXIT_CODE
  else
     matlab -batch "rtl_model_compare(1)"
     EXIT_CODE=$?
     exit $EXIT_CODE
  fi	  
else
  matlab -batch "rtl_model_compare(1)"
  EXIT_CODE=$?
  exit $EXIT_CODE
fi
