-------------------------------------------------------------------------------
--
-- File Name: vcu128_gemini_dsp.vhd
-- Contributing Authors: Jason van Aardt
-- Type: RTL
-- Created: 27 October 2021
--
-- Title: Top Level for the Trafgen (Traffic Generator)
--
--
-------------------------------------------------------------------------------

LIBRARY IEEE, common_lib, axi4_lib, ct_lib, trafgen_lib;
library HBM_PktController_lib, LFAADecode100G_lib, timingcontrol_lib, capture128bit_lib, captureFine_lib, trafgen_lib, filterbanks_lib, interconnect_lib, bf_lib, PSR_Packetiser_lib;
use ct_lib.all;
use trafgen_lib.trafgen_top_pkg.all;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
USE common_lib.common_mem_pkg.ALL;
USE axi4_lib.axi4_lite_pkg.ALL;
USE axi4_lib.axi4_stream_pkg.ALL;
USE axi4_lib.axi4_full_pkg.ALL;

use PSR_Packetiser_lib.ethernet_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;

library technology_lib;
USE technology_lib.tech_mac_100g_pkg.ALL;

library xpm;
use xpm.vcomponents.all;

-------------------------------------------------------------------------------
entity trafgen_top is
    generic (
        -- Number of LFAA blocks per frame for the PSS/PST output.
        -- Each LFAA block is 2048 time samples. e.g. 27 for a 60 ms corner turn.
        -- This value needs to be a multiple of 3 so that there are a whole number of PST outputs per frame.
        -- Maximum value is 30, (limited by the 256MByte buffer size, which has to fit 1024 virtual channels)
        g_DEBUG_ILA                     : boolean := false;
        g_BEAM_ILA                      : boolean := false;
        g_LFAA_BLOCKS_PER_FRAME_DIV3    : integer := 9;  -- Number of LFAA blocks per frame divided by 3; minimum value is 1, i.e. 3 LFAA blocks per frame.
        g_PST_BEAMS                     : integer := 16;
        g_USE_META : boolean := FALSE  -- Put meta data into the memory in place of the actual data, to make it easier to find bugs in the corner turn. 
    );
    port (
        clk_freerun : in std_logic;
        -----------------------------------------------------------------------
        -- Received data from 100GE
        i_data_rx_sosi      : in t_lbus_sosi;
        -- Data to be transmitted on 100GE
        o_data_tx_sosi      : out t_lbus_sosi;
        i_data_tx_siso      : in t_lbus_siso;
        i_clk_100GE         : in std_logic;
        i_eth100G_locked    : in std_logic;
        -----------------------------------------------------------------------
        -- Other processing clocks.
        i_clk450 : in std_logic; -- 450 MHz
        i_clk400 : in std_logic; -- 400 MHz
        -----------------------------------------------------------------------
        -- Debug signal used in the testbench.
        o_validMemRstActive : out std_logic;  -- reset of the valid memory is in progress.
        -----------------------------------------------------------------------
        -- MACE AXI slave interfaces for modules
        -- The 300MHz MACE_clk is also used for some of the signal processing
        i_MACE_clk  : in std_logic;
        i_MACE_rst  : in std_logic;
        
        i_PSR_packetiser_Lite_axi_mosi : in t_axi4_lite_mosi; 
        o_PSR_packetiser_Lite_axi_miso : out t_axi4_lite_miso;
        
        i_PSR_packetiser_Full_axi_mosi : in  t_axi4_full_mosi;
        o_PSR_packetiser_Full_axi_miso : out t_axi4_full_miso;
  

        i_HBM_Pktcontroller_Lite_axi_mosi : in t_axi4_lite_mosi; 
        o_HBM_Pktcontroller_Lite_axi_miso : out t_axi4_lite_miso;
        
        i_HBM_Pktcontroller_Full_axi_mosi : in  t_axi4_full_mosi;
        o_HBM_Pktcontroller_Full_axi_miso : out t_axi4_full_miso;
        -----------------------------------------------------------------------
        -- AXI interfaces to shared memory
        -- Uses the same clock as MACE (300MHz)
        -----------------------------------------------------------------------
        --  Shared memory block for the first corner turn (at the output of the LFAA ingest block)
        -- Corner Turn between LFAA ingest and the filterbanks
        -- AXI4 master interface for accessing HBM for the LFAA ingest corner turn : m01_axi
        -- aw bus = write address
        m01_axi_awvalid  : out std_logic;
        m01_axi_awready  : in std_logic;
        m01_axi_awaddr   : out std_logic_vector(29 downto 0);
        m01_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m01_axi_wvalid    : out std_logic;
        m01_axi_wready    : in std_logic;
        m01_axi_wdata     : out std_logic_vector(511 downto 0);
        m01_axi_wlast     : out std_logic;
        -- b bus - write response
        m01_axi_bvalid    : in std_logic;
        m01_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m01_axi_arvalid   : out std_logic;
        m01_axi_arready   : in std_logic;
        m01_axi_araddr    : out std_logic_vector(29 downto 0);
        m01_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m01_axi_rvalid    : in std_logic;
        m01_axi_rready    : out std_logic;
        m01_axi_rdata     : in std_logic_vector(511 downto 0);
        m01_axi_rlast     : in std_logic;
        m01_axi_rresp     : in std_logic_vector(1 downto 0)
    );
END trafgen_top;

-------------------------------------------------------------------------------
ARCHITECTURE structure OF trafgen_top IS

    COMPONENT ila_0
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
    END COMPONENT;


    ---------------------------------------------------------------------------
    -- SIGNAL DECLARATIONS  --
    --------------------------------------------------------------------------- 
    signal start_stop_tx : std_logic;

    signal packetiser_data_in_wr : std_logic;
    signal packetiser_data : std_logic_vector(511 downto 0);
    signal swapped_packetiser_data : std_logic_vector(511 downto 0);
    signal packetiser_data_to_player_rdy : std_logic;
    signal packetiser_bytes_to_transmit : std_logic_vector(13 downto 0);
   
    signal beamData : std_logic_vector(63 downto 0);
    signal beamPacketCount : std_logic_vector(36 downto 0);
    signal beamBeam : std_logic_vector(7 downto 0);
    signal beamFreqIndex : std_logic_vector(10 downto 0);
    signal beamValid : std_logic;
    signal cmac_ready : std_logic;
    signal i_reset_packet_player : std_logic;

    
    
     signal dbg_ILA_trigger, bdbg_ILA_triggerDel1, bdbg_ILA_trigger, bdbg_ILA_triggerDel2 : std_logic;
    -- signal dataMismatch_dbg, dataMismatch, datamismatchBFclk : std_logic;
    
begin
    
    i_HBM_PktController : entity HBM_PktController_lib.HBM_PktController
    generic map (
        g_LFAA_BLOCKS_PER_FRAME_DIV3 => g_LFAA_BLOCKS_PER_FRAME_DIV3
    ) port map(
        clk_freerun => clk_freerun, 
        -- shared memory interface clock (300 MHz)
        i_shared_clk => i_MACE_clk, -- in std_logic;
        i_shared_rst => i_MACE_rst, -- in std_logic;

        o_reset_packet_player => i_reset_packet_player, 

        --AXI Lite Interface for registers
        i_saxi_mosi => i_HBM_Pktcontroller_Lite_axi_mosi , -- in t_axi4_lite_mosi;
        o_saxi_miso => o_HBM_Pktcontroller_Lite_axi_miso , -- out t_axi4_lite_miso;

        --Register outputs
        o_start_stop_tx => start_stop_tx, -- reset output from a register in the corner turn; used to reset downstream modules.
    
        o_packetiser_data_in_wr => packetiser_data_in_wr, 
        o_packetiser_data => packetiser_data, 
        o_packetiser_bytes_to_transmit => packetiser_bytes_to_transmit, 
        i_packetiser_data_to_player_rdy => packetiser_data_to_player_rdy, 
        -------------------------------------------------------------
        -- AXI bus to the shared memory. 
        -- This has the aw, b, ar and r buses (the w bus is on the output of the LFAA decode module)
        -- aw bus - write address
        m01_axi_awvalid => m01_axi_awvalid, -- out std_logic;
        m01_axi_awready => m01_axi_awready, -- in std_logic;
        m01_axi_awaddr  => m01_axi_awaddr,  -- out std_logic_vector(29 downto 0);
        m01_axi_awlen   => m01_axi_awlen,   -- out std_logic_vector(7 downto 0);
        -- b bus - write response
        m01_axi_bvalid  => m01_axi_bvalid, -- in std_logic;
        m01_axi_bresp   => m01_axi_bresp,  -- in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m01_axi_arvalid => m01_axi_arvalid, -- out std_logic;
        m01_axi_arready => m01_axi_arready, -- in std_logic;
        m01_axi_araddr  => m01_axi_araddr,  -- out std_logic_vector(29 downto 0);
        m01_axi_arlen   => m01_axi_arlen,   -- out std_logic_vector(7 downto 0);
        -- r bus - read data
        m01_axi_rvalid  => m01_axi_rvalid,  -- in std_logic;
        m01_axi_rready  => m01_axi_rready,  -- out std_logic;
        m01_axi_rdata   => m01_axi_rdata,   -- in std_logic_vector(511 downto 0);
        m01_axi_rlast   => m01_axi_rlast,   -- in std_logic;
        m01_axi_rresp   => m01_axi_rresp    -- in std_logic_vector(1 downto 0);
    );

 
    -- Swap the packetizer data because of bizarre CMAC 512 bit vector usage   
    -- process(packetiser_data)
    -- begin
    --     for n in 4 loop
    --         for i in 16 loop
    --             swapped_packetiser_data((128*n + 127 - i*8) downto (128*n 127 - i*8 -7)) <= packetiser_data((128*n + i*8+7) downto (128*n + i*8));
    --         end loop
    --     end loop
    -- end process

    -- Swap the packetizer data because of  bizarre CMAC 512 bit vector usage 
    GEN_SWITCHER:
    for n in 0 to 3 generate
    begin
        ROO:
        for i in 0 to 15 generate
            swapped_packetiser_data((128*n + 127 - i*8) downto (128*n + 127 - i*8 -7)) <= packetiser_data((128*n + i*8+7) downto (128*n+i*8));
        end generate ROO;
    end generate GEN_SWITCHER;



    i_packet_player : entity PSR_Packetiser_lib.packet_player 
        Generic Map(
            LBUS_TO_CMAC_INUSE      => TRUE,      -- FUTURE WORK to IMPLEMENT AXI
            PLAYER_CDC_FIFO_DEPTH   => 256        
            -- FIFO is 512 Wide, 9KB packets = 73728 bits, 512 * 256 = 131072, 256 depth allows ~1.88 9K packets, we are target packets sizes smaller than this.
        )
        Port map ( 
            i_clk400                => i_MACE_clk, 
            i_reset_400             => i_reset_packet_player,
        
            i_cmac_clk              => i_clk_100GE,
            i_cmac_clk_rst          => i_reset_packet_player,
            
            i_bytes_to_transmit     => packetiser_bytes_to_transmit,    -- 
            i_data_to_player        => swapped_packetiser_data, 
            i_data_to_player_wr     => packetiser_data_in_wr,
            o_data_to_player_rdy    => packetiser_data_to_player_rdy,
            
            o_cmac_ready            => cmac_ready,
        
            -- LBUS to CMAC
            o_data_to_transmit      => o_data_tx_sosi,
            i_data_to_transmit_ctl  => i_data_tx_siso
        );
  


    
  
    
 ---------------------------------------------------------------------------------------------------------------------------------------
-- ILA for debugging
    trafgen_ila : ila_0
    port map (
        clk                     => i_MACE_clk, 
        probe0(127 downto 0)    => packetiser_data(127 downto 0),
        probe0(128)             => packetiser_data_in_wr,
        probe0(129)             => packetiser_data_to_player_rdy, 
        probe0(143 downto 130)  => packetiser_bytes_to_transmit,
        probe0(144)             => cmac_ready, 
        probe0(191 downto 145)  => (others => '0')
    );
    

    
    -- -- Capture transactions on the m01 interfaces
    -- u_m01_ila : ila_0
    -- port map (
    --     clk => i_MACE_clk,
    --     probe0(29 downto 0) => m01_axi_araddr(29 downto 0),
    --     probe0(30) =>'0',
    --     probe0(31) =>'0',
    --     probe0(159 downto 32) => m01_axi_rdata(31 downto 0),
 
    --     probe0(160) => m01_axi_arvalid,
    --     probe0(161) => m01_axi_arready,
    --     probe0(169 downto 162) => m01_axi_arlen(7 downto 0),
        
    --     probe0(170) => m01_axi_rvalid,
    --     probe0(171) => m01_axi_rready,
         
    --     probe0(172) => m01_axi_rlast,
    --     probe0(174 downto 173) => m01_axi_rresp(1 downto 0),
    -- );

END structure;
