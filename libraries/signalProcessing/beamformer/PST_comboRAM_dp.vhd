----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 02/11/2024 01:38:41 PM
-- Design Name: 
-- Module Name: PST_comboRAM_dp - Behavioral
-- Description: 
-- 
-- Block RAM to hold the output packet data for the PST beamformer while we find the scale factor.
-- The beamformed data is stored as 32+32 bit complex numbers prior to scaling.
-- This module has memory for 1/3 of the fine channels, for a single coarse channel and 32 time samples.
-- (Three of these modules are instantiated for all the packet data for 1 coarse channel = 216 fine channels)
-- 
-- This module instantiates 2 ultraRAMs and 2 BRAMs.
-- The combined memory is 4608 deep x 128 bits wide; Sufficient for double buffered 1/3 of the fine channels in a coarse channel.
-- 
-- Writes consist of 2 polarisations for the same time sample
-- Reads consist of 2 times for the sample polarisation.
-- To do this, the memory is split into two parts, each 64 bits wide :
--
--  Address | Memory 0     |  Memory 1
--  -------   ------------    -------------
--      0     t=0, pol=0      t=1, pol=0
--      1     t=1, pol=1      t=0, pol=1
--      2     t=2, pol=0      t=3, pol=0
--      3     t=3, pol=1      t=2, pol=1
--      4     t=4, pol=0      t=5, pol=0
--      5     t=5, pol=1      t=4, pol=1
--      6     t=6, pol=0      t=7, pol=0
--      7     t=7, pol=1      t=6, pol=1
--     etc.
--
-- When reading, memory0 and memory1 have the same address, so we get 2 time samples with the same polarisation.
-- When writing, e.g. write to address 0 in memory 0 and address 1 in memory 1, so we can write 2 polarisations for the same time sample.
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
Library xpm;
use xpm.vcomponents.all;
use IEEE.NUMERIC_STD.ALL;

entity PST_comboRAM_dp is
    port(
        i_clk : in std_logic;
        ------------------------------------------------------------
        -- Writes : 2 polarisations for the same time
        -- 128 bits of input data
        i_din_pol0 : in std_logic_vector(63 downto 0);
        i_din_pol1 : in std_logic_vector(63 downto 0);
        -- Write address is made up of buffer, time sample and fine channel
        i_wr_buffer : in std_logic;
        i_wr_time   : in std_logic_vector(4 downto 0); -- 32 time samples
        i_wr_freq   : in std_logic_vector(6 downto 0); -- 72 frequency channels (7 bit value with valid range 0 to 71) 
        i_wrEn : in std_logic;
        ------------------------------------------------------------
        -- Reads : 2 time samples per clock
        -- 128 bit read data, 4 cycle latency
        o_dout_time0 : out std_logic_vector(63 downto 0);
        o_dout_time1 : out std_logic_vector(63 downto 0);
        i_rd_buffer : in std_logic;
        i_rd_time : in std_logic_vector(4 downto 0);  -- two time samples read out, i_rd_time and i_rd_time+1. The LSB of i_rd_time is ignored. 
        i_rd_pol : in std_logic;
        i_rd_freq : in std_logic_vector(6 downto 0)
    );
end PST_comboRAM_dp;

architecture Behavioral of PST_comboRAM_dp is

    signal ultraWE : std_logic_vector(0 downto 0);
    signal bramWE : std_logic_vector(0 downto 0);
    signal din128 : std_logic_vector(127 downto 0);
    signal ultraRAM_dout : std_logic_vector(127 downto 0);
    signal bram_dout : std_logic_vector(127 downto 0);
    signal dout : std_logic_vector(127 downto 0);
    signal rd_select, rd_select_del1 : std_logic;
    
    signal wrAddr0_bit0, wrAddr1_bit0 : std_logic;
    signal wr_time_del1, rd_time_del1 : std_logic_vector(4 downto 0);
    signal wr_freq, rd_freq, wrBaseAddr, rdBaseAddr, wrAddr_base_plus_freq, rdAddr_base_plus_freq : std_logic_vector(7 downto 0);
    signal din0, din1 : std_logic_vector(63 downto 0);
    signal wrAddr0, wrAddr1, rdAddr, rdAddr_del1, rdAddr_del2 : std_logic_vector(12 downto 0);
    signal we_del1 : std_logic;
    signal rd_pol_del1 : std_logic;
    signal ultraRAM_dout0, ultraRAM_dout1, bram_dout0, bram_dout1 : std_logic_vector(63 downto 0);
    
begin
    
    ------------------------------------------------------------------------
    -- Write address and enables
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            
            we_del1 <= i_wrEn;
            
            if i_wr_time(0) = '0' then
                din0 <= i_din_pol0;
                din1 <= i_din_pol1;
            else
                din0 <= i_din_pol1;
                din1 <= i_din_pol0;
            end if;
            
            wrAddr_base_plus_freq <= std_logic_vector(unsigned(wrBaseAddr) + unsigned(wr_freq(7 downto 0)));
            wr_time_del1 <= i_wr_time;
            
            wrAddr0_bit0 <= i_wr_time(0);
            wrAddr1_bit0 <= not i_wr_time(0);
        end if;
    end process;
    
    wr_freq <= '0' & i_wr_freq;
    
    wrBaseAddr <= 
        "00000000" when i_wr_buffer = '0' else 
        "01001000"; -- x48 = 72 = second half of the buffer.
    
    wrAddr0(12 downto 5) <= wrAddr_base_plus_freq;
    wrAddr0(4 downto 1) <= wr_time_del1(4 downto 1);
    wrAddr0(0) <= wrAddr0_bit0;
    
    wrAddr1(12 downto 5) <= wrAddr_base_plus_freq;
    wrAddr1(4 downto 1) <= wr_time_del1(4 downto 1);
    wrAddr1(0) <= wrAddr1_bit0;
    
    ultraWE(0) <= '1' when we_del1 = '1' and wrAddr0(12) = '0' else '0'; 
    bramWE(0) <= '1' when we_del1 = '1' and wrAddr1(12) = '1' else '0';
 
    -------------------------------------------------------------------------
    -- Read

    process(i_clk)
    begin
        if rising_edge(i_clk) then
            
            rdAddr_base_plus_freq <= std_logic_vector(unsigned(rdBaseAddr) + unsigned(rd_freq(7 downto 0)));
            rd_time_del1 <= i_rd_time;
            rd_pol_del1 <= i_rd_pol;
            
            wrAddr0_bit0 <= i_wr_time(0);
            wrAddr1_bit0 <= not i_wr_time(0);
            
            rdAddr_del1 <= rdAddr;
            rdAddr_del2 <= rdAddr_del1;
            
            if rdAddr_del2(12) = '0' then
                if rdAddr_del2(0) = '0' then
                    o_dout_time0 <= ultraRAM_dout0;
                    o_dout_time1 <= ultraRAM_dout1;
                else
                    o_dout_time0 <= ultraRAM_dout1;
                    o_dout_time1 <= ultraRAM_dout0;
                end if;
            else
                if rdAddr_del2(0) = '0' then
                    o_dout_time0 <= bram_dout0;
                    o_dout_time1 <= bram_dout1;
                else
                    o_dout_time0 <= bram_dout1;
                    o_dout_time1 <= bram_dout0;
                end if;
            end if;
            
        end if;
    end process;    
    
    rd_freq <= '0' & i_rd_freq;
    rdBaseAddr <= 
        "00000000" when i_rd_buffer = '0' else 
        "01001000"; -- x48 = 72 = second half of the buffer.
    
    rdAddr(12 downto 5) <= rdAddr_base_plus_freq;
    rdAddr(4 downto 1) <= rd_time_del1(4 downto 1);
    rdAddr(0) <= rd_pol_del1;
    
    
    -------------------------------------------------------------------------
    
    xpm_mem_ultraRAM_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 12,              -- DECIMAL
        ADDR_WIDTH_B => 12,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 64,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "ultra",     -- String
        MEMORY_SIZE => 262144,           -- 64 bits wide x 4096 deep = 262144 bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 64,        -- DECIMAL
        READ_LATENCY_B => 2,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
        WRITE_MODE_B => "read_first"     -- String
    ) port map (
        dbiterrb => open,  
        doutb => ultraRAM_dout0, -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,
        addra => wrAddr0(11 downto 0),      -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb => rdAddr(11 downto 0),      -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka => i_clk,                 
        clkb => i_clk,             
        dina => din0,   -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena => '1',    
        enb => '1',
        injectdbiterra => '0',
        injectsbiterra => '0',
        regceb => '1',      -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb => '0',        -- 1-bit input: Reset signal for the final port B output register stage. 
        sleep => '0',       -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => ultraWE      -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );

    xpm_mem_BlockRAM_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 9,               -- DECIMAL
        ADDR_WIDTH_B => 9,               -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 64,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "block",     -- String
        MEMORY_SIZE => 32768,            -- 64 bits wide x 512 deep = 32768 bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 64,         -- DECIMAL
        READ_LATENCY_B => 2,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
        WRITE_MODE_B => "read_first"     -- String
    ) port map (
        dbiterrb => open,  
        doutb => bram_dout0, -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,
        addra => wrAddr0(8 downto 0),      -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb => rdAddr(8 downto 0),      -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka => i_clk,                 
        clkb => i_clk,             
        dina => din0,   -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena => '1',    
        enb => '1',
        injectdbiterra => '0',
        injectsbiterra => '0',
        regceb => '1',     -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb => '0',       -- 1-bit input: Reset signal for the final port B output register stage. 
        sleep => '0',      -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => bramWE      -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );

    xpm_mem_ultraRAM2_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 12,              -- DECIMAL
        ADDR_WIDTH_B => 12,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 64,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "ultra",     -- String
        MEMORY_SIZE => 262144,           -- 64 bits wide x 4096 deep = 262144 bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 64,        -- DECIMAL
        READ_LATENCY_B => 2,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
        WRITE_MODE_B => "read_first"     -- String
    ) port map (
        dbiterrb => open,  
        doutb => ultraRAM_dout1, -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,
        addra => wrAddr1(11 downto 0),      -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb => rdAddr(11 downto 0),      -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka => i_clk,                 
        clkb => i_clk,             
        dina => din1,   -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena => '1',    
        enb => '1',
        injectdbiterra => '0',
        injectsbiterra => '0',
        regceb => '1',      -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb => '0',        -- 1-bit input: Reset signal for the final port B output register stage. 
        sleep => '0',       -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => ultraWE      -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );

    xpm_mem_BlockRAM2_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 9,               -- DECIMAL
        ADDR_WIDTH_B => 9,               -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 64,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "block",     -- String
        MEMORY_SIZE => 32768,            -- 64 bits wide x 512 deep = 32768 bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 64,         -- DECIMAL
        READ_LATENCY_B => 2,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
        WRITE_MODE_B => "read_first"     -- String
    ) port map (
        dbiterrb => open,  
        doutb => bram_dout1, -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,
        addra => wrAddr1(8 downto 0),      -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb => rdAddr(8 downto 0),      -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka => i_clk,                 
        clkb => i_clk,             
        dina => din1,   -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena => '1',    
        enb => '1',
        injectdbiterra => '0',
        injectsbiterra => '0',
        regceb => '1',     -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb => '0',       -- 1-bit input: Reset signal for the final port B output register stage. 
        sleep => '0',      -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => bramWE      -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );

  

end Behavioral;
