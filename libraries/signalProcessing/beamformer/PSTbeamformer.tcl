create_ip -name dds_compiler -vendor xilinx.com -library ip -version 6.0 -module_name sincosLookup
set_property -dict [list CONFIG.Component_Name {sincosLookup} CONFIG.PartsPresent {SIN_COS_LUT_only} CONFIG.Noise_Shaping {None} CONFIG.Phase_Width {12} CONFIG.Output_Width {18} CONFIG.Amplitude_Mode {Unit_Circle} CONFIG.Parameter_Entry {Hardware_Parameters} CONFIG.Has_Phase_Out {false} CONFIG.DATA_Has_TLAST {Not_Required} CONFIG.S_PHASE_Has_TUSER {Not_Required} CONFIG.M_DATA_Has_TUSER {Not_Required} CONFIG.Latency {6} CONFIG.Output_Frequency1 {0} CONFIG.PINC1 {0}] [get_ips sincosLookup]
create_ip_run [get_ips sincosLookup]

create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name BF_cMult27x18
set_property -dict [list CONFIG.Component_Name {BF_cMult27x18} CONFIG.APortWidth {27} CONFIG.BPortWidth {18} CONFIG.OptimizeGoal {Performance} CONFIG.OutputWidth {46} CONFIG.MinimumLatency {4}] [get_ips BF_cMult27x18]
create_ip_run [get_ips BF_cMult27x18]

# Used for 8-bit filterbank data version (jonesMatrixMult.vhd), superseeded by the 16-bit version
#create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name complexMult16x8
#set_property -dict [list CONFIG.Component_Name {complexMult16x8} CONFIG.BPortWidth {8} CONFIG.OptimizeGoal {Performance} CONFIG.OutputWidth {25} CONFIG.MinimumLatency {4}] [get_ips complexMult16x8]
#create_ip_run [get_ips complexMult16x8]

create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name complexMult16x16
set_property -dict [list CONFIG.Component_Name {complexMult16x16} CONFIG.BPortWidth {16} CONFIG.OptimizeGoal {Performance} CONFIG.OutputWidth {33} CONFIG.MinimumLatency {4}] [get_ips complexMult16x16]
create_ip_run [get_ips complexMult16x16]

create_ip -name axi_clock_converter -vendor xilinx.com -library ip -version 2.1 -module_name axi_clock_converter_BF
set_property -dict [list CONFIG.Component_Name {axi_clock_converter_BF} CONFIG.ADDR_WIDTH {22} CONFIG.ACLK_ASYNC {1}] [get_ips axi_clock_converter_BF]
create_ip_run [get_ips axi_clock_converter_BF]

create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_BF
set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1} CONFIG.Component_Name {axi_bram_ctrl_BF} CONFIG.MEM_DEPTH {262144} CONFIG.READ_LATENCY {5}] [get_ips axi_bram_ctrl_BF]
create_ip_run [get_ips axi_bram_ctrl_BF]
