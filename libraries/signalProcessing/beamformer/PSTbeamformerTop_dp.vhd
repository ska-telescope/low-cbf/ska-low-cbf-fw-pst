----------------------------------------------------------------------------------
-- Company: CSIRO 
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 23.11.2020 16:50:47
-- Module Name: PSTbeamformerTop_dp - Behavioral
-- Description: 
--   Top level for the PST beamformer
--   "dp" indicates this is the "Delay Polynomial" version, which gets delays via a polynomial 
--   (as opposed to a linear approximation to the phase)
--   The beamformer doesn't actually calculate the delay polynomial, it just takes phase 
--   inputs "i_phase" and "i_phase_step" at each time sample
-- Registers:
--   Each beam has 64Kbytes of address space.
--   This module supports up to 64 beams via the g_PST_BEAMS generic.
--   So 4Mbytes of address space is assumed here (64 * 64Kbytes = 4 MBytes). 4Mbytes = 22 address bits.
-- 
----------------------------------------------------------------------------------
library IEEE, axi4_lib, common_lib, bf_lib, xpm;
use xpm.vcomponents.all;
use IEEE.STD_LOGIC_1164.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;


entity PSTbeamformerTop_dp is
    generic (
        g_DEBUG_ILA         : BOOLEAN := FALSE;
        g_PIPE_INSTANCE     : integer := 0;
        g_PST_BEAMS         : integer := 16
    );
    port(
        -- registers axi full interface
        i_MACE_clk : in std_logic;
        i_MACE_rst : in std_logic;
        i_axi_mosi : in  t_axi4_full_mosi;
        o_axi_miso : out t_axi4_full_miso;
        ----------------------------------------------------------------------
        -- Beamformer data from the corner turn
        i_BF_clk : in std_logic;
        i_data   : in std_logic_vector(191 downto 0);  -- 3 consecutive fine channels delivered every clock.
        i_flagged : in std_logic_vector(2 downto 0);  -- aligns with i_data, indicates if any of the 3 fine channels are flagged as RFI.
        i_fine   : in std_logic_vector(7 downto 0);   -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        i_coarse : in std_logic_vector(9 downto 0);   -- index of the coarse channel.
        i_firstStation : in std_logic;                -- First station (used to trigger a new accumulator cycle in the beamformers).
        i_lastStation  : in std_logic;
        i_timeStep     : in std_logic_vector(4 downto 0);   -- Timestep, runs from 0 to 31. There are 32 timesteps per output packet.
        i_virtualChannel : in std_logic_vector(9 downto 0); -- virtual channel
        -- The PST output packet count for this packet, based on the original packet count from LFAA.
        -- Each PST output packet is 6.63552 ms of data. There are 8 PST output packets per corner turn frame.
        i_packetCount  : in std_logic_vector(39 downto 0);
        i_outputPktOdd : in std_logic;
        i_valid   : in std_logic;
        ----------------------------------------------------------------------
        -- polynomial data 
        i_phase_virtualChannel : in std_logic_vector(9 downto 0);
        i_phase_timeStep : in std_logic_vector(7 downto 0);
        i_phase_beam : in std_logic_vector(3 downto 0);
        i_phase : in std_logic_vector(23 downto 0);      -- Phase at the start of the coarse channel.
        i_phase_step : in std_logic_vector(23 downto 0); -- Phase step per fine channel.
        i_phase_valid : in std_logic;
        i_phase_clear : in std_logic;    -- clear registers that hold the current phase to prevent old values being used. This should be set once at the start of each corner turn frame 
        -----------------------------------------------------------------------
        -- Other data from the corner turn
        i_jonesBuffer  : in std_logic;
        i_jones_status : in std_logic_vector(1 downto 0);  -- bit 0 = used default, bit 1 = jones valid;
        i_poly_ok      : in std_logic_vector(1 downto 0);  -- the polynomials used are within their valid time range;
        i_beamsEnabled : in std_logic_vector(7 downto 0);  -- Number of beams which are enabled. 
        i_scale_exp_frac : in std_logic_vector(7 downto 0); -- scale factor as configured from ARGs.
        -----------------------------------------------------------------------
        -- 64 bit bus out to the 100GE Packetiser
        o_BFdata         : out std_logic_vector(63 downto 0);
        o_BFpacketCount  : out std_logic_vector(39 downto 0);
        o_BFBeam         : out std_logic_vector(7 downto 0);
        o_BFFreqIndex    : out std_logic_vector(10 downto 0);
        o_BFvalid        : out std_logic;
        o_BFjones_status : out std_logic_vector(1 downto 0);
        o_BFpoly_ok      : out std_logic_vector(1 downto 0);
        -- debug signals
        i_badPacket : in std_logic
    );
    
    -- prevent optimisation between adjacent instances of the PSTbeamformer.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of PSTbeamformerTop_dp : entity is "yes";
    
    
end PSTbeamformerTop_dp;

architecture Behavioral of PSTbeamformerTop_dp is

    -- create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_BF
    -- set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1} CONFIG.Component_Name {axi_bram_ctrl_BF} CONFIG.MEM_DEPTH {262144} CONFIG.READ_LATENCY {5}] [get_ips axi_bram_ctrl_BF]
    -- generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201123_161551/vitisAccelCore.srcs/sources_1/ip/axi_bram_ctrl_BF/axi_bram_ctrl_BF.xci]
    component axi_bram_ctrl_BF
    port (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC;
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC;
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        bram_rst_a : OUT STD_LOGIC;
        bram_clk_a : OUT STD_LOGIC;
        bram_en_a : OUT STD_LOGIC;
        bram_we_a : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr_a : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
        bram_wrdata_a : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata_a : IN STD_LOGIC_VECTOR(31 DOWNTO 0));
    end component;

    COMPONENT axi_clock_converter_BF
    PORT (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(21 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awregion : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awqos : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(21 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arregion : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arqos : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        m_axi_aclk : IN STD_LOGIC;
        m_axi_aresetn : IN STD_LOGIC;
        m_axi_awaddr : OUT STD_LOGIC_VECTOR(21 DOWNTO 0);
        m_axi_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_awlock : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awregion : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awvalid : OUT STD_LOGIC;
        m_axi_awready : IN STD_LOGIC;
        m_axi_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_wlast : OUT STD_LOGIC;
        m_axi_wvalid : OUT STD_LOGIC;
        m_axi_wready : IN STD_LOGIC;
        m_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_bvalid : IN STD_LOGIC;
        m_axi_bready : OUT STD_LOGIC;
        m_axi_araddr : OUT STD_LOGIC_VECTOR(21 DOWNTO 0);
        m_axi_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_arlock : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arregion : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arvalid : OUT STD_LOGIC;
        m_axi_arready : IN STD_LOGIC;
        m_axi_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_rlast : IN STD_LOGIC;
        m_axi_rvalid : IN STD_LOGIC;
        m_axi_rready : OUT STD_LOGIC);
    end component;
    
    COMPONENT ila_pst
    PORT (
   	    clk : IN STD_LOGIC;
   	    probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
    END COMPONENT;    

    COMPONENT ila_pst2
    PORT (
   	    clk : IN STD_LOGIC;
   	    probe0 : IN STD_LOGIC_VECTOR(95 DOWNTO 0));
    END COMPONENT; 
    
    signal BFmemRst   : std_logic;
    signal BFmemClk   : std_logic;
    signal BFmemEn    : std_logic;
    signal BFmemWrEn  : std_logic_vector(3 downto 0);
    signal BFmemAddr  : std_logic_vector(19 downto 0);
    signal BFmemDin   : std_logic_vector(31 downto 0);
    signal BFmemDout  : t_slv_32_arr(g_PST_BEAMS downto 0);
    
    signal MACE_rstn : std_logic;
    
    signal axi_mosi : t_axi4_full_mosi;
    signal axi_miso : t_axi4_full_miso;    
    signal awlock_slv : std_logic_vector(0 downto 0);
    signal arlock_slv : std_logic_vector(0 downto 0);
    
    signal axi_mosi_arlock : std_logic_vector(0 downto 0);
    signal axi_mosi_awlock : std_logic_vector(0 downto 0);
    signal highbits : std_logic_vector(1 downto 0) := "00";
    signal regAddr : t_slv_24_arr(g_PST_BEAMS downto 0);
    signal regWrData : t_slv_32_arr(g_PST_BEAMS downto 0);
    signal regWrEn : std_logic_vector(g_PST_BEAMS downto 0);
    signal jonesBuffer : std_logic_vector(g_PST_BEAMS downto 0);
    signal beamsEnabled, scale_exp_frac : t_slv_8_arr(g_PST_BEAMS downto 0);
    
    signal PData : t_slv_64_arr(g_PST_BEAMS downto 0);
    signal PvirtualChannel : t_slv_10_arr(g_PST_BEAMS downto 0);
    signal PpacketCount : t_slv_40_arr(g_PST_BEAMS downto 0);
    signal PBeam : t_slv_8_arr(g_PST_BEAMS downto 0);
    signal PFreqIndex : t_slv_11_arr(g_PST_BEAMS downto 0);
    signal Pvalid : std_logic_vector(g_PST_BEAMS downto 0);
    
    signal BFdata : t_slv_192_arr(g_PST_BEAMS downto 0);
    signal BFflagged : t_slv_3_arr(g_PST_BEAMS downto 0);
    signal BFfine : t_slv_8_arr(g_PST_BEAMS downto 0);
    signal BFCoarse : t_slv_10_arr(g_PST_BEAMS downto 0);
    signal BFfirstStation : std_logic_vector(g_PST_BEAMS downto 0);
    signal BFlastStation : std_logic_vector(g_PST_BEAMS downto 0);
    signal BFvirtualChannel : t_slv_10_arr(g_PST_BEAMS downto 0);
    signal BFpacketCount : t_slv_40_arr(g_PST_BEAMS downto 0);
    signal BFvalid, BFpktOdd : std_logic_vector(g_PST_BEAMS downto 0);
    signal BFtimeStep : t_slv_5_arr(g_PST_BEAMS downto 0);
    signal MACE_rstn_BFclk : std_logic;
    signal rst_BFclk : std_logic;
    signal badPacket : std_logic_vector(g_PST_BEAMS downto 0);
    
    signal phase_virtualChannel : t_slv_10_arr(g_PST_BEAMS downto 0);
    signal phase_timeStep : t_slv_8_arr(g_PST_BEAMS downto 0);
    signal phase_beam : t_slv_4_arr(g_PST_BEAMS downto 0);
    signal phase : t_slv_24_arr(g_PST_BEAMS downto 0);
    signal phase_step : t_slv_24_arr(g_PST_BEAMS downto 0);
    signal phase_valid, phase_clear : std_logic_vector(g_PST_BEAMS downto 0);
    
    signal Jones_status : t_slv_2_arr(3 downto 0);
    signal poly_ok : t_slv_2_arr(3 downto 0);
    
    signal dbg_BFpacketCount : std_logic_vector(39 downto 0);
    signal dbg_BFBeam : std_logic_vector(7 downto 0);
    signal dbg_BFFreqIndex : std_logic_vector(10 downto 0);
    signal dbg_BFvalid : std_logic;
    
    signal dbg_BFpacketCount_del1 : std_logic_vector(39 downto 0);
    signal dbg_BFvalid_del1 : std_logic;

    signal dbg_BFpacketCount_del2 : std_logic_vector(39 downto 0);
    signal dbg_BFvalid_del2 : std_logic;
    
    signal expected_packetCount, expected_next_packetCount : std_logic_vector(39 downto 0);
    signal expected_set, expected_next_set : std_logic := '0';
    signal bad_packetCount : std_logic := '0';
    
    
    signal dbg_i_flagged : std_logic_vector(2 downto 0);
    signal dbg_i_fine : std_logic_vector(6 downto 0);
    signal dbg_i_coarse : std_logic_vector(3 downto 0);
    signal dbg_i_firstStation : std_logic;
    signal dbg_i_lastStation : std_logic;
    signal dbg_i_timestep : std_logic_vector(4 downto 0);
    signal dbg_i_virtualChannel : std_logic_vector(3 downto 0);
    signal dbg_i_packetCount : std_logic_vector(19 downto 0);
    signal dbg_i_outputPktOdd : std_logic;
    signal dbg_i_valid : std_logic;
    signal dbg_i_phase_virtualChannel : std_logic_vector(3 downto 0);
    signal dbg_i_phase_timestep : std_logic_vector(7 downto 0);
    signal dbg_i_phase_beam : std_logic_vector(3 downto 0);
    signal dbg_i_phase_valid : std_logic;
    signal dbg_i_phase_clear : std_logic;
    
begin

    MACE_rstn <= not i_MACE_rst;

    xpm_cdc_pulse_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 3,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    )
    port map (
        dest_pulse => rst_BFclk,  -- 1-bit output
        dest_clk => i_BF_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',
        src_clk => i_MACE_clk,    -- 1-bit input: Source clock.
        src_pulse => i_MACE_rst,  -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'
    );
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            MACE_rstn_BFclk <= not rst_BFclk;
        end if;
    end process;
    
    awlock_slv(0) <= i_axi_mosi.awlock;
    arlock_slv(0) <= i_axi_mosi.arlock;
    

    BF_cci : axi_clock_converter_BF
    port map (
        s_axi_aclk    => i_MACE_clk, -- IN STD_LOGIC;
        s_axi_aresetn => MACE_rstn, -- IN STD_LOGIC;
        s_axi_awaddr    => i_axi_mosi.awaddr(21 downto 0),
        s_axi_awlen     => i_axi_mosi.awlen,
        s_axi_awsize    => i_axi_mosi.awsize,
        s_axi_awburst   => i_axi_mosi.awburst,
        s_axi_awlock    => awlock_slv,
        s_axi_awcache   => i_axi_mosi.awcache,
        s_axi_awprot    => i_axi_mosi.awprot,
        s_axi_awregion => (others => '0'), -- in(3:0);
        s_axi_awqos    => (others => '0'), -- in(3:0);
        s_axi_awvalid   => i_axi_mosi.awvalid,
        s_axi_awready   => o_axi_miso.awready,        
        
        s_axi_wdata     => i_axi_mosi.wdata(31 downto 0),
        s_axi_wstrb     => i_axi_mosi.wstrb(3 downto 0),
        s_axi_wlast     => i_axi_mosi.wlast,
        s_axi_wvalid    => i_axi_mosi.wvalid,
        s_axi_wready    => o_axi_miso.wready,
        
        s_axi_bresp     => o_axi_miso.bresp,
        s_axi_bvalid    => o_axi_miso.bvalid,
        s_axi_bready    => i_axi_mosi.bready ,

        s_axi_araddr    => i_axi_mosi.araddr(21 downto 0),
        s_axi_arlen     => i_axi_mosi.arlen,
        s_axi_arsize    => i_axi_mosi.arsize,
        s_axi_arburst   => i_axi_mosi.arburst,
        s_axi_arlock    => arlock_slv,
        s_axi_arcache   => i_axi_mosi.arcache,
        s_axi_arprot    => i_axi_mosi.arprot,
        s_axi_arregion  => "0000", -- in(3:0),
        s_axi_arqos     => "0000", -- in(3:0),
        s_axi_arvalid   => i_axi_mosi.arvalid,
        s_axi_arready   => o_axi_miso.arready,
          
        s_axi_rdata     => o_axi_miso.rdata(31 downto 0),
        s_axi_rresp     => o_axi_miso.rresp,
        s_axi_rlast     => o_axi_miso.rlast,
        s_axi_rvalid    => o_axi_miso.rvalid,
        s_axi_rready    => i_axi_mosi.rready,
        -- master interface

        m_axi_aclk    => i_BF_clk, -- in std_logic;
        m_axi_aresetn => MACE_rstn_BFclk, -- in std_logic;
        m_axi_awaddr  => axi_mosi.awaddr(21 downto 0), -- out STD_LOGIC_VECTOR(19 DOWNTO 0);
        m_axi_awlen   => axi_mosi.awlen, -- out STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_awsize  => axi_mosi.awsize, -- out STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awburst => axi_mosi.awburst, -- out STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_awlock  => axi_mosi_awlock, -- OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_awcache => axi_mosi.awcache, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awprot  => axi_mosi.awprot, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awregion => axi_mosi.awregion, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awqos   => axi_mosi.awqos, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awvalid => axi_mosi.awvalid, -- OUT STD_LOGIC;
        m_axi_awready => axi_miso.awready, -- IN STD_LOGIC;
        m_axi_wdata  => axi_mosi.wdata(31 downto 0), -- OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_wstrb  => axi_mosi.wstrb(3 downto 0), -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_wlast  => axi_mosi.wlast, -- OUT STD_LOGIC;
        m_axi_wvalid => axi_mosi.wvalid, -- OUT STD_LOGIC;
        m_axi_wready => axi_miso.wready, -- IN STD_LOGIC;
        m_axi_bresp  => axi_miso.bresp, -- IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_bvalid => axi_miso.bvalid, -- IN STD_LOGIC;
        m_axi_bready => axi_mosi.bready, -- OUT STD_LOGIC;
        m_axi_araddr => axi_mosi.araddr(21 downto 0), -- OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
        m_axi_arlen  => axi_mosi.arlen, -- OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_arsize => axi_mosi.arsize, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arburst => axi_mosi.arburst, -- OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_arlock  => axi_mosi_arlock, -- OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_arcache => axi_mosi.arcache, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arprot  => axi_mosi.arprot, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arregion => axi_mosi.arregion, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arqos => axi_mosi.arqos, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arvalid => axi_mosi.arvalid, -- OUT STD_LOGIC;
        m_axi_arready => axi_miso.arready, -- IN STD_LOGIC;
        m_axi_rdata   => axi_miso.rdata(31 downto 0), -- IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_rresp   => axi_miso.rresp, -- IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_rlast   => axi_miso.rlast, -- IN STD_LOGIC;
        m_axi_rvalid  => axi_miso.rvalid, -- IN STD_LOGIC;
        m_axi_rready => axi_mosi.rready  -- OUT STD_LOGIC
    );

    axi_mosi.awlock <= axi_mosi_awlock(0);
    axi_mosi.arlock <= axi_mosi_arlock(0);
    
    -- Convert register interface from AXI full to address + data
    BF_ctrli : axi_bram_ctrl_BF
    port map (
        s_axi_aclk      => i_BF_clk,
        s_axi_aresetn   => MACE_rstn_BFclk, -- in std_logic;
        s_axi_awaddr    => axi_mosi.awaddr(19 downto 0),
        s_axi_awlen     => axi_mosi.awlen,
        s_axi_awsize    => axi_mosi.awsize,
        s_axi_awburst   => axi_mosi.awburst,
        s_axi_awlock    => axi_mosi.awlock ,
        s_axi_awcache   => axi_mosi.awcache,
        s_axi_awprot    => axi_mosi.awprot,
        s_axi_awvalid   => axi_mosi.awvalid,
        s_axi_awready   => axi_miso.awready,
        s_axi_wdata     => axi_mosi.wdata(31 downto 0),
        s_axi_wstrb     => axi_mosi.wstrb(3 downto 0),
        s_axi_wlast     => axi_mosi.wlast,
        s_axi_wvalid    => axi_mosi.wvalid,
        s_axi_wready    => axi_miso.wready,
        s_axi_bresp     => axi_miso.bresp,
        s_axi_bvalid    => axi_miso.bvalid,
        s_axi_bready    => axi_mosi.bready ,
        s_axi_araddr    => axi_mosi.araddr(19 downto 0),
        s_axi_arlen     => axi_mosi.arlen,
        s_axi_arsize    => axi_mosi.arsize,
        s_axi_arburst   => axi_mosi.arburst,
        s_axi_arlock    => axi_mosi.arlock ,
        s_axi_arcache   => axi_mosi.arcache,
        s_axi_arprot    => axi_mosi.arprot,
        s_axi_arvalid   => axi_mosi.arvalid,
        s_axi_arready   => axi_miso.arready,
        s_axi_rdata     => axi_miso.rdata(31 downto 0),
        s_axi_rresp     => axi_miso.rresp,
        s_axi_rlast     => axi_miso.rlast,
        s_axi_rvalid    => axi_miso.rvalid,
        s_axi_rready    => axi_mosi.rready,
        bram_rst_a      => BFmemRst,   -- out std_logic;
        bram_clk_a      => BFmemClk,   -- out std_logic;
        bram_en_a       => BFmemEn,     -- out std_logic;
        bram_we_a       => BFmemWrEn,   -- out (3:0)
        bram_addr_a     => BFmemAddr,  -- out (19:0)
        bram_wrdata_a   => BFmemDin,   -- out (31:0)
        bram_rddata_a   => BFmemDout(0)   -- in (31:0)
    );
    
    -- Capture the high order bits of the write address, since the Xilinx axi to BRAM interface only supports 1 Mbyte of address space.
    -- This is a bit of a cheat; check in simulation that the address bits are transferred correctly. May need to add some latency to 
    -- account for the latency through the xilinx axi to bram block.
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            if (axi_mosi.awvalid = '1' and axi_miso.awready = '1') then
                highBits <= axi_mosi.awaddr(21 downto 20);
            end if;
        end if;
    end process;

    -- First stage of the daisy chained register interface
    regAddr(0) <= "00" & highBits & BFmemAddr;
    regWrData(0) <= BFmemDin;
    regWrEn(0) <= BFmemWrEn(0) and BFmemEn;  -- BFmemWrEn is 4 bits wide, one bit per byte; This assumes that all writes are 4 bytes wide. 
        
    -- First stage of the daisy chained data pipeline
    BFdata(0) <= i_data;
    BFflagged(0) <= i_flagged;
    BFfine(0) <= i_fine;
    BFCoarse(0) <= i_coarse;
    BFfirstStation(0) <= i_firstStation;
    BFlastStation(0) <= i_lastStation;
    BFtimeStep(0) <= i_timeStep;
    BFvirtualChannel(0) <= i_virtualChannel;
    BFpacketCount(0) <= i_packetCount;
    BFpktOdd(0) <= i_outputPktOdd;
    BFvalid(0) <= i_valid;

    jonesBuffer(0) <= i_jonesBuffer;
    beamsEnabled(0) <= i_beamsEnabled;
    scale_exp_frac(0) <= i_scale_exp_frac;
    
    phase_virtualChannel(0) <= i_phase_virtualChannel;  -- in (9:0);
    phase_timeStep(0) <= i_phase_timeStep;  -- in (7:0);
    phase_beam(0) <= i_phase_beam;          -- in (3:0);
    phase(0) <= i_phase;             -- in (23:0); Phase at the start of the coarse channel.
    phase_step(0) <= i_phase_step;   -- in (23:0); Phase step per fine channel.
    phase_valid(0) <= i_phase_valid; -- in std_logic;
    phase_clear(0) <= i_phase_clear; -- in std_logic;
    
    -- First stage of the beamformed data pipeline 
    Pdata(0) <= (others => '0');        -- (63:0);
    PpacketCount(0) <= (others => '0'); -- in (39:0);
    PBeam(0) <= (others => '0');        -- in (7:0); 
    PFreqIndex(0) <= (others => '0'); 
    Pvalid(0) <= '0';
    
    badPacket(0) <= i_badPacket;
    
    -- Instantiate one module for each beam we are making
    beamGen : for i in 0 to (g_PST_BEAMS-1) generate
        
        bfinst: entity bf_lib.PSTbeamformer_dp
        generic map (
            g_BEAM_NUMBER => i, -- : integer;
            -- Number of clock cycles to wait after a packet is ready to send before sending it on the BFData interface.
            -- This is to ensure there is are no clashes on the BFdata bus between different beamformers.
            g_PACKET_OFFSET => (i*800) --  integer
        ) port map(
            i_BF_clk  => i_BF_clk, --  in std_logic; -- 400MHz clock
            -- Data in to the beamformer
            i_data    => BFdata(i),    -- in (191:0);  3 consecutive fine channels delivered every clock. Must have a six cycle latency relative to the other inputs ("i_fine", "i_station", etc)
            i_flagged => BFflagged(i), -- in (2:0);
            i_fine    => BFfine(i),    -- in (7:0); fine channel / 3, so the actual fine channel for the first of the 3 fine channels in i_data is (i_fine*3)
            i_coarse  => BFCoarse(i),  -- in(9:0)
            i_firstStation => BFfirstStation(i),  -- in std_logic; First station in a burst.
            i_lastStation => BFlastStation(i),    -- in std_logic; Last station
            i_timeStep    => BFtimeStep(i),       -- in std_logic_vector(4 downto 0);   -- Timestep, runs from 0 to 31. There are 32 timesteps per output packet.
            i_virtualChannel => BFvirtualChannel(i), -- in std_logic_vector(9 downto 0); -- virtual channel
            i_packetCount => BFpacketCount(i), -- in (39:0); The output packet count for this packet, in units of 6.63552 ms since the SKA epoch (Each PST packet is 6.63552 ms)
            i_pktOdd  => BFpktOdd(i), -- in std_logic;
            i_valid   => BFvalid(i), -- in std_logic;
            --------------------------------------------------------------------------
            -- Phase data
            -- This is captured in a pair of registers, which form a 2-deep FIFO, so data for the next station or time can be
            -- loaded while the current station/time is being used.
            i_phase_virtualChannel => phase_virtualChannel(i), -- in (9:0);
            i_phase_timeStep       => phase_timestep(i),       -- in (7:0);
            i_phase_beam           => phase_beam(i),           -- in (3:0);
            i_phase                => phase(i),                -- in (23:0);  Phase at the start of the coarse channel.
            i_phase_step           => phase_step(i),           -- in (23:0);  Phase step per fine channel.
            i_phase_valid          => phase_valid(i),          -- in std_logic;
            i_phase_clear          => phase_clear(i),          -- in std_logic;
            -- Pass on phase data to the next beamformer.
            o_phase_virtualChannel => phase_virtualChannel(i+1), -- out (9:0);
            o_phase_timeStep       => phase_timestep(i+1),       -- out (7:0);
            o_phase_beam           => phase_beam(i+1),           -- out (3:0);
            o_phase                => phase(i+1),                -- out (23:0);  Phase at the start of the coarse channel.
            o_phase_step           => phase_step(i+1),           -- out (23:0);  Phase step per fine channel.
            o_phase_valid          => phase_valid(i+1),          -- out std_logic;
            o_phase_clear          => phase_clear(i+1),          -- out std_logic;
            -- Data out to the next beamformer (pipelined version of the data in) (2 pipeline stages in this module)
            o_data    => BFdata(i+1),    -- out (95:0); 3 consecutive fine channels delivered every clock; 
            o_flagged => BFflagged(i+1), -- out (2:0);
            o_fine    => BFfine(i+1),    -- out (7:0);  fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
            o_coarse  => BFCoarse(i+1),  -- out(9:0)
            o_firstStation => BFfirstStation(i+1), -- out std_logic; pipelined i_firstStation
            o_lastStation  => BFlastStation(i+1),  -- out std_logic; pipelined i_lastStation
            o_timeStep     => BFtimeStep(i+1),     -- out(4:0); pipelined i_timeStep
            o_virtualChannel => BFvirtualChannel(i+1), -- out (9:0); -- .
            o_packetCount => BFpacketCount(i+1),       -- out (39:0); -- The packet count for this packet, based on the original packet count from LFAA.
            o_pktOdd  => BFpktOdd(i+1), -- out std_logic;
            o_valid   => BFvalid(i+1),  -- out std_logic;
            ------------------------------------------------------------------------
            -- Register interface (just address + data)
            i_regAddr => regAddr(i),   -- in(23:0); -- Byte address of the data (low two bits will always be "00")
            i_regData => regWrData(i), -- in(31:0);
            i_WrEn    => regWrEn(i),   -- in std_logic;
            -- Pipelined version of the register interface, for the next beamformer.
            o_regAddr => regAddr(i+1),   -- out (23:0);
            o_regData => regWrData(i+1), -- out (31:0);
            o_wrEn    => regWrEn(i+1),   -- out std_logic;
            -- register read data, 5 cycle latency from i_regAddr
            o_regReadData => BFmemDout(i), -- out(31:0);
            -- Miscellaneous register settings:
            i_jonesBuffer => jonesBuffer(i),   -- in std_logic; Which of the two buffers to use for the Jones matrices
            i_beamsEnabled => beamsEnabled(i), -- in (7:0); -- Number of beams that are enabled. This beamformer outputs packets if g_BEAM_NUMBER < i_beamsEnabled
            i_scale_exp_frac => scale_exp_frac(i),
            -- pipelined outputs for buffer settings
            o_jonesBuffer => jonesBuffer(i+1),   -- out std_logic;
            o_beamsEnabled => beamsEnabled(i+1), -- out (7:0);
            o_scale_exp_frac => scale_exp_frac(i+1),
            ---------------------------------------------------------------------
            -- Packets of data from the previous beamformer, to be passed on to the next beamformer.
            i_BFdata        => Pdata(i),        -- in(63:0);
            i_BFpacketCount => PpacketCount(i), -- out(39:0);
            i_BFBeam        => PBeam(i),        -- in(7:0);  
            i_BFFreqIndex   => PFreqIndex(i),   -- in(10:0);
            i_BFvalid       => PValid(i),       -- in std_logic;
            -- Packets of data out to the 100G interface
            o_BFdata        => Pdata(i+1),        -- out(63:0);
            o_BFpacketCount => PpacketCount(i+1), -- out(39:0);
            o_BFFreqIndex   => PFreqIndex(i+1),   -- out(10:0);
            o_BFBeam        => PBeam(i+1),        -- out(7:0);  
            o_BFvalid       => Pvalid(i+1),       -- out std_logic
            -- debug stuff
            i_badPacket     => badPacket(i),
            o_badPacket     => badPacket(i+1)
        );
        
    end generate;
    
    o_BFdata  <= Pdata(g_PST_BEAMS);              -- out (63:0);
    o_BFpacketCount <= PpacketCount(g_PST_BEAMS); -- out (39:0);
    o_BFBeam <= PBeam(g_PST_BEAMS);               -- out (7:0);
    o_BFFreqIndex <= PFreqIndex(g_PST_BEAMS);     -- out (10:0);
    o_BFvalid <= Pvalid(g_PST_BEAMS);             -- out std_logic
   
    process(i_BF_clk)
    begin
        if rising_Edge(i_BF_clk) then
            -- Notification of jones and polynomial status, just goes straight to the packetiser.
            -- These are set for the duration of the corner turn (53 ms)
            -- Providing there is a gap between corner turn frames a small delay line is fine 
            -- delay line is just to make timing easy to meet.
            Jones_status(0) <= i_jones_status; -- in std_logic_vector(1 downto 0);  bit 0 = used default, bit 1 = jones valid;
            poly_ok(0) <= i_poly_ok;           -- in std_logic_vector(1 downto 0);  the polynomials used are within their valid time range;
            
            jones_status(3 downto 1) <= jones_status(2 downto 0);
            poly_ok(3 downto 1) <= poly_ok(2 downto 0);
            
            o_BFjones_status <= jones_status(3);
            o_BFpoly_ok <= poly_ok(3);
        end if;
    end process;
    
    --------------------------------------------------------------------
    -- Logic to check for correct incrementing of the packet count
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            
            --dbg_BFdata  <= Pdata(g_PST_BEAMS);              -- out (63:0);
            dbg_BFpacketCount <= PpacketCount(g_PST_BEAMS); -- out (39:0);
            dbg_BFBeam <= PBeam(g_PST_BEAMS);               -- out (7:0);
            dbg_BFFreqIndex <= PFreqIndex(g_PST_BEAMS);     -- out (10:0);
            dbg_BFvalid <= Pvalid(g_PST_BEAMS);             -- out std_logic            
            
            dbg_BFvalid_del1 <= dbg_BFvalid;
            dbg_BFpacketCount_del1 <= dbg_BFpacketCount;
            
            dbg_BFvalid_del2 <= dbg_BFvalid_del1;
            dbg_BFpacketCount_del2 <= dbg_BFpacketCount_del1;
            
            -- At the first frequency channel and first beam, the framecount should increment
            if (dbg_BFvalid = '1' and dbg_BFvalid_del1 = '0' and unsigned(dbg_BFBeam) = 0 and unsigned(dbg_BFFreqIndex) = 0) then
                expected_packetCount <= expected_next_packetcount;
                expected_next_packetcount <= std_logic_vector(unsigned(dbg_BFpacketCount) + 1);
                expected_next_set <= '1';
                expected_set <= expected_next_set;
            end if;
            
            if dbg_BFvalid_del1 = '1' and dbg_BFvalid_del2 = '0' then
                if expected_packetCount /= dbg_BFpacketCount_del1 and expected_set = '1' then
                    bad_packetCount <= '1';
                else
                    bad_packetCount <= '0';
                end if;
            else
                bad_packetCount <= '0';
            end if;
            
            -- register the signals captured on the input bus
            dbg_i_flagged <= i_flagged(2 downto 0);
            dbg_i_fine <= i_fine(6 downto 0);
            dbg_i_coarse <= i_coarse(3 downto 0);
            dbg_i_firstStation <= i_firstStation;
            dbg_i_lastStation <= i_lastStation;
            dbg_i_timeStep <= i_timeStep(4 downto 0);
            dbg_i_virtualChannel <= i_virtualChannel(3 downto 0);
            dbg_i_packetCount <= i_packetCount(19 downto 0);
            dbg_i_outputPktOdd <= i_outputPktOdd;
            dbg_i_valid <= i_valid;
            
            dbg_i_phase_virtualChannel <= i_phase_virtualChannel(3 downto 0); --  : in std_logic_vector(9 downto 0);
            dbg_i_phase_timestep <= i_phase_timeStep(7 downto 0);
            dbg_i_phase_beam <= i_phase_beam(3 downto 0); --  : in std_logic_vector(3 downto 0);
            dbg_i_phase_valid <= i_phase_valid; --  in std_logic;
            dbg_i_phase_clear <= i_phase_clear; --  in std_logic; 
            
            
            
        end if;
    end process;

    
    
   debug_ila_gen : IF g_DEBUG_ILA GENERATE
        ila_BF_output: IF (g_PIPE_INSTANCE = 1) GENERATE
            
            -- ila_pst2, 96 bits wide, 64k deep
            ila_beamformer_output : ila_pst2
                port map (
                    clk  => i_BF_clk, --  IN STD_LOGIC;
                    
                    probe0(2 downto 0)   => dbg_i_flagged,
                    probe0(9 downto 3)   => dbg_i_fine,
                    probe0(13 downto 10) => dbg_i_coarse,
                    probe0(14)           => dbg_i_firststation,
                    probe0(15)           => dbg_i_lastStation,
                    probe0(20 downto 16) => dbg_i_timeStep,
                    probe0(24 downto 21) => dbg_i_virtualChannel,
                    probe0(44 downto 25) => dbg_i_packetCount,
                    probe0(45)           => dbg_i_outputPktOdd,
                    probe0(46)           => dbg_i_valid,
                    
                    probe0(50 downto 47) => dbg_i_phase_virtualChannel,
                    probe0(58 downto 51) => dbg_i_phase_timeStep,
                    probe0(62 downto 59) => dbg_i_phase_beam,
                    probe0(63)           => dbg_i_phase_valid,
                    probe0(64)           => dbg_i_phase_clear,
                    
                    probe0(80 downto 65) => dbg_BFpacketCount(15 downto 0),
                    probe0(84 downto 81) => dbg_BFBeam(3 downto 0),
                    probe0(90 downto 85) => dbg_BFFreqIndex(5 downto 0),
                    probe0(91)           => dbg_BFvalid,
                    probe0(92)           => bad_packetCount,
                    probe0(95 downto 93) => expected_packetCount(2 downto 0)
                );
            
--            ila_beamformer_output : ila_pst
--                port map (
--                    clk                     => i_BF_clk, --  IN STD_LOGIC;
                    
--                    probe0(63 downto 0)     => Pdata(2),
--                    probe0(71 downto 64)    => PBeam(2),
--                    probe0(82 downto 72)    => PFreqIndex(2),
--                    probe0(83)              => Pvalid(2),
                    
--                    probe0(84)              => badPacket(2),
--                    probe0(89 downto 85)    => BFtimeStep(1),
                    
--                    probe0(126 downto 90)   => PpacketCount(2)(36 downto 0),
--                    probe0(153 downto 127)  => ( others => '0') ,
--                    probe0(161 downto 154)  => PBeam(1),
--                    probe0(172 downto 162)  => PFreqIndex(1),
--                    probe0(173)             => Pvalid(1),
                    
--                    probe0(181 downto 174)  => BFfine(1),
--                    probe0(191 downto 182)  => BFCoarse(1)
--                );
            
--            ila_beamformer_output_2 : ila_pst
--                port map (
--                    clk                     => i_BF_clk, --  IN STD_LOGIC;
                    
--                    probe0(63 downto 0)     => Pdata(2),
--                    probe0(71 downto 64)    => PBeam(2),
--                    probe0(82 downto 72)    => PFreqIndex(2),
--                    probe0(83)              => Pvalid(2),
                    
--                    probe0(84)              => badPacket(2),
--                    probe0(89 downto 85)    => BFtimeStep(1),
                    
--                    probe0(126 downto 90)   => PpacketCount(2)(36 downto 0),
--                    probe0(153 downto 127)  => ( others => '0') ,
--                    probe0(161 downto 154)  => PBeam(1),
--                    probe0(172 downto 162)  => PFreqIndex(1),
--                    probe0(173)             => Pvalid(1),
                    
--                    probe0(181 downto 174)  => BFfine(1),
--                    probe0(191 downto 182)  => BFCoarse(1)
--                );
            
        END GENERATE;
    
    END GENERATE;    

            
end Behavioral;


