# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
# All rights reserved
import numpy as np
#import scipy.fftpack
#import scipy.signal

# PST filterbank in the beamformer generates 216 fine channels per SPS coarse channel 
# Parameters
#     256 point FFT
#     12x256 FIR taps
#     Oversampled by 4/3
#     Input bandwidth per channel = 925.925925 KHz  (781.25 KHz oversampled by 32/27)
#     Used output bandwidth per fine channel = 925.925925/256 = 3.6169 kHz
#     Total output bandwidth per channel = 4/3 * 925.925/256 = 4.82 kHz

# The central 216 fine channels are kept from the filterbank output.
# Due to the output oversampling, a new filterbank output is generated for every 192 input samples.

FINE_PER_COARSE = 216
"""Fine channels per coarse channel"""

FFT_LENGTH = 256
"""Number of points in the FFT"""

FIR_TAPS = 12
"""total number of FIR taps, in units of the fft size"""

OS_NUMERATOR = 4
OS_DENOMINATOR = 3

def rolling_window(samples: np.ndarray, filter_length: int, sample_step: int) -> np.ndarray:
    """
    Reshape sample stream into overlapped frames suitable for calculating an oversampled polyphase filterbank
    :param samples: vector of samples to be filtered
    :param filter_length: prototype filter length = fft length x fir taps
    :param sample_step: number of input samples per output frame = fft_length / oversampling ratio

    :return: matrix of overlapped sample frames
    """
    shape = samples.shape[:-1] + ((samples.shape[-1] - filter_length + 1) // sample_step + 1, filter_length)
    strides = (samples.strides[0] * sample_step,) + (samples.strides[-1],)
    return np.lib.stride_tricks.as_strided(samples, shape=shape, strides=strides)


class PolyphaseFilterBank:
    def __init__(
        self,
        fir_file,
        fft_length: int = FFT_LENGTH,
        fir_taps: int = FIR_TAPS,
        oversample_numerator: int = OS_NUMERATOR,
        oversample_denominator: int = OS_DENOMINATOR,
    ):
        """
        :param fir_file: text file containing FIR tap values (file object or str path)
        """
        # Load the FIR taps from a text file
        self.fir = np.loadtxt(fir_file)
        # number of points in the FFT
        self.fft_length = int(fft_length)
        # total number of FIR taps, in units of the fft size
        self.fir_taps = fir_taps
        # Check : have we got the correct number of fir taps ?
        if len(self.fir) != (self.fir_taps * self.fft_length):
            raise Exception("Wrong number of FIR taps.")

        self.oversample_numerator = oversample_numerator
        self.oversample_denominator = oversample_denominator
        self.sample_step = int(round(fft_length / (oversample_numerator / oversample_denominator)))

        self.clipped = False  # has clipping occurred?
        self.real_max = 0  # maximum value
        self.imag_max = 0  # maximum value


    def filter(
        self,
        din: np.ndarray,
        time_steps: int = None,
        derotate: bool = False,
        keep: int = FINE_PER_COARSE,
        filter_scale=512,
        fft_scale=128,
        saturate: bool = True,
        zero_pad: bool = True,
        zero_pad_length: int = 0,
    ) -> np.ndarray:
        """
        Apply Polyphase FilterBank (PFB) to the data in a 1-D numpy array.

        :param din: Input data to filter.
        :param time_steps: Number of output time samples to calculate, defaults to
        processing the full input data din
        :param derotate: enable derotation of the filterbank output for oversampled
        filterbanks (i.e. oversample numerator != oversample denominator)
        :param keep: Number of output frequency channels to keep, centered around DC.
        default value of 108 corresponds to the used portion of the channel for a 128
        point FFT, and 32/27 oversampling
        :param filter_scale: scaling factor to apply at the output of the FIR filter.
        :param fft_scale: scaling factor to apply at the output of the FFT.
        :param saturate: if True, limit the output to 16 bit integers.
        :param zero_pad: pads the front of the input data with zeros, as occurs in the
        firmware with preloading of data from the previous corner turn frame.
        :param zero_pad_length : Number of zeros to put at the front. Uses a default value if set to 0.

        :return: Result of applying PFB.
        """

        if zero_pad:
            if zero_pad_length > 0:
                preload_zeros = zero_pad_length
            else:
                # Zero pad the front of the data so that the data for the first fft
                # comes from the first self.sample_step samples of the input data
                preload_zeros = self.fft_length * self.fir_taps - self.sample_step
            din_padded = np.zeros(din.shape[0] + preload_zeros, dtype=np.complex64)
            din_padded[preload_zeros : (preload_zeros + din.shape[0])] = din
            din = din_padded

        if time_steps is None:
            # Calculate the number of time steps needed to use all the input data.
            preload_length = self.fft_length * self.fir_taps - self.sample_step
            time_steps = (len(din) - preload_length) // self.sample_step
        
        fir_mat = self.fir.reshape((self.fir_taps, self.fft_length))
        fir_input = rolling_window(din, self.fir_taps * self.fft_length, self.sample_step)
        fir_input = fir_input.reshape((time_steps, self.fir_taps, self.fft_length))
        fir_output = np.einsum("jk,ijk->ik", fir_mat, fir_input, dtype=np.complex128, optimize="greedy")
        
        # t1 = scipy.fft.fft(np.exp(2j * np.pi * np.arange(8) / 8))
        # todo: casting to np.complex64 may limit precision, but this is needed to keep output identical to original code before vectorising
        #dout = (scipy.fft.fft(fir_output / filter_scale, axis=-1, overwrite_x=True) / fft_scale).astype(np.complex64)
        dout = (np.fft.fft(fir_output / filter_scale, axis=-1) / fft_scale).astype(np.complex64)
        #dout = (scipy.fft.fft(fir_output / filter_scale, axis=-1) / fft_scale).astype(np.complex64)
        
        if derotate:
            f_range = np.arange(self.fft_length)
            t_range = np.arange(time_steps)
            fmat, tmat = np.meshgrid(f_range, t_range)
            dout = dout * np.exp(-1j * fmat * self.oversample_denominator / self.oversample_numerator * tmat * 2 * np.pi)

        # Select the central "keep" frequencies
        dout = np.fft.fftshift(dout, axes=1)
        fmin = int((self.fft_length / 2) - (keep / 2))
        fmax = int((self.fft_length / 2) + (keep / 2))
        dout = dout[:, fmin:fmax] / 16  # final scaling

        self.clipped = False
        self.real_max = np.max(np.abs(dout.real))
        self.imag_max = np.max(np.abs(dout.imag))
        if saturate:
            # check if clipping is going to occur.
            # We both check and clip so that we can leave a record, both of
            # whether clipping occurred (in self.clipped) and of the
            # maximum value prior to clipping (in self.real_max, self.imag_max)
            if (self.real_max > 32767) or (self.imag_max > 32767):
                self.clipped = True
                # limit data to 16 bits dynamic range, i.e. [-32768, 32767)
                np.clip(dout.real, -32768, 32767, out=dout.real)
                np.clip(dout.imag, -32768, 32767, out=dout.imag)

        return dout
