----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au) & Norbert Abel
-- 
-- Create Date: 13.03.2020 09:47:34
-- Module Name: ct_atomic_pst_in - Behavioral
-- Description: 
--  First stage corner turn (between LFAA ingest and the filterbanks) 
--  The corner turn takes data for all channels for some number of stations, buffers the data, 
--  and outputs data in bursts for each channel.
--
----------------------------------------------------------------------------------
-- Structure
-- ---------
-- This is the top level of the corner turn; it contains :
--  + Timing controls; i.e. when to start reading data out of the buffer.
--  + Registers
--  + Logic to write data to the HBM.
--  + corner turn readout.
--
--  The corner turn supports up to 1024 virtual channels. It is agnostic about what the virtual channels represent, e.g.
--    - 512 stations * 2 coarse channels
--    - 8 stations * 128 coarse channels
--
--  The corner turn uses 4 buffers of 256 MByte each.
--  Each buffer uses the first 256 MBytes in a 512MByte block. This is to ensure that 
--  reads and writes go to different memory interfaces in the u55, which is needed to handle 100G burst traffic.
--  
--  Each buffer is 256 MBytes. 
--   * 256 MBytes/1024 channels = 256 kBytes per channel
--     - Each LFAA packet is 8192 bytes, so 256 kbytes = 32 LFAA packets
--     - Each LFAA packet is 2.21184ms, so 32 LFAA packets = 70.779 ms
--   * Address of a packet within the buffer = (virtual_channel) * 256kbytes + packet_count
--     - i.e. byte address within a buffer has 
--          - bits 12:0 = byte within an LFAA packet (LFAA packets are 8192 bytes)
--          - bits 17:13 = packet count within the buffer (up to 32 LFAA packets per buffer)
--          - bits 27:18 = virtual channel
--   * The total number of SPS packets per buffer per virtual channel is 24
--     so there is a bit of spare space in each buffer, since the maximum possible is 32.
--   * 24 SPS packets is selected so that the number of time samples output
--     per buffer readout is 24*2048/192 = 256 = 8*(32 per beamformer output packet)
--   * 24 SPS packets also means that the 2nd corner turn can hold an entire frame in a 512 MByte buffer
--     (Note the 2nd corner turn uses 16+16 complex data + 4/3 oversampling instead of 32/27 oversampling)
--   
--  A shadow memory keeps track of which LFAA packets have been written to the memory.
--  (1 Gbyte)/(8192 bytes) = 2^30/2^13 = 2^17 = 131072 blocks.
--  1 ultraRAM = 32 kbytes = 262144 bits. So 1/2 an ultraRAM is used as the shadow memory.
--  
----------------------------------------------------------------------------------
-- Sequencing
--  On reset, the fsm uses the packet count for the first packet received (on "i_packetCount")
--  to determine which packets to expect. Thereafter, it follows the fastest advancing
--  packet count, providing it doesn't skip ahead multiple frames.
--  
--
----------------------------------------------------------------------------------
-- Default Numbers:
--  LFAA time samples = 1080 ns
--  LFAA bandwidth/coarse channel = 1/1080ns = 925.925 KHz
--  LFAA input blocks = 2048 time samples = 2.21184 ms
--  
--  PSS/PST output:
--   Output is in 64 sample blocks. For 24 LFAA blocks:
--   24 LFAA blocks = 24 * 2.2ms = 53.08416 ms
--   24 SPS packets = 24 * (2048/64) = 768 x 64 sample PSS output blocks
--   Preload samples = 256 * 11 = 44 * (64 sample output blocks)
--   64 sample output blocks per second = (1024 channels) * (768+44) / 59.719680 ms  = 15,663,580 (64 sample blocks/second)
--   Used clock cycles on the output bus (3 dual-pol channels per cycle) = (15663580/3) * 64 = 334,156,378
--   The PST filterbank is 4/3 oversampled, so it needs at least (4/3) * 64 clocks per output block = 86 clocks.
--   So required clock cycles on the output bus including padding for oversampling = (15663580/3 [blocks/second]) * (86 [clocks/block])  = 449.02 MHz
--   So for PSS/PST filterbanks running at 450 MHz
--     - Packets/second = 15663580  (note 64 time samples/packet)
--     - Clocks/second = 450000000
--     - Clocks/packet available = 86.187 (64 on, 22 off)
--
----------------------------------------------------------------------------------
-- Delays
--   Delays are specified by polynomial coefficients:
--
--  163840 Bytes (=160kBytes) total, first 80 kBytes are the first buffer, second 80 kBytes are the second buffer
--  Within that memory :
--    words 0 to 9 : Config for virtual channel 0, buffer 0 (see below for specification of contents)
--    words 10 to 19 : Config for virtual channel 1, buffer 0
--    ...
--    words 10230 to 10239 : Config for virtual channel 1023, first buffer
--    words 10240 to 20479 : Config for all 1024 virtual channels, second buffer
--  See comments in the "poly_eval.vhd" module for the definition of the 80 bytes for each virtual channel and buffer.
--
----------------------------------------------------------------------------------

library IEEE, ct_lib, common_lib, xpm;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library DSP_top_lib;
use DSP_top_lib.DSP_top_pkg.all;
USE ct_lib.pst_ct1_reg_pkg.ALL;
USE common_lib.common_pkg.ALL;
use xpm.vcomponents.all;

Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;

entity pst_ct1_top is
    port (
        -- clock (300 or 400 MHz)
        -- 
        i_shared_clk     : in std_logic;
        i_shared_rst     : in std_logic;
        -- Registers (uses the shared memory clock)
        i_saxi_mosi       : in  t_axi4_lite_mosi; -- MACE IN
        o_saxi_miso       : out t_axi4_lite_miso; -- MACE OUT
        i_poly_full_axi_mosi : in  t_axi4_full_mosi; -- => mc_full_mosi(c_pst_ct1_full_index),
        o_poly_full_axi_miso : out t_axi4_full_miso; -- => mc_full_miso(c_pst_ct1_full_index),
        -- other config (comes from LFAA ingest module).
        -- This should be valid before coming out of reset.
        i_rst               : in std_logic;   -- While in reset, process nothing.
        o_rst               : out std_logic;  -- Reset is now driven from the LFAA ingest module.
        o_validMemRstActive : out std_logic;  -- reset is in progress, don't send data; Only used in the testbench. Reset takes about 20us.
        -- Headers for each valid packet received by the LFAA ingest.
        -- LFAA packets are about 8300 bytes long, so at 100Gbps each LFAA packet is about 660 ns long. This is about 200 of the interface clocks (@300MHz)
        -- These signals use i_shared_clk
        i_virtualChannel : in std_logic_vector(15 downto 0); -- Single number which incorporates both the channel and station; this module supports values in the range 0 to 1023.
        i_packetCount    : in std_logic_vector(47 downto 0);
        i_totalChannels  : in std_logic_vector(11 downto 0); -- Total channels for the table currently being used in this module
        i_totalStations  : in std_logic_vector(11 downto 0);
        i_totalCoarse    : in std_logic_vector(11 downto 0);
        i_valid          : in std_logic;
        -- Select table in use 
        o_vct_table_select : out std_logic;
        ------------------------------------------------------------------------------------
        -- Data output, to go to the filterbanks.
        -- Data bus output to the Filterbanks
        -- 6 Outputs, each complex data, 8 bit real, 8 bit imaginary.
        FB_clk  : in std_logic;
        o_rfi_scale : out std_logic_vector(4 downto 0);  -- Comes from a register.
        o_sof   : out std_logic;   -- Start of frame, occurs for every new set of channels.
        o_sofFull : out std_logic; -- Start of a full frame, i.e. first virtual channel in a new 60ms block.
        o_data0  : out t_slv_8_arr(1 downto 0);
        o_data1  : out t_slv_8_arr(1 downto 0);
        -- From DSP_top_pkg.vhd: 
        --  .HDeltaP(15:0), .VDeltaP(15:0), HOffsetP(15:0), VOffsetP(15:0), 
        --  .frameCount(36:0), .virtualChannel(15:0), .valid;
        o_meta01 : out t_atomic_CT_pst_META_out; 
        o_data2  : out t_slv_8_arr(1 downto 0);
        o_data3  : out t_slv_8_arr(1 downto 0);
        o_meta23 : out t_atomic_CT_pst_META_out;
        o_data4  : out t_slv_8_arr(1 downto 0);
        o_data5  : out t_slv_8_arr(1 downto 0);
        o_meta45 : out t_atomic_CT_pst_META_out;
        o_valid  : out std_logic;
        -- o_bad_polynomials uses shared_clk; pass direct to corner turn 2.
        o_bad_polynomials : out std_logic;
        o_totalChannels   : out std_logic_vector(11 downto 0); -- Total channels for the table currently being used in readout
        o_totalStations   : out std_logic_vector(11 downto 0);
        o_totalCoarse     : out std_logic_vector(11 downto 0);
        -------------------------------------------------------------
        -- AXI bus to the shared memory. 
        -- This has the aw, b, ar and r buses (the w bus is on the output of the LFAA decode module)
        -- w bus - write data
        o_m01_axi_aw : out t_axi4_full_addr; -- write address bus : out t_axi4_full_addr (.valid, .addr(39:0), .len(7:0))
        i_m01_axi_awready : in std_logic;
        -- b bus - write response
        i_m01_axi_b  : in t_axi4_full_b;   -- (.valid, .resp); resp of "00" or "01" means ok, "10" or "11" means the write failed.
        -- ar bus - read address
        o_m01_axi_ar      : out t_axi4_full_addr; -- read address bus : out t_axi4_full_addr (.valid, .addr(39:0), .len(7:0))
        i_m01_axi_arready : in std_logic;
        -- r bus - read data
        i_m01_axi_r       : in  t_axi4_full_data;
        o_m01_axi_rready  : out std_logic
    );
    
    -- prevent optimisation across module boundaries.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of pst_ct1_top : entity is "yes";    
    
end pst_ct1_top;

architecture Behavioral of pst_ct1_top is
    
    constant g_SPS_PACKETS_PER_FRAME : integer := 24;
    
    -- Bus to communicate HBM addresses to the input buffer (ct_vfc_input_buffer) from the memory allocation module (ct_vfc_malloc)
    signal writePacketCount : std_logic_vector(31 downto 0);  -- Packet count from the packet header
    signal writeChannel : std_logic_vector(15 downto 0);  -- virtual channel from the packet header
    signal writePacketCountValid : std_logic;                      -- Goes high to indicate o_packet_count is valid, and stays high until a response comes back
    signal writeAddress : std_logic_vector(23 downto 0); -- Address to write this packet to, in units of 8192 bytes.
    signal writeOK : std_logic;                     -- Write address is valid; if low, then the packet should be dropped as it is either too early or too late.
    signal writeAddressValid : std_logic;
    
    -- register interface
    signal config_rw : t_config_rw;
    signal config_ro : t_config_ro;
    
    signal hbm_ready : std_logic;
    signal useNewConfig, useNewConfigDel1, useNewConfigDel2, loadNewConfig : std_logic := '0';
    
    signal validMemWriteAddr : std_logic_vector(16 downto 0);
    signal validMemWrEn : std_logic;
    signal validMemReadAddr : std_logic_vector(16 downto 0);
    signal validMemReadData : std_logic;
    
    signal output_count_in  : t_config_pst_output_count_ram_in;
    signal output_count_out : t_config_pst_output_count_ram_out;
    
    signal virtualChannel : std_logic_vector(15 downto 0);
    type input_fsm_type is (idle, start_divider, wait_divider, check_range_calc,
        check_range, dump_pre_latch_on, packet_early_or_late, 
        generate_aw, generate_aw_discard, check_advance_buffer, start_readout_calc0, start_readout_calc1, 
        start_readout_calc2, start_readout);
    signal input_fsm : input_fsm_type;
    
    signal AWFIFO_dout : std_logic_vector(31 downto 0);
    signal AWFIFO_empty : std_logic;
    signal AWFIFO_full : std_logic;
    signal AWFIFO_RdDataCount : std_logic_vector(9 downto 0);
    signal AWFIFO_WrDataCount : std_logic_vector(9 downto 0);
    signal AWFIFO_din : std_logic_vector(31 downto 0);
    signal AWFIFO_rst : std_logic;
    signal AWFIFO_wrEn : std_logic;
    signal awCount : std_logic_vector(3 downto 0) := "0000";
    signal sps_addr : std_logic_vector(31 downto 0) := (others => '0');
    
    signal validMemSetWrAddr : std_logic_vector(16 downto 0);
    signal validMemSetWrEn : std_logic;
    signal duplicate : std_logic;
    signal dataMissing : std_logic;
    signal missing_count, duplicate_count, early_or_late_count : std_logic_vector(31 downto 0) := (others => '0');
   -- signal NChannels : std_logic_vector(11 downto 0) := x"400";
    signal clocksPerPacket : std_logic_vector(15 downto 0);
    signal running : std_logic := '0';
    signal chan0, chan1, chan2 : std_logic_vector(9 downto 0);
    signal ok0, ok1, ok2 : std_logic := '0';
    signal validOut : std_logic;
    signal validOutDel : std_logic;
    signal outputCountAddr : std_logic_vector(9 downto 0);
    signal outputCountWrData : std_logic_vector(31 downto 0);
    signal outputCountRdDat : std_logic_vector(31 downto 0);
    signal outputCountWrEn : std_logic;
    type validBlocks_fsm_type is (idle, clear_all_start, clear_all_run, readChan0, readChan0Wait0, readChan0Wait1, 
        readChan0Wait2, writeChan0, readChan1, readChan1Wait0, readChan1Wait1, readChan1Wait2, writeChan1, 
        readChan2, readChan2Wait0, readChan2Wait1, readChan2Wait2, writeChan2);
    signal validBlocks_fsm : validBlocks_fsm_type := idle;
    signal meta01, meta23, meta45 : t_atomic_CT_pst_META_out;
    signal data0, data1, data2, data3, data4, data5 : t_slv_8_arr(1 downto 0);
    signal FBClk_rst : std_logic;
    signal haltPacketCountEqZero : std_logic;
    --signal table0Rd_dat, table1Rd_dat : std_logic_vector(31 downto 0);
    signal validMemRstActive : std_logic;
    signal AWFIFO_rst_del2, AWFIFO_rst_del1 : std_logic;
    
    component ila_beamData
    port (
        clk : in std_logic;
        probe0 : in std_logic_vector(119 downto 0)); 
    end component;
    
    signal valid_del1 : std_logic;
    signal input_packets : std_logic_vector(31 downto 0) := x"00000000";
    
    signal div_remainder : std_logic_vector(7 downto 0);    
    signal div_quotient : std_logic_vector(41 downto 0);
    signal div_valid, do_division : std_logic;
    signal sps_packet_count : std_logic_vector(47 downto 0);
    signal data_rst : std_logic;
    signal status : std_logic_vector(31 downto 0);
    signal pre_latch_on_count : std_logic_vector(31 downto 0);
    signal input_fsm_dbg : std_logic_vector(4 downto 0);
    signal current_wr_53ms_frame, current_rd_53ms_frame : std_logic_vector(39 downto 0) := (others => '0');
    signal waiting_to_latch_on, first_readout : std_logic;
    signal packet_count_in_buffer : std_logic_vector(7 downto 0);
    signal next_wr_53ms_frame, previous_wr_53ms_frame : std_logic_vector(39 downto 0);
    signal sps_eq_current, sps_eq_next, sps_eq_previous : std_logic;
    signal framecount_start, framecount_start_limited : std_logic_vector(7 downto 0);
    signal aw_overflow : std_logic;
    signal awfifo_hwm : std_logic_vector(9 downto 0);
    signal trigger_readout : std_logic;
    signal drop_packet : std_logic := '0';
    signal readoverflow, readOverflow_set : std_logic := '0';
    signal buffers_sent_count : std_logic_vector(31 downto 0);
    signal poly_addr : std_logic_vector(14 downto 0); 
    signal poly_rddata : std_logic_vector(63 downto 0);
    
    signal sps_53ms_frame : std_logic_vector(39 downto 0);
    signal rfi_scale : std_logic_vector(4 downto 0);
    
    signal FB_to_shared_send : std_logic := '0';
    signal FB_to_shared_rcv : std_logic := '0';
    signal cdc_dataIn : std_logic_vector(12 downto 0);
    signal cdc_dataOut : std_logic_vector(12 downto 0);
    signal FB_to_shared_req : std_logic;
    
    signal read_overflow_buffer_count, max_frame_jump, recent_clocks_between_readouts : std_logic_vector(31 downto 0);
    signal min_clocks_between_readouts, buffer_count_at_min_readout_gap, recent_packets_per_readout : std_logic_vector(31 downto 0);

    signal recent_packetCount, packetCount_diff, packetCount_diff_abs : std_logic_vector(47 downto 0);
    signal recent_packetCount_valid, packetCount_diff_check, packetCount_diff_check_del, packetCount_diff_check_del2, packetCount_diff_negative : std_logic := '0';
    signal packetCount_diff_abs_16bit : std_logic_vector(15 downto 0);
    
    signal trigger_readout_occurred : std_logic := '0';
    signal clocks_between_readouts : std_logic_vector(31 downto 0);
    signal packets_per_readout : std_logic_vector(31 downto 0);
    
    signal totalChannels, totalStations, totalCoarse : std_logic_vector(11 downto 0);
    signal ro_totalChannels, ro_totalStations, ro_totalCoarse : std_logic_vector(11 downto 0);
    signal table_select_change, table_select_del : std_logic;
    signal ro_dbg_status : std_logic_vector(31 downto 0);
    signal read_overflow_buffers_skipped : std_logic_vector(31 downto 0);
    
begin
    
    ------------------------------------------------------------------------------------
    -- CONFIG (TO/FROM MACE)
    ------------------------------------------------------------------------------------

    E_TOP_CONFIG : entity ct_lib.PST_ct1_reg
    port map (
        MM_CLK  => i_shared_clk, -- in std_logic;
        MM_RST  => i_shared_rst, -- in std_logic;
        SLA_IN  => i_saxi_mosi,  -- IN    t_axi4_lite_mosi;
        SLA_OUT => o_saxi_miso,  -- OUT   t_axi4_lite_miso;

        CONFIG_FIELDS_RW   => config_rw, -- OUT t_config_rw;
        CONFIG_FIELDS_RO   => config_ro, -- IN  t_config_ro;
        
        CONFIG_PST_OUTPUT_COUNT_IN => output_count_in,   -- IN  t_config_psspst_output_count_ram_in;
		CONFIG_PST_OUTPUT_COUNT_OUT => output_count_out  -- OUT t_config_psspst_output_count_ram_out
    );
    
    xpm_cdc_array_single_inst : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
        WIDTH => 5           -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => rfi_scale, -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
        dest_clk => FB_clk, -- 1-bit input: Clock signal for the destination clock domain.
        src_clk  => i_shared_clk,   -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in   => config_rw.rfi_scale(4 downto 0)  -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain.
    );
    
    process(fb_clk)
    begin
        if rising_edge(fb_clk) then
            o_rfi_scale <= rfi_scale;
        end if;
    end process;
    
    -----------------------------------------------------------------------
    -- Full AXI interface - write to the ultraRAM 
    -- Full axi to bram
    poly_axi_bram_inst : entity ct_lib.poly_axi_bram_wrapper
    port map (
        i_clk  => i_shared_clk,
        i_rst  => i_shared_rst,
        -------------------------------------------------------
         -------------------------------------------------------
        -- Block ram interface for access by the rest of the module
        -- Memory is 18432 x 8 byte words
        -- read latency 3 clocks
        i_bram_addr    => poly_addr, -- in std_logic_vector(14 downto 0); 
        o_bram_rddata  => poly_rddata, --  out std_logic_vector(63 downto 0);
        -------------------------------------------------------
        -- ARGs axi interface
        i_vd_full_axi_mosi => i_poly_full_axi_mosi, -- in  t_axi4_full_mosi
        o_vd_full_axi_miso => o_poly_full_axi_miso  -- out t_axi4_full_miso
    );
	
    -- Four buffers in the HBM. 
    -- Buffer 0 stores framecounts : 0, 4, 8,  12, 16, 20 ...
    -- Buffer 1 stores framecounts : 1, 5, 9,  13, 17, 21 ...
    -- Buffer 2 stores framecounts : 2, 6, 10, 14, 18, 22 ...
    -- Buffer 3 stores framecounts : 3, 7, 11, 15, 19, 23 ...
    -- This is for writing to the buffer. Equivalent count for reading would be behind by one.
    config_ro.frame_count_low <= current_wr_53ms_frame(31 downto 0);
    config_ro.frame_count_high(7 downto 0) <= current_wr_53ms_frame(39 downto 32);
    config_ro.frame_count_high(31 downto 8) <= (others => '0');
    -- 32 bit status counters
    config_ro.early_or_late_count <= early_or_late_count;
    config_ro.pre_latch_on_count <= pre_latch_on_count;
    config_ro.duplicates_count <= duplicate_count;
    config_ro.missing_count <= "0000" & missing_count(31 downto 4); -- Drop low 4 bits since each missing block is reported 16 times.
    config_ro.input_packets <= input_packets;
    config_ro.status <= status;
    config_ro.buffers_sent_count <= buffers_sent_count;
    -- registers to note if something terrible happened.
    config_ro.error_input_overflow <= aw_overflow; -- std_logic;
    config_ro.error_read_overflow <= readOverflow_set; -- std_logic;
    -- various status counters
    config_ro.read_overflow_buffer_count <= read_overflow_buffer_count; -- yaml description : buffers_sent_count at most recent occurrence of read_overflow
    config_ro.max_frame_jump <= max_frame_jump; -- yaml description : maximum value by which the SPS frame count has jumped from one packet to the next
    config_ro.recent_clocks_between_readouts <= recent_clocks_between_readouts; -- yaml description : Number of clocks between most recent two readout triggers
    config_ro.min_clocks_between_readouts <= min_clocks_between_readouts; -- yaml description : minimum number of clocks between readout triggers
    config_ro.buffer_count_at_min_readout_gap <= buffer_count_at_min_readout_gap; -- yaml description : buffers_sent_count at the time when min_clocks_between_readouts was most recently set
    config_ro.recent_packets_per_readout <= recent_packets_per_readout; -- yaml description : Number of SPS packets received between most recent two readout triggers
    config_ro.readout_dbg <= ro_dbg_status;
    config_ro.read_overflow_buffers_skipped <= read_overflow_buffers_skipped;
    o_rst   <= config_rw.full_reset;
    
    process(i_shared_clk)
    begin
        if rising_edge(i_shared_clk) then
            if data_rst = '1' then
                early_or_late_count <= (others => '0');
                pre_latch_on_count <= (others => '0');
                duplicate_count <= (others => '0');
                missing_count <= (others => '0');
                input_packets <= (others => '0');
                buffers_sent_count <= (others => '0');
                max_frame_jump <= (others => '0');
                recent_packetCount <= (others => '0');
                recent_packetCount_valid <= '0';
                recent_clocks_between_readouts <= (others => '0');
                min_clocks_between_readouts <= (others => '1');
                trigger_readout_occurred <= '0';
                buffer_count_at_min_readout_gap <= (others => '0');
                recent_packets_per_readout <= (others => '0');
                packets_per_readout <= (others => '0');
            else
                if valid_del1 = '1' then
                    input_packets <= std_logic_vector(unsigned(input_packets) + 1);
                end if;
                if input_fsm = packet_early_or_late then
                    early_or_late_count <= std_logic_vector(unsigned(early_or_late_count) + 1);
                end if;
                if input_fsm = dump_pre_latch_on then
                    pre_latch_on_count <= std_logic_vector(unsigned(pre_latch_on_count) + 1);
                end if;
                if duplicate = '1' then
                    duplicate_count <= std_logic_vector(unsigned(duplicate_count) + 1);
                end if;
                if dataMissing = '1' then
                    missing_count <= std_logic_vector(unsigned(missing_count) + 1);
                end if;
                if (trigger_readout = '1') then
                    buffers_sent_count <= std_logic_vector(unsigned(buffers_sent_count) + 1);
                end if;
                
                if trigger_readout = '1' then
                    trigger_readout_occurred <= '1';  -- wait for trigger_readout to first occur so we don't track time before first trigger.
                    clocks_between_readouts <= (others => '0');
                    if trigger_readout_occurred = '1' then
                        recent_clocks_between_readouts <= clocks_between_readouts;
                        if (unsigned(clocks_between_readouts) < unsigned(min_clocks_between_readouts)) then
                            min_clocks_between_readouts <= clocks_between_readouts;
                            buffer_count_at_min_readout_gap <= buffers_sent_count;
                        end if;
                    end if;
                elsif trigger_readout_occurred = '1' then
                    clocks_between_readouts <= std_logic_vector(unsigned(clocks_between_readouts) + 1);
                end if;

                if trigger_readout = '1' then
                    packets_per_readout <= (others => '0');
                    recent_packets_per_readout <= packets_per_readout;
                elsif valid_del1 = '1' then
                    packets_per_readout <= std_logic_vector(unsigned(packets_per_readout) + 1);
                end if;
                
                if i_valid = '1' then
                    recent_packetCount <= i_packetCount;
                    recent_packetCount_valid <= '1';
                end if;
                
                if i_valid = '1' and recent_packetCount_valid = '1' then
                    packetCount_diff <= std_logic_vector(unsigned(i_packetCount) - unsigned(recent_packetCount));
                    packetCount_diff_check <= '1';
                else
                    packetCount_diff_check <= '0';
                end if;
                
                if (signed(packetCount_diff) < 0) then
                    packetCount_diff_abs <= std_logic_vector(-signed(packetCount_diff));
                    packetCount_diff_negative <= '1';
                else
                    packetCount_diff_abs <= packetCount_diff;
                    packetCount_diff_negative <= '0';
                end if;
                packetCount_diff_check_del <= packetCount_diff_check;
                
                if (unsigned(packetCount_diff_abs) > 65535) then
                    packetCount_diff_abs_16bit <= x"ffff";
                else
                    packetCount_diff_abs_16bit <= packetCount_diff_abs(15 downto 0);
                end if;
                packetCount_diff_check_del2 <= packetCount_diff_check_del;
                
                if packetCount_diff_check_del2 = '1' then
                    if unsigned(packetCount_diff_abs_16bit) > unsigned(max_frame_jump) then
                        max_frame_jump <= x"0000" & packetCount_diff_abs_16bit;
                    end if;
                end if;
                
            end if;
            
            status(4 downto 0) <= input_fsm_dbg;
            status(5) <= running;
            -- low 2 bits of 53ms_frame select which 256 MByte piece of memory is being written to.
            status(7 downto 6) <= current_wr_53ms_frame(1 downto 0);
            status(8) <= first_readout;
            status(9) <= waiting_to_latch_on;
            status(10) <= aw_overflow;
            status(20 downto 11) <= awfifo_hwm;
            status(21) <= '0';
            status(22) <= readOverflow_set;
            status(31 downto 23) <= "000000000";
            
            if data_rst = '1' then
                aw_overflow <= '0';
                awfifo_hwm <= (others => '0'); -- high water mark for the aw fifo
                readOverflow_set <= '0';
                read_overflow_buffer_count <= (others => '0');
                read_overflow_buffers_skipped <= (others => '0');
            else
                if (i_valid = '1' and input_fsm /= idle) then
                    aw_overflow <= '1';
                end if;
                if (unsigned(awfifo_wrDataCount) > unsigned(awfifo_hwm)) then
                    awfifo_hwm <= awfifo_wrDataCount;
                end if;
                if readoverflow = '1' then
                    readOverflow_set <= '1';
                    read_overflow_buffer_count <= buffers_sent_count;
                    read_overflow_buffers_skipped <= std_logic_vector(unsigned(read_overflow_buffers_skipped) + 1);
                end if;
            end if;
            if waiting_to_latch_on = '0' and data_rst = '0' then
                running <= '1';
            else
                running <= '0';
            end if;
            
            framecount_start <= '0' & config_rw.framecount_start(6 downto 0);
            if (unsigned(framecount_start) > (g_SPS_PACKETS_PER_FRAME/2 - 1)) then
                -- Ensure we start before we get into the preload section for the next frame
                -- This also needs to be less than the threshold used to discard packets for being too far into the future.
                framecount_start_limited <= std_logic_vector(to_unsigned(g_SPS_PACKETS_PER_FRAME/2 - 2,8));  
            elsif (unsigned(framecount_start) < 4) then
                framecount_start_limited <= "00000100"; -- 4; Don't start until we are past the last word used in the previous frame.
            else
                framecount_start_limited <= framecount_start;
            end if;
            
        end if;
    end process;
    
    -----------------------------------------------------------------------------------------------------
    -- Processing of input headers (i_virtualChannel, i_packetCount, i_valid) to generate write addresses
    -----------------------------------------------------------------------------------------------------
    
    -- i_packet_count determines the location to write to in the HBM
    -- This repeats every 96 packets : 0-23 -> HBM buffer 0, 24-47 -> HBM buffer 1, 48-71 -> HBM buffer 2, 72-95 -> HBM buffer 3
    -- To find which frame and buffer the packet should go to, divide by 4*24=96
    div96 : entity ct_lib.pst_div96
    port map (
        i_clk  => i_shared_clk,
        -- Input
        i_din   => sps_packet_count, -- in (47:0); 
        i_valid => do_division, -- in std_logic;
        -- Output - XX clock latency
        o_quotient  => div_quotient,  -- out (41:0); block of 108 SPS packets this is within.
        o_remainder => div_remainder, -- out (7:0); Selects which HBM buffer this packet should go to.
        o_valid     => div_valid      -- out std_logic, about 35 clocks after i_valid
    );
    
    do_division <= '1' when input_fsm = start_divider else '0';
    
    process(i_shared_clk)
    begin
        if rising_edge(i_shared_clk) then
            
            o_validMemRstActive <= validMemRstActive;
            
            valid_del1 <= i_valid;
            
            -- The data path reset puts everything back to the default state,
            -- and causes the state machine to latch on to the incoming data stream
            -- when it is released.
            -- data_rst also clears all the status registers.
            data_rst <= i_rst;
            
            ------------------------------------------------------------------------
            -- Handle switchover of the virtual channel table
            table_select_del <= config_rw.table_select;
            if ((config_rw.table_select /= table_select_del) or data_rst = '1') then
                table_select_change <= '1';
            elsif input_fsm = idle then
                -- wait until we go to the idle state to clear table_select_change, since
                -- input_fsm picks up table_select_change in that state.
                table_select_change <= '0';
            end if;
            o_vct_table_select <= config_rw.table_select;
            ------------------------------------------------------------------------
            
            if data_rst = '1' then
                input_fsm_dbg <= "00000";
                input_fsm <= idle;
                -- After coming out of reset, always start writing in the first half of a buffer,
                -- so that we will have preload data.
                --
                --                 first writes 
                --                 after reset
                --                   |
                -- ... buf1   buf2   buf3   buf0  buf1 ...
                --                          |
                --                      Start of frame
                --                      will be read  
                --                      from here.
                --
                current_wr_53ms_frame <= (others => '0');
                current_rd_53ms_frame <= (others => '0');
                -- When "waiting_to_latch_on" packets are discarded until they
                -- fall into the correct window.
                waiting_to_latch_on <= '1';
                -- Hold off reading out anything until we are reading out 
                -- buffer 0 (i.e. the first buffer in the frame).
                first_readout <= '1';
                drop_packet <= '0';
                -- totalChannels, totalStations and totalCoarse come from the ingest module.
                -- These are valid for a given packet notification (i_valid = '1'), and indicate the
                -- values for the virtual channel table that was used for that notification (i.e. to lookup the virtual channel in i_virtualChannel)
                totalChannels <= i_totalChannels;
                totalStations <= i_totalStations;
                totalCoarse <= i_totalCoarse;
                clocksPerPacket <= config_rw.output_cycles;
                trigger_readout <= '0';
            else
                case input_fsm is
                    when idle =>
                        input_fsm_dbg <= "00001";
                        if table_select_change = '1' then
                            waiting_to_latch_on <= '1';
                            first_readout <= '1';
                        end if;
                        trigger_readout <= '0';
                        if i_valid = '1' then
                            sps_addr(27 downto 18) <= i_virtualChannel(9 downto 0);
                            -- which packet out of 27 SPS packets per corner turn frame (put in later after division finishes)
                            sps_addr(17 downto 13) <= "00000";
                            -- low 13 bits are zero, sps packets are 8192 byte aligned in HBM. 
                            sps_addr(12 downto 0) <= (others => '0');
                            -- top 2 bits select the HBM buffer (put in later).
                            -- bit 28 is always zero, we only use the low 256MB in each 512 MB piece of HBM
                            sps_addr(30 downto 28) <= "000";
                            -- 2 GByte total for the corner turn, so only address bits 30:0 are used.
                            sps_addr(31) <= '0';
                            
                            -- There are 24 SPS packets per corner turn frame.
                            -- There are 4 frame buffers (of 256 MBytes each), so
                            --  - Divide by 4x24 = 96; n = packet_count/96
                            --  - n = 0:23  => buffer 0, offset = n
                            --  - n = 24:47 => buffer 1, offset = n-27
                            --  - n = 48:71 => buffer 2, offset = n-54
                            --  - n = 72:96 => buffer 3, offset = n-81
                            sps_packet_count <= i_packetCount(47 downto 0); -- ct_frame_count is the input to the divider (/108)
                            -- divider finds sps_packet_count/81.
                            input_fsm <= start_divider;
                            totalChannels <= i_totalChannels;
                            totalStations <= i_totalStations;
                            totalCoarse <= i_totalCoarse;
                        end if;
                        awCount <= "0000";
                        AWFIFO_wrEn <= '0';
                    
                    when start_divider =>
                        input_fsm_dbg <= "00010";
                        trigger_readout <= '0';
                        AWFIFO_wrEn <= '0';
                        -- Divide ct_frame_count by 96, to get which of the 4 corner turn buffers
                        -- the packet should go into
                        input_fsm <= wait_divider;
                        
                    when wait_divider =>
                        input_fsm_dbg <= "00011";
                        trigger_readout <= '0';
                        AWFIFO_wrEn <= '0';
                        if div_valid = '1' then
                            -- Which of the 4 HBM buffers does the packet go to ? (2 bits)
                            if unsigned(div_remainder) < 24 then
                                sps_53ms_frame(1 downto 0) <= "00";
                                packet_count_in_buffer <= div_remainder;
                            elsif unsigned(div_remainder) < 48 then
                                sps_53ms_frame(1 downto 0) <= "01";
                                packet_count_in_buffer <= std_logic_vector(unsigned(div_remainder) - 24);
                            elsif unsigned(div_remainder) < 72 then
                                sps_53ms_frame(1 downto 0) <= "10";
                                packet_count_in_buffer <= std_logic_vector(unsigned(div_remainder) - 48);
                            else
                                sps_53ms_frame(1 downto 0) <= "11";
                                packet_count_in_buffer <= std_logic_vector(unsigned(div_remainder) - 72);
                            end if;
                            -- which group of 4 buffers is this packet a part of (42 bits)
                            -- Each buffer = 24 SPS packets = 24*2048*1080ns = 53.08416 ms
                            -- 40 bit value = 2^40 * 53.08416ms = 1850 years from SKA epoch
                            sps_53ms_frame(39 downto 2) <= div_quotient(37 downto 0);
                            input_fsm <= check_range_calc;
                        end if;
                    
                    when check_range_calc =>
                        input_fsm_dbg <= "00100";
                        trigger_readout <= '0';
                        AWFIFO_wrEn <= '0';
                        input_fsm <= check_range;
                    
                    when check_range =>
                        input_fsm_dbg <= "00101";
                        AWFIFO_wrEn <= '0';
                        trigger_readout <= '0';
                        -- Address bits 30:29 = select 512 MByte buffer = 60 ms block of data.
                        -- Address bit 28 = '0', always use the low 256 MBytes in the 512Mbyte block.
                        sps_addr(31) <= '0';   --
                        sps_addr(30 downto 29) <= sps_53ms_frame(1 downto 0);
                        sps_addr(28) <= '0';
                        -- sps_addr(27 downto 18) is the virtual channel, which is set in the idle state.
                        -- sps_addr(17 downto 13) is the packet count within the buffer, 0 to 26 (for g_SPS_PACKETS_PER_FRAME = 27)
                        sps_addr(17 downto 13) <= packet_count_in_buffer(4 downto 0);
                        
                        if waiting_to_latch_on = '1' or validMemRstActive = '1' then
                            -- if we are waiting to latch on to the data stream,
                            -- then check that the packet is not at the end of the buffer, to 
                            -- ensure that we get all the packets needed for preload in the next buffer.
                            if (validMemRstActive = '0' and unsigned(packet_count_in_buffer) < (g_SPS_PACKETS_PER_FRAME-3)) then
                                waiting_to_latch_on <= '0';
                                -- current 53ms frame is the index of the corner turn frame that
                                -- we are currently writing data into the HBM for.
                                -- The index is relative to the SPS epoch (either monthly
                                -- updates, or SKA-epoch ~= J2000, depending on the ICD version)
                                current_wr_53ms_frame <= sps_53ms_frame;
                            end if;
                            -- packet is always dropped here, only keep the final SPS packet in the first frame.
                            -- Since that is the only packet that is used for preload of the filterbanks.
                            drop_packet <= '1';
                            input_fsm <= dump_pre_latch_on;
                        elsif first_readout = '1' then
                            -- We've latched on, but haven't yet reached the final SPS packet in the frame,
                            -- so we may still need to drop the packet.
                            if (((sps_eq_current = '1') and unsigned(packet_count_in_buffer) = (g_SPS_PACKETS_PER_FRAME-1)) or
                                (sps_eq_next = '1')) then
                                -- The incoming SPS packet is in the part of the current 53ms frame used for preload of the next frame,
                                -- or it is part of the next frame.
                                input_fsm <= generate_aw;
                                drop_packet <= '0';
                            else
                                input_fsm <= dump_pre_latch_on;
                                drop_packet <= '1';
                            end if;
                        else
                            -- check the packet count is not too early or too late
                            if ((sps_eq_current = '1') or
                                ((sps_eq_next = '1') and (unsigned(packet_count_in_buffer) < (g_SPS_PACKETS_PER_FRAME/2))) or
                                ((sps_eq_previous = '1') and (unsigned(packet_count_in_buffer) > (g_SPS_PACKETS_PER_FRAME/2)))) then
                                -- the incoming SPS packet is either in the current 60ms (256 MByte) buffer, or
                                -- it is in the first half of the next buffer or the last half of the previous buffer. 
                                -- Write the packet into the buffer
                                input_fsm <= generate_aw;
                                drop_packet <= '0';
                            else
                                -- packet is too far into the future or the past, drop it.
                                input_fsm <= packet_early_or_late;
                                drop_packet <= '1';
                            end if;
                        end if;
                    
                    when dump_pre_latch_on =>
                        input_fsm_dbg <= "00110";
                        AWFIFO_wrEn <= '0';
                        trigger_readout <= '0';
                        input_fsm <= generate_aw_discard; 
                        -- Still need to generate write addresses even if we are discarding the packet.
                        -- Otherwise it will mess up the axi bus, since wdata bus is in the LFAA ingest module.
                        -- Write to a point past the end of the used part of the current buffer
                        -- (there is space for 32 SPS packets, but only 27 are used in the buffer)
                        sps_addr(31) <= '0';   --
                        sps_addr(30 downto 29) <= sps_53ms_frame(1 downto 0);
                        sps_addr(28) <= '0';
                        sps_addr(27 downto 18) <= (others => '0');  -- virtual channel, always use 0, discarded so doesn't matter.
                        sps_addr(17 downto 13) <= "11111"; -- SPS packet location 31, last used in the buffer is 26. 
                        sps_addr(12 downto 0) <= (others => '0');
                    
                    when packet_early_or_late =>
                        input_fsm_dbg <= "00111";
                        -- signal an error - packet was too early or too late.
                        AWFIFO_wrEn <= '0';
                        trigger_readout <= '0';
                        input_fsm <= generate_aw_discard; 
                        -- Still need to generate write addresses even if we are discarding the packet.
                        -- Otherwise it will mess up the axi bus, since wdata bus is in the LFAA ingest module.
                        -- Write to a point just past the end of the write window, where it will get overwritten
                        -- later when the real packet turns up.
                        sps_addr(31) <= '0';
                        sps_addr(30 downto 29) <= sps_53ms_frame(1 downto 0);
                        sps_addr(28) <= '0';
                        sps_addr(27 downto 18) <= (others => '0');
                        sps_addr(17 downto 13) <= "11111"; -- SPS packet location 31, last used in the buffer is 26. 
                        sps_Addr(12 downto 0) <= (others => '0');
                    
                    when generate_aw =>
                        input_fsm_dbg <= "01001";
                        trigger_readout <= '0';
                        -- Put the write addresses into the FIFO
                        -- Generates 16 write addresses, each 8 beats.
                        -- 16 writes * 8 beats * 64 bytes/beat = 8192 bytes
                        awFIFO_din(31 downto 13)    <= sps_addr(31 downto 13);
                        awFIFO_din(12)              <= awCount(0);
                        awFIFO_din(11 downto 1)     <= (others => '0');  -- each burst is 8 beats * 64 bytes = 512 bytes, so all writes are 512 byte aligned.
                        awFIFO_din(0) <= drop_packet; -- not used for the axi transaction, just used to avoid writing to the valid memory
                        awCount <= std_logic_vector(unsigned(awCount) + 1);
                        AWFIFO_wrEn <= '1';
                        if awCount = "0001" then
                            input_fsm <= check_advance_buffer;
                        end if;
                    
                    when generate_aw_discard =>
                        -- generate aw bus writes when discarding
                        input_fsm_dbg <= "01010";
                        trigger_readout <= '0';
                        -- Put the write addresses into the FIFO
                        -- Generates 16 write addresses, each 8 beats.
                        -- 16 writes * 8 beats * 64 bytes/beat = 8192 bytes
                        awFIFO_din(31 downto 13)    <= sps_addr(31 downto 13);
                        awFIFO_din(12)              <= awCount(0);
                        awFIFO_din(11 downto 1)     <= (others => '0');  -- each burst is 8 beats * 64 bytes = 512 bytes, so all writes are 512 byte aligned.
                        awFIFO_din(0) <= drop_packet; -- not used for the axi transaction, just used to avoid writing to the valid memory
                        awCount <= std_logic_vector(unsigned(awCount) + 1);
                        AWFIFO_wrEn <= '1';
                        if awCount = "0001" then
                            input_fsm <= idle;
                        end if;
                    
                    when check_advance_buffer =>
                        -- Do we need to move to the next buffer
                        input_fsm_dbg <= "01011";
                        AWFIFO_wrEn <= '0';
                        trigger_readout <= '0';
                        if (sps_eq_next = '1' and (unsigned(packet_count_in_buffer) >= unsigned(framecount_start_limited))) then
                            -- update current buffer to the next buffer, trigger readout 
                            current_wr_53ms_frame <= next_wr_53ms_frame;
                            
                            if first_readout = '1' then
                                -- don't do a readout the first time we advance the buffer, because
                                -- we won't have a full buffer of data.
                                first_readout <= '0';
                                input_fsm <= idle;
                            else
                                current_rd_53ms_frame <= current_wr_53ms_frame;
                                first_readout <= '0';
                                -- These are used in the readout, only update them at the start of the readout.
                                -- This uses the values from the most recent packet.
                                ro_totalChannels <= totalChannels;
                                ro_totalStations <= totalStations;
                                ro_totalCoarse <= totalCoarse;
                                
                                clocksPerPacket <= config_rw.output_cycles;
                                input_fsm <= start_readout_calc0;
                            end if;
                        else
                            input_fsm <= idle;
                        end if;
                    
                    when start_readout_calc0 =>
                        input_fsm_dbg <= "01100";
                        AWFIFO_wrEn <= '0';
                        trigger_readout <= '0';
                        input_fsm <= start_readout_calc1;
                        
                    when start_readout_calc1 =>
                        input_fsm_dbg <= "01101";
                        AWFIFO_wrEn <= '0';
                        trigger_readout <= '0';
                        input_fsm <= start_readout_calc2;
                        
                    when start_readout_calc2 =>
                        input_fsm_dbg <= "01110";
                        AWFIFO_wrEn <= '0';
                        trigger_readout <= '0';
                        input_fsm <= start_readout;
                        
                    when start_readout =>
                        input_fsm_dbg <= "01111";
                        AWFIFO_wrEn <= '0';
                        trigger_readout <= '1';
                        input_fsm <= idle;
                        
                    when others =>
                        input_fsm_dbg <= "11111";
                        trigger_readout <= '0';
                        AWFIFO_wrEn <= '0';
                        input_fsm <= idle;
                end case;
            end if;
            
            next_wr_53ms_frame <= std_logic_vector(unsigned(current_wr_53ms_frame) + 1);
            previous_wr_53ms_frame <= std_logic_vector(unsigned(current_wr_53ms_frame) - 1);
            
            -- pipeline calculations to improve timing
            -- input_fsm state "check_range_calc" exists to allow a cycle for these calculations to finish.
            if (sps_53ms_frame = next_wr_53ms_frame) then
                sps_eq_next <= '1';
            else
                sps_eq_next <= '0';
            end if;
            if (sps_53ms_frame = current_wr_53ms_frame) then
                sps_eq_current <= '1';
            else
                sps_eq_current <= '0';
            end if;
            if (sps_53ms_frame = previous_wr_53ms_frame) then
                sps_eq_previous <= '1';
            else
                sps_eq_previous <= '0';
            end if;

            ---------------------------------------------------
            AWFIFO_rst <= data_rst;
            
        end if;
    end process;
    
    -- FIFO for write addresses 
    -- Input to the fifo comes from "input_fsm". It is read as fast as addresses are accepted by the shared memory bus.
    fifo_aw_inst : xpm_fifo_sync
    generic map (
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "auto", -- String
        FIFO_READ_LATENCY => 1,     -- DECIMAL
        FIFO_WRITE_DEPTH => 512,     -- DECIMAL; Allow up to 32 outstanding write requests.
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 10,  -- DECIMAL
        READ_DATA_WIDTH => 32,      -- DECIMAL
        READ_MODE => "fwft",        -- String
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "0404", -- String  -- bit 2 and bit 10 enables write data count and read data count
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 32,     -- DECIMAL
        WR_DATA_COUNT_WIDTH => 10   -- DECIMAL
    )
    port map (
        almost_empty => open,     -- 1-bit output: Almost Empty : When asserted, this signal indicates that only one more read can be performed before the FIFO goes to empty.
        almost_full => open,      -- 1-bit output: Almost Full: When asserted, this signal indicates that only one more write can be performed before the FIFO is full.
        data_valid => open,       -- Need to set bit 12 of "USE_ADV_FEATURES" to enable this output. 1-bit output: Read Data Valid: When asserted, this signal indicates that valid data is available on the output bus (dout).
        dbiterr => open,          -- 1-bit output: Double Bit Error: Indicates that the ECC decoder detected a double-bit error and data in the FIFO core is corrupted.
        dout => AWFIFO_dout,      -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven when reading the FIFO.
        empty => AWFIFO_empty,    -- 1-bit output: Empty Flag: When asserted, this signal indicates that- the FIFO is empty.
        full => AWFIFO_full,      -- 1-bit output: Full Flag: When asserted, this signal indicates that the FIFO is full.
        overflow => open,         -- 1-bit output: Overflow: This signal indicates that a write request (wren) during the prior clock cycle was rejected, because the FIFO is full
        prog_empty => open,       -- 1-bit output: Programmable Empty: This signal is asserted when the number of words in the FIFO is less than or equal to the programmable empty threshold value.
        prog_full => open,        -- 1-bit output: Programmable Full: This signal is asserted when the number of words in the FIFO is greater than or equal to the programmable full threshold value.
        rd_data_count => AWFIFO_RdDataCount, -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates the number of words read from the FIFO.
        rd_rst_busy => open,      -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO read domain is currently in a reset state.
        sbiterr => open,          -- 1-bit output: Single Bit Error: Indicates that the ECC decoder detected and fixed a single-bit error.
        underflow => open,        -- 1-bit output: Underflow: Indicates that the read request (rd_en) during the previous clock cycle was rejected because the FIFO is empty.
        wr_ack => open,           -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => AWFIFO_WrDataCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,      -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO write domain is currently in a reset state.
        din => AWFIFO_din,        -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',     -- 1-bit input: Double Bit Error Injection
        injectsbiterr => '0',     -- 1-bit input: Single Bit Error Injection: 
        rd_en => i_m01_axi_awready, -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO. 
        rst => AWFIFO_rst,        -- 1-bit input: Reset: Must be synchronous to wr_clk.
        sleep => '0',             -- 1-bit input: Dynamic power saving- If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_shared_clk,   -- 1-bit input: Write clock: Used for write operation. wr_clk must be a free running clock.
        wr_en => AWFIFO_wrEn      -- 1-bit input: Write Enable: 
    );
    
    o_m01_axi_aw.valid <= not AWFIFO_empty; --  out std_logic;
    o_m01_axi_aw.addr  <= x"00" & AWFIFO_dout(31 downto 1) & '0';
    -- Number of beats in a burst -1; 
    -- 8 beats * 64 byte wide bus = 512 bytes per burst, so 16 bursts for a full LFAA packet of 8192 bytes.
    -- Warning : The "wlast" signal generated in the LFAA ingest module (in "LFAAProcess100G.vhd") assumes that this value is 7 (=8 beats per burst).
    o_m01_axi_aw.len   <= x"3F"; -- Update to 4096 bytes per transfer from 512. "00000111"; -- out std_logic_vector(7 downto 0); 
    
    -----------------------------------------------------------------------------------------------
    -- Valid memory keeps track of whether data has been written to each 8192 byte block in the shared memory.
    -- One valid bit for every 8192 bytes.
    -- 1Gbyte/8192 bytes = 2^30/2^13 = 2^17 bits
    -- 
    
    -- When the last write address goes for an LFAA packet, then we assume we are done writing and can set the bit in the valid memory.
    process(i_shared_clk)
    begin
        if rising_edge(i_shared_clk) then
            if (AWFIFO_empty = '0') and (i_m01_axi_awready = '1') and (AWFIFO_dout(12) = '1') and (AWFIFO_dout(0) = '0') then
                -- bit 0 of AWFIFO_dout indicates that the packet is being dropped.
                validMemSetWrEn <= '1';
                -- skip bit 28 which is unused since we only use the low 256MB of each 512MB piece of HBM
                validMemSetWrAddr <= AWFIFO_dout(30 downto 29) & AWFIFO_dout(27 downto 13); -- pre 512MB buffer change, this was AWFIFO_dout(29 downto 13);
            else
                validMemSetWrEn <= '0';
            end if;
        end if;
    end process;
    
    validmemInst : entity ct_lib.pst_ct1_valid
    port map (
        i_clk => i_shared_clk,
        i_rst => AWFIFO_rst,
        o_rstActive => validMemRstActive,
        -- Set valid
        i_setAddr   => validMemSetWrAddr,  -- in(16:0)
        i_setValid  => validMemSetWrEn,    -- in std_logic;
        o_duplicate => duplicate,          -- out std_logic;
        -- clear valid
        i_clearAddr => validMemWriteAddr,  -- in(16:0)
        i_clearValid => validMemWrEn,      -- in std_logic;
        -- Read contents
        i_readAddr => validMemReadAddr,    -- in(16:0)
        o_readData => validMemReadData     -- out std_logic;
    );
    
    -----------------------------------------------------------------------------------------------
    -- readout of a frame
    
    readout : entity ct_lib.pst_ct1_readout
    generic map (
        g_SPS_PACKETS_PER_FRAME => g_SPS_PACKETS_PER_FRAME
    ) port map (
        shared_clk => i_shared_clk, -- in std_logic; Shared memory clock
        i_rst      => AWFIFO_rst,
        -- input signals to trigger reading of a buffer
        i_ct_frame => current_rd_53ms_frame,  -- in(39:0); Corner turn frame from epoch, units of g_SPS_PACKETS_PER_FRAME*2048[samples/frame]*1080[ns/sample]
        i_readStart => trigger_readout,       -- in std_logic; Pulse to start readout from i_currentBuffer
        i_totalChannels => ro_totalChannels,  -- in(11:0); Total number of virtual channels to read out
        i_totalStations => ro_totalStations,
        i_totalCoarse => ro_totalCoarse,
        
        i_clocksPerPacket => clocksPerPacket, -- in(15:0);
        i_polyValidTime => config_rw.poly_valid_time, -- in(23:0); Number of corner turn frames polynomials are valid for.
        o_invalidPolyCount => config_ro.invalid_poly_count, -- out (15:0);  Count of polynomial evaluations that did not have a valid polynomial.
        -- Reading Coarse and fine delay info from the registers
        -- In the registers, word 0, bits 15:0  = Coarse delay, word 0 bits 31:16 = Hpol DeltaP, word 1 bits 15:0 = Vpol deltaP, word 1 bits 31:16 = deltaDeltaP
        o_delayTableAddr => poly_addr, -- out (14:0); -- 2 addresses per virtual channel, up to 1024 virtual channels
        i_delayTableData => poly_rdData, -- in (63:0); -- Data from the delay table with 3 cycle latency. 
        
        -- Read and write to the valid memory, to check the place we are reading from in the HBM has valid data
        o_validMemReadAddr => validMemReadAddr, -- out (16:0);  8192 bytes per LFAA packet, 1 GByte of memory, so 1Gbyte/8192 bytes = 2^30/2^13 = 2^17
        i_validMemReadData => validMemReadData, -- in std_logic;  read data returned 3 clocks later.
        o_validMemWriteAddr => validMemWriteAddr, -- out (16:0);  write always clear the memory (mark the block as invalid).
        o_validMemWrEn      => validMemWrEn,      -- out std_logic;
        
        -- Data output to the filterbanks
        FB_clk  => FB_clk,  -- in std_logic; Interface runs off shared_clk
        o_sof   => o_sof,   -- out std_logic; start of frame.
        o_sofFull => o_sofFull, -- out std_logic; -- start of a full frame, i.e. 60ms of data.
        
        o_HPol0 => data0,  -- out t_slv_8_arr(1 downto 0);
        o_VPol0 => data1,  -- out t_slv_8_arr(1 downto 0);
        o_meta0 => meta01, -- out t_CT1_META_out;
        
        o_HPol1 => data2,  -- out t_slv_8_arr(1 downto 0);
        o_VPol1 => data3,  -- out t_slv_8_arr(1 downto 0);
        o_meta1 => meta23, -- out t_CT1_META_out;
        
        o_HPol2 => data4,  -- out t_slv_8_arr(1 downto 0);
        o_VPol2 => data5,  -- out t_slv_8_arr(1 downto 0);
        o_meta2 => meta45, -- out t_CT1_META_out;
        
        o_valid => validOut, -- out std_logic;
        -- These 4 signals use shared_clk; pass direct to corner turn 2.
        o_bad_polynomials => o_bad_polynomials, -- out std_logic;
        o_totalChannels   => o_totalChannels,   -- out (11:0); Total channels for the table currently being used in readout
        o_totalStations   => o_totalStations,   -- out (11:0);
        o_totalCoarse     => o_totalCoarse,     -- out (11:0);
        -- AXI read address and data input buses
        -- ar bus - read address
        o_axi_ar      => o_m01_axi_ar,      -- out t_axi4_full_addr; -- read address bus : out t_axi4_full_addr (.valid, .addr(39:0), .len(7:0))
        i_axi_arready => i_m01_axi_arready, -- in std_logic;
        -- r bus - read data
        i_axi_r       => i_m01_axi_r,      -- in  t_axi4_full_data;
        o_axi_rready  => o_m01_axi_rready, -- out std_logic;
        -- errors and debug
        -- Flag an error; we were asked to start reading but we haven't finished reading the previous frame.
        o_readOverflow => readOverflow,       -- out std_logic -- pulses high in the shared_clk domain.
        o_Unexpected_rdata => open,   -- out std_logic -- data was returned from the HBM that we didn't expect (i.e. no read request was put in for it)
        o_dataMissing => dataMissing, -- out std_logic -- Read from a HBM address that we haven't written data to. Most reads are 8 beats = 8*64 = 512 bytes, so this will go high 16 times per missing LFAA packet.
        o_dbg_status => ro_dbg_status -- out (31:0)     
    );
    o_data0 <= data0;
    o_data1 <= data1;
    o_data2 <= data2;
    o_data3 <= data3;
    o_data4 <= data4;
    o_data5 <= data5;
    o_valid <= validOut;
    o_meta01 <= meta01;
    o_meta23 <= meta23;
    o_meta45 <= meta45;
    
    -- Get virtual channels back to the i_shared_clk clock domain.
    process(FB_clk)
    begin
        if rising_edge(FB_clk) then
            validOutdel <= validOut;
            if validOut = '1' and validOutdel = '0' then
                cdc_dataIn(9 downto 0) <= meta01.virtualChannel(9 downto 0);
                -- Check if the first sample in the packet is flagged, either data was missing or it is RFI
                if data0(0) = "10000000" then
                    cdc_dataIn(10) <= '0';
                else
                    cdc_dataIn(10) <= '1';
                end if;
                if data2(0) = "10000000" then
                    cdc_dataIn(11) <= '0';
                else
                    cdc_dataIn(11) <= '1';
                end if;
                if data4(0) = "10000000" then
                    cdc_dataIn(12) <= '0';
                else
                    cdc_dataIn(12) <= '1';
                end if;
                FB_to_shared_send <= '1';
            elsif FB_to_shared_rcv = '1' then
                FB_to_shared_send <= '0';
            end if;
        end if;
    end process;
    
    
    xpm_cdc_handshake_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 3,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 3,    -- DECIMAL; range: 2-10
        WIDTH => 13          -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => cdc_dataOut, -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => FB_to_shared_req, -- 1-bit output: Pulse high where dout_out is valid
        src_rcv => FB_to_shared_rcv,   -- 1-bit output: Acknowledgement from destination logic that src_in has been received. 
        dest_ack => '0',      -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_shared_clk,    -- 1-bit input: Destination clock.
        src_clk => FB_clk, -- 1-bit input: Source clock.
        src_in => cdc_dataIn,  -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => FB_to_shared_send  -- 1-bit input: Assertion of this signal allows the src_in bus to be synchronized to the destination clock domain. 
    );
    
    
    -- Everything on the same clock domain;
    process(i_shared_clk)
    begin
        if rising_edge(i_shared_clk) then
            AWFIFO_rst_del1 <= AWFIFO_rst;
            AWFIFO_rst_del2 <= AWFIFO_rst_del1;
            FBClk_rst <= AWFIFO_rst_del1 and (not AWFIFO_rst_del2);
        end if;
    end process;
    
    -- Count valid blocks output to the filterbanks for each channel
    process(i_shared_clk)
    begin
        if rising_edge(i_shared_clk) then
            --validOutdel <= validOut;
            
            outputCountRdDat <= output_count_out.rd_dat;
            
            -- fsm to go through and do read-modify-write for each of the output channels to count valid packets.
            if FBClk_rst = '1' then
                validBlocks_fsm <= clear_all_start;
            else
                case validBlocks_fsm is
                    when idle =>
                        if FB_to_shared_req = '1' then
                            chan0 <= cdc_dataOut(9 downto 0);
                            chan1 <= std_logic_vector(unsigned(cdc_dataOut(9 downto 0)) + 1);
                            chan2 <= std_logic_vector(unsigned(cdc_dataOut(9 downto 0)) + 2);
                            -- First sample in the packet is flagged,
                            -- either data was missing or it is RFI
                            ok0 <= cdc_dataOut(10);
                            ok1 <= cdc_dataOut(11);
                            ok2 <= cdc_dataOut(12);
                            validBlocks_fsm <= readChan0;
                        end if;
                        outputCountWrEn <= '0';
                        
                    when clear_all_start =>
                        outputCountAddr <= (others => '0');
                        outputCountWrData <= (others => '0');
                        outputCountWrEn <= '1';
                        validBlocks_fsm <= clear_all_run;
                        
                    when clear_all_run => 
                        outputCountAddr <= std_logic_vector(unsigned(outputCountAddr) + 1);
                        if outputCountAddr = "1111111111" then
                            validBlocks_fsm <= idle;
                            outputCountWrEn <= '0';
                        end if;
                        
                    when readChan0 =>
                        validBlocks_fsm <= readChan0Wait0;
                        outputCountAddr <= chan0;
                        outputCountWrEn <= '0';
                    
                    when readChan0Wait0 =>  -- address to the memory is correct for chan0 in this state
                        validBlocks_fsm <= readChan0Wait1;
                        outputCountWrEn <= '0';
                    
                    when readChan0Wait1 =>
                        validBlocks_fsm <= readChan0Wait2;
                        outputCountWrEn <= '0';
                    
                    when readChan0Wait2 =>
                        validBlocks_fsm <= writeChan0;
                        outputCountWrEn <= '0';
                    
                    when writeChan0 =>   -- read data for chan0 is valid in this state.
                        validBlocks_fsm <= readChan1;
                        outputCountWrData <= std_logic_vector(unsigned(outputCountRdDat) + 1);
                        outputCountWrEn <= ok0;
                        
                    when readChan1 =>
                        validBlocks_fsm <= readChan1Wait0;
                        outputCountWrEn <= '0';
                        outputCountAddr <= chan1;
                    
                    when readChan1Wait0 =>
                        validBlocks_fsm <= readChan1Wait1;
                        outputCountWrEn <= '0';
                    
                    when readChan1Wait1 =>
                        validBlocks_fsm <= readChan1Wait2;
                        outputCountWrEn <= '0';
                    
                    when readChan1Wait2 =>
                        validBlocks_fsm <= writeChan1;
                        outputCountWrEn <= '0';
                        
                    when writeChan1 =>
                        validBlocks_fsm <= readChan2;
                        outputCountWrData <= std_logic_vector(unsigned(outputCountRdDat) + 1);
                        outputCountWrEn <= ok1;
                    
                    when readChan2 =>
                        validBlocks_fsm <= readChan2Wait0;
                        outputCountWrEn <= '0';
                        outputCountAddr <= chan2;
                    
                    when readChan2Wait0 =>
                        validBlocks_fsm <= readChan2Wait1;
                        outputCountWrEn <= '0';
                    
                    when readChan2Wait1 =>
                        validBlocks_fsm <= readChan2Wait2;
                        outputCountWrEn <= '0';
                    
                    when readChan2Wait2 =>
                        validBlocks_fsm <= writeChan2;
                        outputCountWrEn <= '0';
                    
                    when writeChan2 =>
                        validBlocks_fsm <= idle;
                        outputCountWrData <= std_logic_vector(unsigned(outputCountRdDat) + 1);
                        outputCountWrEn <= ok2;
                    
                    when others =>
                        validBlocks_fsm <= idle;
                end case;
            end if;
        end if;
    end process;
    
    output_count_in.adr <= outputCountAddr;
    output_count_in.wr_dat <= outputCountWrData;
    output_count_in.wr_en <= outputCountWrEn;
    output_count_in.rd_en <= '1';
    output_count_in.clk <= i_shared_clk;
    output_count_in.rst <= '0';
    
    
end Behavioral;
