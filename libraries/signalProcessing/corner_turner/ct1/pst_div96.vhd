----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 08.08.2023 
-- Module Name: pst_div108 - Behavioral
-- Description: 
--   Divide by 96, returning both integer and remainder part.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library xpm;
use xpm.vcomponents.all;

entity pst_div96 is
    port (
        i_clk  : in std_logic;
        -- Input
        i_din  : in std_logic_vector(47 downto 0); 
        i_valid : in std_logic;
        -- Output, 35 clock latency
        o_quotient : out std_logic_vector(41 downto 0);
        o_remainder : out std_logic_vector(7 downto 0); 
        o_valid : out std_logic
    );
end pst_div96;

architecture Behavioral of pst_div96 is
    
    signal dividend, divisor : std_logic_vector(47 downto 0);
    signal quotient : std_logic_vector(41 downto 0);
    signal divCount : std_logic_vector(5 downto 0);
    signal divRunning : std_logic;
    
begin
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            
            if i_valid = '1' then
                dividend <= i_din;
                divisor <= "110000000000000000000000000000000000000000000000"; -- 48 bits; divisor = 96 * 2^41
                divCount <= "101010";  -- 42 steps in the division.
                divRunning <= '1';
                quotient <= (others => '0');
                o_valid <= '0';
            elsif divRunning = '1' then
                divCount <= std_logic_vector(unsigned(divCount) - 1);
                quotient(41 downto 1) <= quotient(40 downto 0);
                if (unsigned(dividend) >= unsigned(divisor)) then
                    dividend <= std_logic_vector(unsigned(dividend) - unsigned(divisor));
                    quotient(0) <= '1';
                else
                    quotient(0) <= '0';
                end if;
                divisor <= '0' & divisor(47 downto 1);
                if (unsigned(divCount) = 0) then
                    divRunning <= '0';
                end if;
                
                if (unsigned(divCount) = 0) then
                    divRunning <= '0';
                    o_valid <= '1';
                    o_quotient <= quotient;
                    o_remainder <= dividend(7 downto 0);
                else
                    o_valid <= '0';
                end if;
            else
                o_valid <= '0';
                o_quotient <= (others => '0');
                o_remainder <= (others => '0');
            end if;

        end if;
    end process;
    
end Behavioral;

