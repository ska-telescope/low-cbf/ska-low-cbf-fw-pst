----------------------------------------------------------------------------------
-- Company: CSIRO 
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 19.03.2024
-- Module Name: ct2_poly_eval - Behavioral
-- Description: 
--   Polynomial evaluation for PST beamformer.
-- 
-- Data output :
--  The data output needs to match the order it is expected in the beamformer :
--
--    For timeGroup = 0:(<time samples per corner turn>/32 - 1) -- 53 ms corner turn, So 0:(256/32-1) = 0:7
--       For Coarse = 0:(i_coarse-1)                            -- For 512 stations, 2 coarse, this is 0:1
--          For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
--             For Station = 0:(i_stations-1)
--                For fine_offset = 0:3:213                     -- Total of 216 fine channels per SPS channel
--                   Send a 192 bit word (i.e. 3 fine channels x (2 pol) x (16+16 bit complex data)).
--
--  This means the delays need to be generated in the following order :
--
--    For timeGroup = 0:(<time samples per corner turn>/32 - 1) -- 53 ms corner turn, So 0:(256/32-1) = 0:7
--       For Coarse = 0:(i_coarse-1)                            -- For 512 stations, 2 coarse, this is 0:1
--          For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
--
--             Note : Minimum gap between steps
--             in "Time" is 72 clocks (occurs for the case where there is only a single station)
--
--             For Station = 0:(i_stations-1)                   -- Up to 512 stations
--                For beam = 0:(i_beams-1)                      -- Up to 16 beams
--                
--
-- Calculations :
--   - Which set of polynomials to use ? (compare i_ct_frame with i_poly_buffer0_valid_frame etc)
--   - Determine the time to use for "t" in the polynomial.
--      - To calculate t :
--          - Find corner turn frames from start of validity 
--              ct_offset_frames = i_ct_frame - i_poly_buffer{0,1}_valid_frame
--              ct_offset_nanoseconds = (ct_offset_frames * 53084160)
--              ct_offset_epoch =  ct_offset_nanoseconds + i_poly_buffer{0,1}_offset_ns
--
--          - For each new time sample within a corner turn frame, 
--              add (192*1080ns) = 207360 ns
--              ct_offset_epoch = ct_offset_epoch + 207360
--      - t calculation uses integer nanoseconds precision
--          - Single precision cannot work - not enough bits to accurately add corner turn frame length after a few minutes 
--      - Convert t to floating point t_ns = double(ct_offset_epoch) 
--      - get t_seconds = double(1e-9) * t_ns
--      - Compute powers of t_seconds using double precision multiplier
--      - Convert to single precision
--
--
----------------------------------------------------------------------------------
library IEEE, axi4_lib, common_lib, bf_lib, xpm, ct_lib;
use xpm.vcomponents.all;
use IEEE.STD_LOGIC_1164.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;


entity ct2_poly_eval is
    port(
        i_BF_clk : in std_logic;
        -------------------------------------------------------------------------------
        -- Control registers
        i_poly_buffer0_valid_frame : in std_logic_vector(47 downto 0);
        i_poly_buffer0_valid_duration : in std_logic_vector(31 downto 0);
        i_poly_buffer0_offset_ns : in std_logic_vector(31 downto 0);
        i_poly_buffer0_valid : in std_logic;
        i_poly_buffer1_valid_frame : in std_logic_vector(47 downto 0);
        i_poly_buffer1_valid_duration : in std_logic_vector(31 downto 0);
        i_poly_buffer1_offset_ns : in std_logic_vector(31 downto 0);
        i_poly_buffer1_valid : in std_logic;
        i_beams : in std_logic_vector(7 downto 0); -- number of beams to evaluate
        i_stations : in std_logic_vector(9 downto 0);  -- Number of stations
        i_coarse : in std_logic_vector(9 downto 0);    -- Number of coarse channels
        -------------------------------------------------------------------------------
        -- polynomial configuration memory interface
        ----------------------
        -- Virtual channel map
        -- 4 clock read latency
        -- Address is the virtual channel
        o_vcmap_RdAddr : out std_logic_vector(9 downto 0);
        -- Data returned : bits (31:0) = FP32 sky frequency in GHz for the lowest frequency fine channel in the coarse channel.
        --                 bits (40:32) = station number (0 to 511), used to look up the polynomial coefficients
        i_vcmap_rdData : in std_logic_vector(63 downto 0);
        ----------------------
        -- Polynomial coefficient table
        -- 14 clock read latency (chained ultraRAMs, 12 deep to get 49152 = 12 * 4096) 
        -- Address = (buffer * 24576) + (beam * 1536) + (station * 3) + coef
        --  Where buffer = 0 or 1, as determined from i_poly_buffer0_valid_frame etc.
        --        beam = 0 to 15,
        --        station = 0 to 511
        --        coef = 0 to 2, where 0 : bits (31:0) = c0, bits (63:32) = c1
        --                             1 : bits (31:0) = c2, bits (63:32) = c3
        --                             2 : bits (31:0) = c4, bits (63:32) = c5
        -- c0 to c5 are FP32 values for the delay polynomial :
        --  c0 + c1*t + c2 * t^2 + c3 * t^3 + c4 * t^4 + c5 * t^5
        o_polymem_RdAddr : out std_logic_vector(15 downto 0);
        i_polymem_rdData : in std_logic_vector(63 downto 0);
        --------------------------------------------------------------------------------
        -- Polynomial results output
        i_poly_eval : in std_logic;  -- Initiate polynomial evaluation for a corner turn frame, i.e. 256 time steps, all beams, all virtual channels 
        i_ct_frame : in std_logic_vector(36 downto 0);  -- Which corner turn frame to use 
        o_phase_virtualChannel : out std_logic_vector(9 downto 0);
        o_phase_timeStep : out std_logic_vector(7 downto 0);
        o_phase_beam : out std_logic_vector(3 downto 0);  -- 0 to 15
        o_phase : out std_logic_vector(23 downto 0);      -- Phase at the start of the coarse channel.
        o_phase_step : out std_logic_vector(23 downto 0); -- Phase step per fine channel.
        o_phase_valid : out std_logic;
        o_poly_ok : out std_logic;     -- Current time is between start and end validity times for the polynomial buffer in use
        i_stop : in std_logic          -- Data delivered to a FIFO, FIFO near full so stop sending stuff.
    );
end ct2_poly_eval;

architecture Behavioral of ct2_poly_eval is
    
    constant c_FINE_STEP_GHz : std_logic_vector := x"3672b9d6"; -- = 3.616898148e-6 = Frequency step between fine channels in GHz, as a 32-bit floating point value.
    constant c_fp32_unity : std_logic_vector := x"3F800000"; -- = 1 as a single precision floating point value. 
    
    ---------------------------------------------------------------
    -- Single precision multiply-add, 
    -- 16 clock latency
    component single_AxB_plusC
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axis_c_tvalid : IN STD_LOGIC;
        s_axis_c_tdata  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_result_tvalid : OUT STD_LOGIC;
        m_axis_result_tdata  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
    end component;
    
    -- single precision adder
    -- 11 clock latency
    component single_A_plus_B
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_result_tvalid : OUT STD_LOGIC;
        m_axis_result_tdata  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
    end component;
    
    -- single precision multiplier, 8 clock latency
    component fp32_mult
    port (
        aclk : in std_logic;
        s_axis_a_tvalid : in STD_LOGIC;
        s_axis_a_tdata : in STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axis_b_tvalid : in STD_LOGIC;
        s_axis_b_tdata : in STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_result_tvalid : out STD_LOGIC;
        m_axis_result_tdata : out STD_LOGIC_VECTOR(31 DOWNTO 0));
    end component;
    
    -- single precision to 32.24 fixed point
    -- 6 clock latency
    component fp32_to_fixed
    port (
        aclk : in STD_LOGIC;
        s_axis_a_tvalid : in STD_LOGIC;
        s_axis_a_tdata : in STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_result_tvalid : out STD_LOGIC;
        m_axis_result_tdata : out STD_LOGIC_VECTOR(55 DOWNTO 0));
    end component;
    
    signal times_fifo_rst, trigger_poly_time_start, poly_time_idle : std_logic;
    type time_eval_fsm_type is (start, reset_fifo, wait_buf_sel, trigger_times_gen, 
        wait_for_idle0, wait_for_idle1, wait_for_idle2, wait_for_idle, update_loop, wait_fifo_space, done);
    signal time_eval_fsm : time_eval_fsm_type := done;
    signal time_eval_total_coarse_minus1, time_eval_coarse : std_logic_vector(9 downto 0);
    signal time_eval_timegroup : std_logic_vector(2 downto 0);
    signal times_fifo_rd_count : std_logic_vector(6 downto 0);
    signal time_eval_ct_frame : std_logic_vector(47 downto 0);
    
    signal poly_eval_beams : std_logic_vector(7 downto 0); -- number of beams to evaluate
    signal poly_eval_stations : std_logic_vector(9 downto 0);  -- Number of stations
    signal poly_eval_coarse : std_logic_vector(9 downto 0);
    signal t_fifo_rdEn : std_logic := '0';
    signal t1, t2, t3, t4, t5 : std_logic_vector(31 downto 0);
    signal poly_t1, poly_t2, poly_t3, poly_t4, poly_t5 : std_logic_vector(31 downto 0);
    
    type poly_eval_fsm_type is (check_t_fifo, skip_read_times, read_times_fifo, wait_output_fifo, read_c1c0, read_c3c2, read_c5c4, update_loop, done);
    signal poly_eval_fsm : poly_eval_fsm_type := done;
    type poly_eval_fsm_del_t is array(63 downto 0) of poly_eval_fsm_type;
    signal poly_eval_fsm_del : poly_eval_fsm_Del_t := (others => done);
    
    signal poly_eval_stations_minus1, poly_Eval_coarse_minus1 : std_logic_vector(9 downto 0);
    signal poly_beam : std_logic_vector(3 downto 0) := "0000";
    signal poly_time : std_logic_vector(7 downto 0);
    signal poly_time_del : t_slv_8_arr(58 downto 0);
    signal poly_coarse : std_logic_vector(9 downto 0);
    signal poly_station, poly_vc : std_logic_vector(9 downto 0);
    
    signal polymem_RdAddr0, polymem_RdAddr1 : std_logic_vector(15 downto 0);
    signal poly_beam_x1024, poly_beam_x512, polymem_base, polymem_coef_offset, poly_station_x2, poly_station_x1 : std_logic_vector(15 downto 0);
    signal buf_sel : std_logic;
    signal mAdd1_dinB, mAdd2_dinB, mAdd1_dinC, mAdd2_dinC : std_logic_vector(31 downto 0);
    signal madd1_dout, madd2_dout : std_logic_vector(31 downto 0);
    
    signal fine_step_rotations, fine_total_rotations, final_delay_ns, coarse_freq_GHz : std_logic_vector(31 downto 0);
    signal fine_step_phase_int, fine_total_phase_int : std_logic_vector(55 downto 0);
    signal poly_time_base_ns : std_logic_vector(63 downto 0);
    signal vcmap_RdAddr_del : t_slv_10_arr(58 downto 0);
    signal poly_beam_del : t_slv_4_arr(58 downto 0);
    signal buf_sel_done : std_logic;
    
    signal polymem_rdData : std_logic_vector(63 downto 0);
    signal last_poly_coarse, last_poly_station, poly_time_eq_31 : std_logic;
    
begin
    
    -------------------------------------------------------------------------
    -- Select which polynomial buffer to use
    -- This happens once per corner turn frame (i.e. every 53.08416 ms)
    
    buffer_seli : entity ct_lib.ct2_buffer_select
    port map(
        clk => i_BF_clk, --  in std_logic;
        --------------------------------------------------------------------
        -- Configuration from the registers
        i_poly_buffer0_valid_frame => i_poly_buffer0_valid_frame,       -- in (47:0);
        i_poly_buffer0_valid_duration => i_poly_buffer0_valid_duration, -- in (31:0);
        i_poly_buffer0_offset_ns => i_poly_buffer0_offset_ns,           -- in (31:0);
        i_poly_buffer0_valid => i_poly_buffer0_valid,                   -- in std_logic;
        i_poly_buffer1_valid_frame => i_poly_buffer1_valid_frame,       -- in (47:0);
        i_poly_buffer1_valid_duration => i_poly_buffer1_valid_duration, -- in (31:0);
        i_poly_buffer1_offset_ns => i_poly_buffer1_offset_ns,  -- in (31:0);
        i_poly_buffer1_valid => i_poly_buffer1_valid,          -- in std_logic;
        ---------------------------------------------------------------------
        -- start getting times from (i_ct_frame + i_timeGroup)
        -- Time group within the corner turn frame. Each time group is 32 time samples = 32*192*1080ns = 6635520 ns
        i_start => i_poly_eval,   -- in std_logic;
        i_ct_frame => i_ct_frame, -- in (36:0);
        --
        o_valid => buf_sel_done,  -- out std_logic;
        o_buf_sel => buf_sel,     -- out std_logic; Which polynomial buffer to use
        o_buf_valid => o_poly_ok, -- out std_logic; Current frame count is within the range of valid frame counts for the polynomial buffer selected.
        o_poly_time_ns => poly_time_base_ns  -- out (63:0), time to use in the polynomial
    );
    
    -------------------------------------------------------------------------
    -- Time evaluation 
    --  - Gets the "t" to use in the polynomial.
    -- 
    polytimei : entity ct_lib.ct2_poly_time
    port map (
        clk => i_BF_clk, -- in std_logic
        ---------------------------------------------------------------------
        -- start getting times from (i_ct_frame + i_timeGroup)
        -- Time group within the corner turn frame. Each time group is 32 time samples = 32*192*1080ns = 6635520 ns
        i_start => trigger_poly_time_start,  -- in std_logic;
        i_timeGroup => time_eval_timegroup,  -- in (2:0);   -- 8 time groups per frame, 32 step per group = 256 time samples per frame.
        i_time_base_ns => poly_time_base_ns, -- in (63:0);
        o_idle => poly_time_idle, -- 
        ---------------------------------------------------------------------
        -- Output times as single precision floating point values
        o_t1 => t1, -- out (31:0); time used in the polynomial in seconds
        o_t2 => t2, -- out (31:0); time squared
        o_t3 => t3, -- out (31:0); time cubed
        o_t4 => t4, -- out (31:0); time to the fourth
        o_t5 => t5, -- out (31:0); time to the fifth
        o_step => open, -- out (7:0); time sample within the frame for the current outputs, 0 to 255.
        o_t_valid => open, -- out std_logic; We only read if there is data in the fifo, so no need to check valid.
        i_fifo_rdEn => t_fifo_rdEn, -- in std_logic; -- outputs valid one clock after i_fifo_RdEn, if there is data in the fifo.
        i_fifo_rst => times_fifo_rst, --  in std_logic;
        o_fifo_rd_count => times_fifo_rd_count -- out (6:0); Number of words in the FIFO
    );
    
    
    
    -- State machine to control time evaluation
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            
            -- Step through the order that time data is needed :
            --    For timeGroup = 0:(<time samples per corner turn>/32 - 1) -- 53 ms corner turn, So 0:(256/32-1) = 0:7
            --       For Coarse = 0:(i_coarse-1)                            -- For 512 stations, 2 coarse, this is 0:1
            --          For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
            --
            if i_poly_eval = '1' then
                time_eval_fsm <= start;
                time_eval_total_coarse_minus1 <= std_logic_vector(unsigned(i_coarse) - 1);
                time_eval_ct_frame <= "00000000000" & i_ct_frame;
            else
                case time_eval_fsm is
                    when start =>
                        time_eval_timegroup <= "000";
                        time_eval_coarse <= (others => '0');
                        if poly_time_idle = '1' then
                            time_eval_fsm <= reset_fifo;
                        end if;
                        
                    when reset_fifo =>
                        time_eval_fsm <= wait_buf_sel;
                        
                    when wait_buf_sel =>
                        if buf_sel_done = '1' then
                            time_eval_fsm <= trigger_times_gen;
                        end if;
                        
                    when trigger_times_gen =>
                        time_eval_fsm <= wait_for_idle0;
                    
                    when wait_for_idle0 =>
                        time_eval_fsm <= wait_for_idle1;
                    
                    when wait_for_idle1 =>
                        time_eval_fsm <= wait_for_idle2;
                    
                    when wait_for_idle2 =>
                        time_eval_fsm <= wait_for_idle;
                    
                    when wait_for_idle =>
                        if poly_time_idle = '1' then
                            time_eval_fsm <= update_loop;
                        end if;
                        
                    when update_loop =>
                        if (unsigned(time_eval_coarse) >= unsigned(time_eval_total_coarse_minus1)) then
                            time_eval_coarse <= (others => '0');
                            if time_eval_timegroup = "111" then
                                time_eval_fsm <= done;
                            else
                                time_eval_timegroup <= std_logic_vector(unsigned(time_eval_timegroup) + 1);
                                time_eval_fsm <= wait_fifo_space;
                            end if;
                        else
                            time_eval_coarse <= std_logic_vector(unsigned(time_eval_coarse) + 1);
                            time_eval_fsm <= wait_fifo_space;
                        end if;
                        
                    when wait_fifo_space =>
                        if unsigned(times_fifo_rd_count) < 31 then
                            time_eval_fsm <= trigger_times_gen;
                        end if; 
                         
                    when done =>
                        time_eval_fsm <= done;
                        
                    when others =>
                        time_eval_fsm <= done;
                end case;
            end if;
            
            if time_eval_fsm = reset_fifo then
                times_fifo_rst <= '1';
            else
                times_fifo_rst <= '0';
            end if;
            
            if (time_eval_fsm = trigger_times_gen) then
                trigger_poly_time_start <= '1';
            else
                trigger_poly_time_start <= '0';
            end if;
            
        end if;
    end process;
    

    
    -------------------------------------------------------------------------
    -- Polynomial evaluation
    -- Pipelined :
    --
    -- delay_ns = c5*t^5 + c4*t^4 + c3*t^3 + c2*t^2 + c1*t + c0
    --
    -- Two fused multiply-add blocks, "mAdd1" and "mAdd2", also an adder "add3"
    -- (1) mAdd1 : calculate c1*t + 0          mAdd2 : c0*1 + 0                <--- poly_eval_fsm = read_c1c0
    --   -- 16 cycle latency --
    -- (2) mAdd1 : c3*t^3 + (mAdd1 output)     mAdd2 : c2*t^2 + (mAdd2 output) <--- poly_eval_fsm = read_c3c2
    --           = c3*t^3 + c1*t                     = c2*t^2 + c0
    --   -- 16 cycle latency --
    -- (3) mAdd1 : c5*t^5 + (mAdd1 output)     mAdd2 : c4*t^4 + (mAdd2 output) <--- poly_eval_fsm = read_c5c4
    --           = c5*t^5 + c3*t^3 + c1*t            = c4*t^4 + c2*t^2 + c0
    --   -- 16 cycle latency --
    -- (4) add3 : (mAdd1 output) + (mAdd2 output)                              <--- follows on from the read_c5c4 state, after a 16 cycle latency
    --          = (c5*t^5 + c3*t^3 + c1*t) + (c4*t^4 + c2*t^2 + c0)
    --          = c5*t^5 + c4*t^4 + c3*t^3 + c2*t^2 + c1*t + c0
    --          = Delay in nanoseconds
    --   -- 11 cycle latency --
    -- (5) f_mult1, f_mult2 : Outputs to be generated :                        <--- follows on from the add3, i.e. follows read_c5c4 state, after 27 cycle latency
    --   o_phase : 24 bit phase at the start of the coarse channel.
    --   o_phase_step : 24 bit phase step per fine channel.
    --   -- 8 cycle latency -- 
    -- (6) conv_fixed1, conv_fixed2 generate o_phase, o_phase_step             <--- follows on from f_mult1, f_mult2, i.e. follows read_c5c4 state, after 35 cycle latency.
    --   -- 6 cycle latency -- 
    --
    -- Width of a fine channel (span from center of one fine channel to the next)
    --  = (1/1080e-9) / 256 = 3616.898148148148 Hz
    -- Start of a coarse channel in GHz = fp32 value read from vcmap table
    -- Fine channel frequency step in GHz = 3.616898148148148e-6 GHz
    -- So f_mult1, f_mult2 calculate : 
    --     total rotations = delay_ns * (sky_frequency_GHz) 
    --     rotations per fine channel = delay_ns * c_FINE_STEP_GHz
    -- Then convert both to 32.23 fixed point format
    --     o_phase      = total_rotations fractional part 
    --     o_phase_step = rotation_per_fine_channel fractional part
    --
    --------------------------------------------------------------
    -- Reads of the configuration memories needed :
    --
    --  Note : The corner turn rd/wr logic assumes that 
    --      virtual channel = rdStation + i_stations*rdCoarse;
    --  e.g. for 4 stations, 2 coarse channels:
    --      virtual channel : 0  1  2  3  4  5  6  7
    --      station         : 0  1  2  3  0  1  2  3
    --      coarse          : 0  0  0  0  1  1  1  1
    --  To read the polynomial coeficients, all that is needed is the station index, 
    --  which is just 0 to (i_stations-1)
    --  
    --  Reading of vcmap (virtual channel map) is only needed to get the sky frequency, 
    --  which occurs in step (5) above.
    --
    --
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            --    For timeGroup = 0:(<time samples per corner turn>/32 - 1) -- 53 ms corner turn, So 0:(256/32-1) = 0:7
            --       For Coarse = 0:(i_coarse-1)                            -- For 512 stations, 2 coarse, this is 0:1
            --          For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
            --             For Station = 0:(i_stations-1)                   -- Up to 512 stations
            --                For beam = 0:(i_beams-1)                      -- Up to 16 beams
            if i_poly_eval = '1' then
                poly_eval_fsm <= check_t_fifo;
                poly_eval_beams <= i_beams;       -- 8  bit number of beams to evaluate
                poly_eval_stations_minus1 <= std_logic_vector(unsigned(i_stations) - 1); -- 10 bit number of stations
                poly_eval_coarse_minus1 <= std_logic_vector(unsigned(i_coarse) - 1);     -- 10 bit number of coarse channels
                -- where we are up to :
                poly_beam <= (others => '0');  -- Note the state machine assumes 16 beams, if there are less then the extras are masked at the output.
                poly_time <= "00000000";  -- 256 time samples per corner turn frame
                poly_coarse <= "0000000000"; -- up to 512 coarse channels (but typically only a few)
                poly_station <= "0000000000"; -- up to 512 stations
                poly_vc <= "0000000000"; -- virtual channel = poly_station + poly_coarse * i_stations
            else
                case poly_eval_fsm is
                    when check_t_fifo =>
                        if unsigned(poly_station) = 0 then
                            -- make sure there is time data available
                            if (unsigned(times_fifo_rd_count) > 0) then
                                poly_eval_fsm <= read_times_fifo;
                            end if;
                        else
                            -- Use the previous time data, stored in poly_t1, poly_t2 etc.
                            poly_eval_fsm <= skip_read_times;
                        end if;
                    
                    when read_times_fifo =>
                        poly_eval_fsm <= wait_output_fifo;

                    when skip_read_times =>
                        poly_eval_fsm <= wait_output_fifo;

                    when wait_output_fifo =>
                        if i_stop = '0' then
                            poly_eval_fsm <= read_c1c0;
                        end if;

                    when read_c1c0 =>
                        -- Count through 16 beams 
                        if unsigned(poly_beam) = 15 then
                            poly_beam <= (others => '0');
                            poly_eval_fsm <= read_c3c2;
                        else
                            poly_beam <= std_logic_vector(unsigned(poly_beam) + 1);
                        end if;
                    
                    when read_c3c2 =>
                        -- Count through 16 beams
                        if unsigned(poly_beam) = 15 then
                            poly_beam <= (others => '0');
                            poly_eval_fsm <= read_c5c4;
                        else
                            poly_beam <= std_logic_vector(unsigned(poly_beam) + 1);
                        end if;
                    
                    when read_c5c4 =>
                        -- count through 16 beams
                        if unsigned(poly_beam) = 15 then
                            poly_beam <= (others => '0');
                            poly_eval_fsm <= update_loop;
                        else
                            poly_beam <= std_logic_vector(unsigned(poly_beam) + 1);
                        end if;
                    
                    when update_loop =>
                        if last_poly_station = '1' then
                            poly_station <= (others => '0');
                            if poly_time_eq_31 = '1' then
                                if last_poly_coarse = '1' then
                                    poly_coarse <= (others => '0');
                                    poly_vc <= (others => '0');  -- poly_vc follows poly_coarse, but steps in units of i_stations, so it is the first virtual channel in a block of stations
                                    if poly_time(7 downto 5) = "111" then
                                        -- last time, last coarse, last station
                                        poly_eval_fsm <= done;
                                    else
                                        poly_time <= std_logic_vector(unsigned(poly_time) + 1);
                                        poly_eval_fsm <= check_t_fifo;
                                    end if;
                                else
                                    poly_time(4 downto 0) <= "00000";
                                    poly_coarse <= std_logic_vector(unsigned(poly_coarse) + 1);
                                    poly_vc <= std_logic_vector(unsigned(poly_vc) + unsigned(poly_eval_stations_minus1) + 1);
                                    poly_eval_fsm <= check_t_fifo;
                                end if;
                            else
                                poly_time <= std_logic_vector(unsigned(poly_time) + 1);
                                poly_eval_fsm <= check_t_fifo;
                            end if;
                        else
                            poly_station <= std_logic_vector(unsigned(poly_station) + 1);
                            poly_eval_fsm <= check_t_fifo;
                        end if;
                    
                    when done =>
                        poly_eval_fsm <= done;
                        
                    when others =>
                        poly_eval_fsm <= done;
                end case;
            end if;
            
            poly_eval_fsm_del(0) <= poly_eval_fsm;
            poly_eval_fsm_del(63 downto 1) <= poly_eval_fsm_del(62 downto 0);
            
            -- pipeline some evaluations used in the update_loop state
            if poly_coarse = poly_eval_coarse_minus1 then
                last_poly_coarse <= '1';
            else
                last_poly_coarse <= '0';
            end if;
            
            if poly_station = poly_eval_stations_minus1 then
                last_poly_station <= '1';
            else
                last_poly_station <= '0';
            end if;
            
            if poly_time(4 downto 0) = "11111" then
                poly_time_eq_31 <= '1';
            else
                poly_time_eq_31 <= '0';
            end if;
            
            if poly_eval_fsm_del(14) = read_times_fifo then
                t_fifo_rdEn <= '1';
            else 
                t_fifo_rdEn <= '0';
            end if;
            
            if poly_eval_fsm_del(16) = read_times_fifo then
                -- one clock after t_fifo_rdEn = '1'
                -- del(16) so that it aligns with the polymem read latency, data is available just before it is used in the fp32 multipliers.
                poly_t1 <= t1;
                poly_t2 <= t2;
                poly_t3 <= t3;
                poly_t4 <= t4;
                poly_t5 <= t5;
            end if;
            
            ---------------------------------------------------------------------
            -- Address = (buffer * 24576) + (beam * 1536) + (station * 3) + coef
            --  Where buffer = 0 or 1, as determined from i_poly_buffer0_valid_frame etc.
            --   beam = 0 to 15, station = 0 to 511, 
            --   coef = 0 (poly_eval_fsm = read_c1c0), 1 (poly_eval_fsm = read_c3c2), 2 (poly_eval_fsm = read_c5c4)
            poly_beam_x1024 <= "00" & poly_beam & "0000000000";
            poly_beam_x512  <= "000" & poly_beam & "000000000";
            if buf_sel = '0' then
                polymem_base <= x"0000";
            else
                polymem_base <= x"6000";
            end if;
            if poly_eval_fsm = read_c1c0 then
                polymem_coef_offset <= x"0000";
            elsif poly_eval_fsm = read_c3c2 then
                polymem_coef_offset <= x"0001";
            else
                polymem_coef_offset <= x"0002";
            end if;
            poly_station_x2 <= "00000" & poly_station & '0';
            poly_station_x1 <= "000000" & poly_station;
            --
            polymem_RdAddr0 <= std_logic_vector(unsigned(polymem_base) + unsigned(poly_beam_x1024) + unsigned(poly_beam_x512));
            polymem_RdAddr1 <= std_logic_vector(unsigned(poly_station_x2) + unsigned(poly_station) + unsigned(polymem_coef_offset));
            -- poly_beam, poly_station          aligns with poly_eval_fsm
            -- poly_beam_x1024, poly_station_x2 aligns with poly_eval_fsm_del(0)
            -- polymem_RdAddr0                  aligns with poly_eval_fsm_del(1)
            -- o_polymem_rdAddr                 aligns with poly_eval_fsm_del(2)
            --  -- 15 clock read latency --
            -- i_polymem_rddata                 aligns with poly_eval_fsm_del(17)
            o_polymem_RdAddr <= std_logic_vector(unsigned(polymem_Rdaddr0) + unsigned(polymem_RdAddr1));
            polymem_rdData <= i_polymem_rdData;  -- 14 clock read latency for the memory, +1 clock here, for 15 total.
            ----------------------------------------------------------------
            
            -- Select inputs to madd1, madd2
            if (poly_eval_fsm_del(16) = read_c1c0) then
                mAdd1_dinB <= poly_t1;
                mAdd2_dinB <= c_fp32_unity;
            elsif (poly_eval_fsm_del(16) = read_c3c2) then
                mAdd1_dinB <= poly_t3;
                mAdd2_dinB <= poly_t2;
            else -- The only other case that is used is (poly_eval_fsm_del() = read_c5c4) 
                mAdd1_dinB <= poly_t5;
                mAdd2_dinB <= poly_t4;
            end if;
            
            -- vcmap_RdAddr_del(0) is valid when poly_eval_fsm_del(0) = read_c5c4
            vcmap_RdAddr_del(0) <= std_logic_vector(unsigned(poly_vc) + unsigned(poly_station));
            vcmap_RdAddr_del(58 downto 1) <= vcmap_rdAddr_del(57 downto 0);
            o_vcmap_rdAddr <= vcmap_RdAddr_del(38);  -- count back from 43 when the data is required.
            if (poly_eval_fsm_del(43) = read_c5c4) then
                -- del(17) = input to madd1, madd2 is valid for c5,c4
                -- del(33) = output from madd1, madd2 is valid, input to final_addi is valid
                -- del(44) = final_delay_ns is valid
                coarse_freq_GHz <= i_vcmap_rdData(31 downto 0);
            end if;
        end if;
    end process;
    
    
    -- Single precision multiply-add (a*b + c)
    -- 16 clock latency
    madd1 : single_AxB_plusC
    port map (
        aclk => i_BF_clk,
        s_axis_a_tvalid => '1', 
        s_axis_a_tdata  => polymem_rdData(63 downto 32), -- in (31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => madd1_dinB, -- in (31:0);
        s_axis_c_tvalid => '1',
        s_axis_c_tdata  => madd1_dinC, -- in (31:0);
        m_axis_result_tvalid => open, --
        m_axis_result_tdata  => madd1_dout -- out (31:0) = A*B + C
    );
    
    mAdd1_dinC <= x"00000000" when poly_eval_fsm_del(17) = read_c1c0 else madd1_dout;
    
    madd2 : single_AxB_plusC
    port map (
        aclk => i_BF_clk,
        s_axis_a_tvalid => '1', 
        s_axis_a_tdata  => polymem_rdData(31 downto 0), -- in (31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => madd2_dinB, -- in (31:0);
        s_axis_c_tvalid => '1',
        s_axis_c_tdata  => madd2_dinC, -- in (31:0);
        m_axis_result_tvalid => open, --
        m_axis_result_tdata  => madd2_dout -- out (31:0) = A*B + C
    );
    
    mAdd2_dinC <= x"00000000" when poly_eval_fsm_del(17) = read_c1c0 else madd2_dout;
    
    ---------------------------------------------------------------------------
    -- Phase and phase step
    -- Convert the time delays from the polynomial into a phase and phase step
    --
    
    -- single precision adder
    -- 11 clock latency
    final_addi : single_A_plus_B
    port map (
        aclk => i_BF_clk,
        s_axis_a_tvalid => '1', -- in std_logic;
        s_axis_a_tdata  => madd1_dout, -- in (31:0)
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => madd2_dout, -- in (31:0)
        m_axis_result_tvalid => open,  --
        m_axis_result_tdata  => final_delay_ns -- out (31:0);
    );
    
    
    -- Single precision multiplier, 8 clock latency
    -- multiply by the frequency step between fine channels
    f_mult1 : fp32_mult
    port map (
        aclk  => i_BF_clk, 
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => final_delay_ns,  -- in (31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => c_FINE_STEP_GHz, -- in (31:0);
        m_axis_result_tvalid => open,       -- out std_logic;
        m_axis_result_tdata  => fine_step_rotations -- out (31:0)
    );
    
    -- Single precision multiplier, 8 clock latency
    -- Multiply by the start frequency of the coarse channel
    f_mult2 : fp32_mult
    port map (
        aclk  => i_BF_clk, 
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => final_delay_ns,  -- in (31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => coarse_freq_GHz, -- in (31:0);
        m_axis_result_tvalid => open,       -- out std_logic;
        m_axis_result_tdata  => fine_total_rotations -- out (31:0)
    );
    
    -- float to fixed point, 6 cycle latency
    fp32_to_fixed1i : fp32_to_fixed
    port map (
        aclk => i_BF_clk, 
        s_axis_a_tvalid => '0', -- in STD_LOGIC;
        s_axis_a_tdata  => fine_step_rotations, -- in (31:0)
        m_axis_result_tvalid => open, -- out STD_LOGIC;
        m_axis_result_tdata  => fine_step_phase_int -- out (55:0)
    );
    
    fp32_to_fixed2i : fp32_to_fixed
    port map (
        aclk => i_BF_clk, 
        s_axis_a_tvalid => '0', -- in STD_LOGIC;
        s_axis_a_tdata  => fine_total_rotations, -- in (31:0);
        m_axis_result_tvalid => open, -- out STD_LOGIC;
        m_axis_result_tdata  => fine_total_phase_int -- out (55:0)
    );
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            o_phase <= fine_total_phase_int(23 downto 0);
            o_phase_step <= fine_step_phase_int(23 downto 0);
            
            -- del(44) = final_delay_ns is valid
            -- del(52) = fine_total_rotations is valid
            -- del(58) = fine_total_phase_int is valid
            
            o_phase_virtualChannel <= vcmap_RdAddr_del(58); -- out (9:0);
            
            poly_time_del(0) <= poly_time;
            poly_time_del(58 downto 1) <= poly_time_del(57 downto 0);
            o_phase_timeStep  <= poly_time_del(58); -- out (7:0);
            
            poly_beam_del(0) <= poly_beam;
            poly_beam_del(58 downto 1) <= poly_beam_del(57 downto 0);
            o_phase_beam <= poly_beam_del(58); --  out (3:0);  -- 0 to 15
            if poly_eval_fsm_del(58) = read_c5c4 then
                o_phase_valid <= '1';
            else
                o_phase_valid <= '0';
            end if;
        end if;
    end process;
    
end Behavioral;


