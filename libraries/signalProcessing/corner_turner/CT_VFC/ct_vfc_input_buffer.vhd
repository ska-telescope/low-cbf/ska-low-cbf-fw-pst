----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au) & Norbert Abel
-- 
-- Create Date: 13.03.2020 16:12:03
-- Module Name: ct_vfc_input_buffer - Behavioral
-- Description: 
--  Functions:
--   * Separate the header from the data, 
--   * Cross the clock domain
--   * Buffer the data 
--   * Add checksum and packet count fields to each burst of 16 writes.
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
library DSP_top_lib;
use DSP_top_lib.DSP_top_pkg.all;
Library xpm;
use xpm.vcomponents.all;
library axi4_lib;
use axi4_lib.axi4_full_pkg.all;

entity ct_vfc_input_buffer is
    port(
        i_hbm_clk        : in  std_logic;
        i_hbm_clk_rst    : in  std_logic;
        i_input_clk      : in  std_logic;
        i_input_clk_rst  : in  std_logic;
        -- data in from the LFAA ingest
        i_data           : in std_logic_vector(255 downto 0);  -- incoming data stream (header, data)
        i_dataChecksum   : in std_logic_vector(31 downto 0);   -- Checksum is the sum of all the 32-bit words in every 4 clocks (for 256 bit interface); 
        i_pktValid        :in std_logic;                       -- High for the duration of the packet. 
        i_dataValid      : in std_logic;                       -- high for 257 clocks per packet (will be every second clock if the bus was 128 bits wide at the level above this).
        -- Talk to the pointer management unit, to get the HBM address to write each block to.
        o_packetCount      : out std_logic_vector(31 downto 0); -- Packet count from the packet header
        o_channel          : out std_logic_vector(15 downto 0); -- virtual channel from the packet header
        o_packetCountValid : out std_logic;                     -- Goes high to indicate o_packet_count is valid, and stays high until a response comes back
        i_packetAddress    : in std_logic_vector(31 downto 0);  -- Address to write this packet to.
        i_packetDrop       : in std_logic;                      -- Packet should be dropped as it is either too early or too late.
        i_addrValid        : in std_logic;                      -- i_packet_addr, i_packet_drop are valid. This concludes the transaction with the pointer management module.
        -- Writes to the HBM :
        --  AXI signals to the HBM
        -- This module only uses the "AW" (write address) bus and the "W" (write data) bus. 
        -- Combine at the higher level with the read buses to forma a single AXI interface to the HBM.
        o_hbm_mosi    : out t_axi4_full_mosi;
        o_wdata_extra : out std_logic_vector(31 downto 0); -- Extra 32 bits is a separate bus that is valid at the same time as o_hbm_mosi.wdata.
        i_hbm_miso    : in  t_axi4_full_miso;
        i_hbm_ready   : in  std_logic;
        --error out (in input_clk):
        o_error_input_buffer_overflow : out std_logic  --!! unassigned
    );
end ct_vfc_input_buffer;

architecture Behavioral of ct_vfc_input_buffer is
    
    -- Data FIFO write depth is 1024, with 256 bit wide input.
    -- This has to be BRAM in order to cross the clock domain, 
    -- BRAM has a maximum bit width of 72 bits, so the minimum number of BRAMs 4.
    -- 4 BRAMs would correspond to a depth of 512, but would only buffer 2 input packets.
    -- 8 BRAMs (depth = 1024) buffers 4 input packets.
    constant c_FIFO_WRITE_DEPTH : integer := 1024;
    constant c_FIFO_WR_DATA_COUNT_WIDTH : integer := 11; -- must be log2(c_FIFO_WRITE_DEPTH) + 1
    -- The data FIFO write width is 256+32. The extra 32 bits cycle between the packet count, virtual channel, and checksum.
    -- The packet count and virtual channel are kept with the data as a check to ensure nothing has gone wrong when the data comes out 
    -- of the HBM.
    constant c_FIFO_WRITE_WIDTH : integer := 256+32;
    --constant c_INPUT_BLOCK_LENGTH_256 : integer := c_INPUT_BLOCK_LENGTH_64/4; 
    
    signal headerIn : t_ctc_input_header;
    signal dataInDel1 : std_logic_vector((256+32-1) downto 0);
    signal dataInChecksumDel1 : std_logic_vector(31 downto 0);
    signal dataValidDel1, pktValidDel1 : std_logic;
    signal dataCount : std_logic_vector(9 downto 0);
    
begin

    -- Capture the header information for each packet and write to the FIFO
    -- The header information is also put into the data FIFO and written to the HBM using the parity bits, in order to verify correct operation.
    -- Each group of 4 data words sent to the HBM cycles through the following in the 32 parity bits :
    --   1. packet count (32 bits)
    --   2. 0x0000 & virtual channel (total 32 bits)
    --   3. 0x0000 & station id (total 32 bits)
    --   4. data checksum (32 bits)
    --
    process(i_input_clk)
    begin
        if rising_edge(i_input_clk) then
            
            dataValidDel1 <= i_dataValid;
            pktValidDel1 <= i_pktValid;
            
            if dataValidDel1 = '0' and i_dataValid = '1' then
                headerIn <= slv_to_header(i_data(127 downto 0));
                -- Count the data words in the packet. This will run from 0 to 255
                dataCount <= (others => '0');
            elsif i_dataValid = '1' then  -- Data part of the packet
                dataCount <= std_logic_vector(unsigned(dataCount) + 1);
                dataInDel1(255 downto 0) <= i_data(255 downto 0);
                -- 32 bits of extra data for each 256 bit word, as described above (packet count, virtual channel, station id, checksum)
                if dataCount(1 downto 0) = "00" then
                    dataInDel1(287 downto 256) <= headerIn.packet_count(31 downto 0);
                elsif dataCount(1 downto 0) = "01" then
                    dataInDel1(287 downto 256) <= x"0000" & headerIn.virtual_channel(15 downto 0);
                elsif dataCount(1 downto 0) = "10" then
                    dataInDel1(287 downto 256) <= x"0000" & headerIn.station_id(15 downto 0);
                else
                    dataInDel1(287 downto 256) <= i_dataChecksum(31 downto 0);
                end if;
            end if;
            
        end if;
    end process;        
    

    -- Data FIFO.
    -- Input packets are 256 data words of 256 bit wide data.
    -- We need a clock crossing, and to read 256 bits at a time, so we have to have 4 BRAMs (each has a max 72 bit interface).
    -- 4 BRAMs is enough for 2 incoming packets (16384 bytes). This FIFO uses 8 BRAMs for extra safety to prevent overflows.
    -- So the fifo is 256 bits wide by 1024 deep.
    -- Note that data may potentially be read faster than it is written, so we need to check the amount of data in the FIFO before sending 
    -- a burst to the HBM.
    E_DATA_FIFO : xpm_fifo_async
    generic map (
        DOUT_RESET_VALUE    => "0",
        ECC_MODE            => "no_ecc",
        FIFO_MEMORY_TYPE    => "block",
        FIFO_READ_LATENCY   => 0,
        FIFO_WRITE_DEPTH    => c_FIFO_WRITE_DEPTH,
        FULL_RESET_VALUE    => 1,
        PROG_EMPTY_THRESH   => 10,
        PROG_FULL_THRESH    => 0,
        RD_DATA_COUNT_WIDTH => 10,
        READ_DATA_WIDTH     => 256,
        READ_MODE           => "fwft",
        USE_ADV_FEATURES    => "0000",       --
        WAKEUP_TIME         => 0,
        WRITE_DATA_WIDTH    => c_FIFO_WRITE_WIDTH,
        WR_DATA_COUNT_WIDTH => c_FIFO_WR_DATA_COUNT_WIDTH  -- should be log2(fifo_write_depth) + 1
    ) port map (
        --di
        wr_clk        => i_input_clk,
        rst           => i_input_clk_rst,
        wr_rst_busy   => open,
        din           => dataInDel1(287 downto 0),
        wr_en         => wr_en,
        prog_full     => prog_full,
        full          => full,
        --do
        rd_clk        => i_hbm_clk,
        rd_rst_busy   => rd_rst_busy,
        dout          => dout,
        rd_en         => not empty and i_data_out_rdy,
        empty         => empty,
        --unused
        sleep         => '0', 
        injectdbiterr => '0',
        injectsbiterr => '0'
    ); 
    
    
    assert h_full/='1' or in_header_we/='1' report "HEADER FIFO OVERFLOW!" severity FAILURE;
    
    E_HEADER_FIFO : xpm_fifo_async
    generic map (
        DOUT_RESET_VALUE    => "0",
        ECC_MODE            => "no_ecc",
        FIFO_MEMORY_TYPE    => "distributed",
        FIFO_READ_LATENCY   => 0,
        FIFO_WRITE_DEPTH    => 16,
        FULL_RESET_VALUE    => 1,
        PROG_EMPTY_THRESH   => 10,
        PROG_FULL_THRESH    => 7,     
        RD_DATA_COUNT_WIDTH => log2_ceil(16),
        READ_DATA_WIDTH     => pc_CTC_HEADER_WIDTH+1,
        READ_MODE           => "fwft",
        USE_ADV_FEATURES    => "0002",       --prog_full activated
        WAKEUP_TIME         => 0,
        WRITE_DATA_WIDTH    => pc_CTC_HEADER_WIDTH+1,
        WR_DATA_COUNT_WIDTH => log2_ceil(16)
    ) port map (
        --di
        wr_clk        => i_input_clk,
        rst           => i_input_clk_rst,
        din           => dummy_header & header_to_slv(in_header),
        wr_en         => in_header_we,
        full          => h_full,
        prog_full     => h_prog_full,
        --do
        rd_clk        => i_hbm_clk,
        dout          => header_dout,
        rd_en         => not out_header_empty and i_header_out_rdy,
        empty         => out_header_empty,
        --unused
        sleep         => '0', 
        injectdbiterr => '0',
        injectsbiterr => '0'
    );    
    
    out_header        <= slv_to_header(header_dout(pc_CTC_HEADER_WIDTH-1 downto 0));
    
    
    
       
!!!!! get addresses from the ct_vfc_malloc module, and assign to the AXI bus 
        P_AW:
        process (i_hbm_clk)
        begin
            if rising_edge(i_hbm_clk) then
                if i_hbm_miso.awready='1' then
                    o_hbm_mosi.awaddr <= (others => '0');
                    o_hbm_mosi.awaddr(i_wa'LEFT+5 downto i_wa'RIGHT+5) <= i_wa;
            
                    o_hbm_mosi.awburst           <= (0=>'1', others => '0');
                    o_hbm_mosi.awid(5 downto 0)  <= i_wid;
                    o_hbm_mosi.awlen(3 downto 0) <= std_logic_vector(to_unsigned(g_BURST_LEN-1, 4));
                    o_hbm_mosi.awsize            <= B"101"; --256bit
                    o_hbm_mosi.awvalid           <= i_wae and i_hbm_ready;
                end if;
            end if;
        end process;    
                
        P_W:
        process (i_hbm_clk)
        begin
            if rising_edge(i_hbm_clk) then
                if i_hbm_miso.wready='1' then
                    o_hbm_mosi.wdata(255 downto 0)  <= i_data_in.data;
                    o_hbm_mosi.wlast  <= i_last;
                    o_hbm_mosi.wstrb  <= (others=>'1');
                    o_hbm_mosi.wvalid <= i_we and i_hbm_ready;
                end if;
            end if;    
        end process;        
    
end Behavioral;


-- XPM_FIFO instantiation template for Asynchronous FIFO configurations
-- Refer to the targeted device family architecture libraries guide for XPM_FIFO documentation
-- =======================================================================================================================

-- Parameter usage table, organized as follows:
-- +---------------------------------------------------------------------------------------------------------------------+
-- | Parameter name       | Data type          | Restrictions, if applicable                                             |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Description                                                                                                         |
-- +---------------------------------------------------------------------------------------------------------------------+
-- +---------------------------------------------------------------------------------------------------------------------+
-- | CDC_SYNC_STAGES      | Integer            | Range: 2 - 8. Default value = 2.                                        |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specifies the number of synchronization stages on the CDC path                                                      |
-- |                                                                                                                     |
-- |   Must be < 5 if FIFO_WRITE_DEPTH = 16                                                                              |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | DOUT_RESET_VALUE     | String             | Default value = 0.                                                      |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Reset value of read data path.                                                                                      |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | ECC_MODE             | String             | Allowed values: no_ecc, en_ecc. Default value = no_ecc.                 |
-- |---------------------------------------------------------------------------------------------------------------------|
-- |                                                                                                                     |
-- |   "no_ecc" - Disables ECC                                                                                           |
-- |   "en_ecc" - Enables both ECC Encoder and Decoder                                                                   |
-- |                                                                                                                     |
-- | NOTE: ECC_MODE should be "no_ecc" if FIFO_MEMORY_TYPE is set to "auto". Violating this may result incorrect behavior.|
-- +---------------------------------------------------------------------------------------------------------------------+
-- | FIFO_MEMORY_TYPE     | String             | Allowed values: auto, block, distributed. Default value = auto.         |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Designate the fifo memory primitive (resource type) to use.                                                         |
-- |                                                                                                                     |
-- |   "auto"- Allow Vivado Synthesis to choose                                                                          |
-- |   "block"- Block RAM FIFO                                                                                           |
-- |   "distributed"- Distributed RAM FIFO                                                                               |
-- |                                                                                                                     |
-- | NOTE: There may be a behavior mismatch if Block RAM or Ultra RAM specific features, like ECC or Asymmetry, are selected with FIFO_MEMORY_TYPE set to "auto".|
-- +---------------------------------------------------------------------------------------------------------------------+
-- | FIFO_READ_LATENCY    | Integer            | Range: 0 - 10. Default value = 1.                                       |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Number of output register stages in the read data path.                                                             |
-- |                                                                                                                     |
-- |   If READ_MODE = "fwft", then the only applicable value is 0.                                                       |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | FIFO_WRITE_DEPTH     | Integer            | Range: 16 - 4194304. Default value = 2048.                              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Defines the FIFO Write Depth, must be power of two.                                                                 |
-- |                                                                                                                     |
-- |   In standard READ_MODE, the effective depth = FIFO_WRITE_DEPTH-1                                                   |
-- |   In First-Word-Fall-Through READ_MODE, the effective depth = FIFO_WRITE_DEPTH+1                                    |
-- |                                                                                                                     |
-- | NOTE: The maximum FIFO size (width x depth) is limited to 150-Megabits.                                             |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | FULL_RESET_VALUE     | Integer            | Range: 0 - 1. Default value = 0.                                        |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Sets full, almost_full and prog_full to FULL_RESET_VALUE during reset                                               |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | PROG_EMPTY_THRESH    | Integer            | Range: 3 - 4194301. Default value = 10.                                 |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specifies the minimum number of read words in the FIFO at or below which prog_empty is asserted.                    |
-- |                                                                                                                     |
-- |   Min_Value = 3 + (READ_MODE_VAL*2)                                                                                 |
-- |   Max_Value = (FIFO_WRITE_DEPTH-3) - (READ_MODE_VAL*2)                                                              |
-- |                                                                                                                     |
-- | If READ_MODE = "std", then READ_MODE_VAL = 0; Otherwise READ_MODE_VAL = 1.                                          |
-- | NOTE: The default threshold value is dependent on default FIFO_WRITE_DEPTH value. If FIFO_WRITE_DEPTH value is      |
-- | changed, ensure the threshold value is within the valid range though the programmable flags are not used.           |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | PROG_FULL_THRESH     | Integer            | Range: 5 - 4194301. Default value = 10.                                 |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specifies the maximum number of write words in the FIFO at or above which prog_full is asserted.                    |
-- |                                                                                                                     |
-- |   Min_Value = 3 + (READ_MODE_VAL*2*(FIFO_WRITE_DEPTH/FIFO_READ_DEPTH))+CDC_SYNC_STAGES                              |
-- |   Max_Value = (FIFO_WRITE_DEPTH-3) - (READ_MODE_VAL*2*(FIFO_WRITE_DEPTH/FIFO_READ_DEPTH))                           |
-- |                                                                                                                     |
-- | If READ_MODE = "std", then READ_MODE_VAL = 0; Otherwise READ_MODE_VAL = 1.                                          |
-- | NOTE: The default threshold value is dependent on default FIFO_WRITE_DEPTH value. If FIFO_WRITE_DEPTH value is      |
-- | changed, ensure the threshold value is within the valid range though the programmable flags are not used.           |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | RD_DATA_COUNT_WIDTH  | Integer            | Range: 1 - 23. Default value = 1.                                       |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specifies the width of rd_data_count. To reflect the correct value, the width should be log2(FIFO_READ_DEPTH)+1.    |
-- |                                                                                                                     |
-- |   FIFO_READ_DEPTH = FIFO_WRITE_DEPTH*WRITE_DATA_WIDTH/READ_DATA_WIDTH                                               |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | READ_DATA_WIDTH      | Integer            | Range: 1 - 4096. Default value = 32.                                    |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Defines the width of the read data port, dout                                                                       |
-- |                                                                                                                     |
-- |   Write and read width aspect ratio must be 1:1, 1:2, 1:4, 1:8, 8:1, 4:1 and 2:1                                    |
-- |   For example, if WRITE_DATA_WIDTH is 32, then the READ_DATA_WIDTH must be 32, 64,128, 256, 16, 8, 4.               |
-- |                                                                                                                     |
-- | NOTE:                                                                                                               |
-- |                                                                                                                     |
-- |   READ_DATA_WIDTH should be equal to WRITE_DATA_WIDTH if FIFO_MEMORY_TYPE is set to "auto". Violating this may result incorrect behavior. |
-- |   The maximum FIFO size (width x depth) is limited to 150-Megabits.                                                 |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | READ_MODE            | String             | Allowed values: std, fwft. Default value = std.                         |
-- |---------------------------------------------------------------------------------------------------------------------|
-- |                                                                                                                     |
-- |   "std"- standard read mode                                                                                         |
-- |   "fwft"- First-Word-Fall-Through read mode                                                                         |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | RELATED_CLOCKS       | Integer            | Range: 0 - 1. Default value = 0.                                        |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specifies if the wr_clk and rd_clk are related having the same source but different clock ratios                    |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | SIM_ASSERT_CHK       | Integer            | Range: 0 - 1. Default value = 0.                                        |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | 0- Disable simulation message reporting. Messages related to potential misuse will not be reported.                 |
-- | 1- Enable simulation message reporting. Messages related to potential misuse will be reported.                      |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | USE_ADV_FEATURES     | String             | Default value = 0707.                                                   |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Enables data_valid, almost_empty, rd_data_count, prog_empty, underflow, wr_ack, almost_full, wr_data_count,         |
-- | prog_full, overflow features.                                                                                       |
-- |                                                                                                                     |
-- |   Setting USE_ADV_FEATURES[0] to 1 enables overflow flag;     Default value of this bit is 1                        |
-- |   Setting USE_ADV_FEATURES[1]  to 1 enables prog_full flag;    Default value of this bit is 1                       |
-- |   Setting USE_ADV_FEATURES[2]  to 1 enables wr_data_count;     Default value of this bit is 1                       |
-- |   Setting USE_ADV_FEATURES[3]  to 1 enables almost_full flag;  Default value of this bit is 0                       |
-- |   Setting USE_ADV_FEATURES[4]  to 1 enables wr_ack flag;       Default value of this bit is 0                       |
-- |   Setting USE_ADV_FEATURES[8]  to 1 enables underflow flag;    Default value of this bit is 1                       |
-- |   Setting USE_ADV_FEATURES[9]  to 1 enables prog_empty flag;   Default value of this bit is 1                       |
-- |   Setting USE_ADV_FEATURES[10] to 1 enables rd_data_count;     Default value of this bit is 1                       |
-- |   Setting USE_ADV_FEATURES[11] to 1 enables almost_empty flag; Default value of this bit is 0                       |
-- |   Setting USE_ADV_FEATURES[12] to 1 enables data_valid flag;   Default value of this bit is 0                       |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | WAKEUP_TIME          | Integer            | Range: 0 - 2. Default value = 0.                                        |
-- |---------------------------------------------------------------------------------------------------------------------|
-- |                                                                                                                     |
-- |   0 - Disable sleep                                                                                                 |
-- |   2 - Use Sleep Pin                                                                                                 |
-- |                                                                                                                     |
-- | NOTE: WAKEUP_TIME should be 0 if FIFO_MEMORY_TYPE is set to "auto". Violating this may result incorrect behavior.   |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | WRITE_DATA_WIDTH     | Integer            | Range: 1 - 4096. Default value = 32.                                    |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Defines the width of the write data port, din                                                                       |
-- |                                                                                                                     |
-- |   Write and read width aspect ratio must be 1:1, 1:2, 1:4, 1:8, 8:1, 4:1 and 2:1                                    |
-- |   For example, if WRITE_DATA_WIDTH is 32, then the READ_DATA_WIDTH must be 32, 64,128, 256, 16, 8, 4.               |
-- |                                                                                                                     |
-- | NOTE:                                                                                                               |
-- |                                                                                                                     |
-- |   WRITE_DATA_WIDTH should be equal to READ_DATA_WIDTH if FIFO_MEMORY_TYPE is set to "auto". Violating this may result incorrect behavior. |
-- |   The maximum FIFO size (width x depth) is limited to 150-Megabits.                                                 |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | WR_DATA_COUNT_WIDTH  | Integer            | Range: 1 - 23. Default value = 1.                                       |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specifies the width of wr_data_count. To reflect the correct value, the width should be log2(FIFO_WRITE_DEPTH)+1.   |
-- +---------------------------------------------------------------------------------------------------------------------+

-- Port usage table, organized as follows:
-- +---------------------------------------------------------------------------------------------------------------------+
-- | Port name      | Direction | Size, in bits                         | Domain  | Sense       | Handling if unused     |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Description                                                                                                         |
-- +---------------------------------------------------------------------------------------------------------------------+
-- +---------------------------------------------------------------------------------------------------------------------+
-- | almost_empty   | Output    | 1                                     | rd_clk  | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Almost Empty : When asserted, this signal indicates that only one more read can be performed before the FIFO goes to|
-- | empty.                                                                                                              |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | almost_full    | Output    | 1                                     | wr_clk  | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Almost Full: When asserted, this signal indicates that only one more write can be performed before the FIFO is full.|
-- +---------------------------------------------------------------------------------------------------------------------+
-- | data_valid     | Output    | 1                                     | rd_clk  | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Read Data Valid: When asserted, this signal indicates that valid data is available on the output bus (dout).        |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | dbiterr        | Output    | 1                                     | rd_clk  | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Double Bit Error: Indicates that the ECC decoder detected a double-bit error and data in the FIFO core is corrupted.|
-- +---------------------------------------------------------------------------------------------------------------------+
-- | din            | Input     | WRITE_DATA_WIDTH                      | wr_clk  | NA          | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Write Data: The input data bus used when writing the FIFO.                                                          |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | dout           | Output    | READ_DATA_WIDTH                       | rd_clk  | NA          | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Read Data: The output data bus is driven when reading the FIFO.                                                     |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | empty          | Output    | 1                                     | rd_clk  | Active-high | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Empty Flag: When asserted, this signal indicates that the FIFO is empty.                                            |
-- | Read requests are ignored when the FIFO is empty, initiating a read while empty is not destructive to the FIFO.     |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | full           | Output    | 1                                     | wr_clk  | Active-high | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Full Flag: When asserted, this signal indicates that the FIFO is full.                                              |
-- | Write requests are ignored when the FIFO is full, initiating a write when the FIFO is full is not destructive       |
-- | to the contents of the FIFO.                                                                                        |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | injectdbiterr  | Input     | 1                                     | wr_clk  | Active-high | Tie to 1'b0            |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Double Bit Error Injection: Injects a double bit error if the ECC feature is used on block RAMs or                  |
-- | UltraRAM macros.                                                                                                    |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | injectsbiterr  | Input     | 1                                     | wr_clk  | Active-high | Tie to 1'b0            |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Single Bit Error Injection: Injects a single bit error if the ECC feature is used on block RAMs or                  |
-- | UltraRAM macros.                                                                                                    |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | overflow       | Output    | 1                                     | wr_clk  | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Overflow: This signal indicates that a write request (wren) during the prior clock cycle was rejected,              |
-- | because the FIFO is full. Overflowing the FIFO is not destructive to the contents of the FIFO.                      |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | prog_empty     | Output    | 1                                     | rd_clk  | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Programmable Empty: This signal is asserted when the number of words in the FIFO is less than or equal              |
-- | to the programmable empty threshold value.                                                                          |
-- | It is de-asserted when the number of words in the FIFO exceeds the programmable empty threshold value.              |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | prog_full      | Output    | 1                                     | wr_clk  | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Programmable Full: This signal is asserted when the number of words in the FIFO is greater than or equal            |
-- | to the programmable full threshold value.                                                                           |
-- | It is de-asserted when the number of words in the FIFO is less than the programmable full threshold value.          |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | rd_clk         | Input     | 1                                     | NA      | Rising edge | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Read clock: Used for read operation. rd_clk must be a free running clock.                                           |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | rd_data_count  | Output    | RD_DATA_COUNT_WIDTH                   | rd_clk  | NA          | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Read Data Count: This bus indicates the number of words read from the FIFO.                                         |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | rd_en          | Input     | 1                                     | rd_clk  | Active-high | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO.        |
-- |                                                                                                                     |
-- |   Must be held active-low when rd_rst_busy is active high.                                                          |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | rd_rst_busy    | Output    | 1                                     | rd_clk  | Active-high | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Read Reset Busy: Active-High indicator that the FIFO read domain is currently in a reset state.                     |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | rst            | Input     | 1                                     | wr_clk  | Active-high | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Reset: Must be synchronous to wr_clk. The clock(s) can be unstable at the time of applying reset, but reset must be released only after the clock(s) is/are stable.|
-- +---------------------------------------------------------------------------------------------------------------------+
-- | sbiterr        | Output    | 1                                     | rd_clk  | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Single Bit Error: Indicates that the ECC decoder detected and fixed a single-bit error.                             |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | sleep          | Input     | 1                                     | NA      | Active-high | Tie to 1'b0            |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Dynamic power saving: If sleep is High, the memory/fifo block is in power saving mode.                              |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | underflow      | Output    | 1                                     | rd_clk  | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Underflow: Indicates that the read request (rd_en) during the previous clock cycle was rejected                     |
-- | because the FIFO is empty. Under flowing the FIFO is not destructive to the FIFO.                                   |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | wr_ack         | Output    | 1                                     | wr_clk  | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.    |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | wr_clk         | Input     | 1                                     | NA      | Rising edge | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Write clock: Used for write operation. wr_clk must be a free running clock.                                         |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | wr_data_count  | Output    | WR_DATA_COUNT_WIDTH                   | wr_clk  | NA          | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Write Data Count: This bus indicates the number of words written into the FIFO.                                     |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | wr_en          | Input     | 1                                     | wr_clk  | Active-high | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Write Enable: If the FIFO is not full, asserting this signal causes data (on din) to be written to the FIFO.        |
-- |                                                                                                                     |
-- |   Must be held active-low when rst or wr_rst_busy is active high.                                                   |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | wr_rst_busy    | Output    | 1                                     | wr_clk  | Active-high | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Write Reset Busy: Active-High indicator that the FIFO write domain is currently in a reset state.                   |
-- +---------------------------------------------------------------------------------------------------------------------+

