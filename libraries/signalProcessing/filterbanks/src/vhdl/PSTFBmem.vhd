----------------------------------------------------------------------------------
-- Company: CSIRO - CASS
-- Engineer: David Humphrey
-- 
-- Create Date: 06.12.2018 10:49:58
-- Module Name: PSTFBmem - Behavioral
-- Description: 
--   
-- 
----------------------------------------------------------------------------------
library IEEE, common_lib, filterbanks_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use common_lib.common_pkg.all;

entity PSTFBMem is
    generic (
        TAPS : integer := 12;      -- Note only partially parameterized; modification needed to support anything other than 12.
        -- Number of bits in the data path; Default is 96 = 6*16 bits. 
        -- Only partially parameterised; 
        --  * Set the data type of rd_data_o to match.
        --  * Width of the distributed memory IP block also needs to be set to match. 
        DATAWIDTH : integer := 96  
    );
    port(
        clk          : in std_logic;
        -- Write data for the start of the chain
        i_wrData     : in std_logic_vector((DATAWIDTH-1) downto 0);
        i_wrAddr     : in std_logic_vector(8 downto 0);
        i_wrEn       : in std_logic; -- should be a burst of 4096 clocks.
        -- Read data, comes out 2 clocks after the first write.
        o_rdData     : out t_slv_96_arr(TAPS-1 downto 0);  -- 96 (=6*16) bits wide, 12 taps simultaneously; First sample is wr_data_i delayed by 1 clock.
        i_rdAddr     : in std_logic_vector(8 downto 0);
        i_rdWrEn     : in std_logic;
        -- Read FIR filter taps
        i_romAddr    : in std_logic_vector(7 downto 0); 
        o_coef       : out t_slv_18_arr(TAPS-1 downto 0)   -- 18 bits per filter tap.
    );
end PSTFBMem;

architecture Behavioral of PSTFBmem is

    signal dummy0SLV : std_logic_vector(0 downto 0);
    signal dummy0_18 : std_logic_vector(17 downto 0);
    signal romAddrDel : t_slv_8_arr((TAPS-1) downto 0):= (others => (others => '0'));
    signal rdAddrDelFull : t_slv_9_arr(TAPS downto 0);
    signal rdAddrDel70 : t_slv_8_arr((TAPS) downto 0) := (others => (others => '0'));
    signal rdAddrDel8 : std_logic_vector(TAPS downto 0) := (others => '0');
    signal rdDataDel : t_slv_96_arr((TAPS-1) downto 0);
    signal rdWrEnDel : std_logic_vector((TAPS) downto 0) := (others => '0');
    
begin
    
    firstMemi : entity filterbanks_lib.BRAM_512x96
    port map (
        -- Simple dual port RAM, 9 bit address, 96 bit data.
        clk    => clk,
        i_wrEn => i_wrEn,              -- in std_logic;
        i_wr_addr => i_wrAddr,         -- in(8:0);
        i_wr_data => i_wrData,         -- in(95:0);
        i_rd_addr => rdAddrDelFull(0), -- in(8:0);
        o_rd_data => rdDataDel(0)      -- out(95:0)
    );    
    
    rdAddrDel70(0) <= i_rdAddr(7 downto 0);
    rdAddrDel8(0) <= i_rdAddr(8);
    rdAddrDelFull(0) <= rdAddrDel8(0) & rdAddrDel70(0);
    romAddrDel(0) <= i_romAddr;
    o_rdData(0) <= rdDataDel(0);
    rdWrEnDel(0) <= i_rdWrEn;
    
    process(clk)
    begin
        if rising_edge(clk) then
           
            rdAddrDel70(TAPS downto 1) <= rdAddrDel70((TAPS-1) downto 0);
            rdAddrDel8(TAPS downto 1) <= not rdAddrDel8((TAPS-1) downto 0); -- read and write alternate between different halves of the memories.
            
            romAddrDel((TAPS-1) downto 1) <= romAddrDel((TAPS-2) downto 0);
            rdWrEnDel(TAPS downto 1) <= rdWrEnDel(TAPS-1 downto 0);
        end if;
    end process;
    
    
    rdAddrDelFull(TAPS) <= rdAddrDel8(TAPS) & rdAddrDel70(TAPS);
    
    othermem : for i in 1 to (TAPS-1) generate
        
        rdAddrDelFull(i) <= rdAddrDel8(i) & rdAddrDel70(i);
        
        otherMemi : entity filterbanks_lib.BRAM_512x96
        port map (
            -- Simple dual port RAM, 9 bit address, 96 bit data.
            clk    => clk,
            i_wrEn => rdWrEnDel(i+1),        -- in std_logic;
            i_wr_addr => rdAddrDelFull(i+1), -- in(8:0);
            i_wr_data => rdDataDel(i-1),     -- in(95:0);
            i_rd_addr => rdAddrDelFull(i),   -- in(8:0);
            o_rd_data => rdDataDel(i)        -- out(95:0)
        );
        
        o_rdData(i) <= rdDataDel(i);
    end generate;

    --------------------------------------------------------------------------------------
    -- Filter Coefficients
    -- 12 ROMs, each 18 bits wide, 512 deep.
    
    dummy0SLV(0) <= '0';
    dummy0_18 <= (others => '0');
    
    -- ROMS with filter taps.
    -- Beware this assumes the generic "TAPS" is 12.
    
    FIRTaps1 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps12.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(0), -- in std_logic_vector(7 downto 0);
        douta => o_coef(0)      -- out std_logic_vector(17 downto 0)
    );
    
    FIRTaps2 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps11.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(1), -- in std_logic_vector(7 downto 0);
        douta => o_coef(1)      -- out std_logic_vector(17 downto 0)
    );
 
    FIRTaps3 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps10.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(2), -- in std_logic_vector(7 downto 0);
        douta => o_coef(2)      -- out std_logic_vector(17 downto 0)
    );
    
    FIRTaps4 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps9.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(3), -- in std_logic_vector(7 downto 0);
        douta => o_coef(3)      -- out std_logic_vector(17 downto 0)
    );

    FIRTaps5 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps8.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(4), -- in std_logic_vector(7 downto 0);
        douta => o_coef(4)      -- out std_logic_vector(17 downto 0)
    );

    FIRTaps6 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps7.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(5), -- in std_logic_vector(7 downto 0);
        douta => o_coef(5)      -- out std_logic_vector(17 downto 0)
    );

    FIRTaps7 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps6.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(6), -- in std_logic_vector(7 downto 0);
        douta => o_coef(6)      -- out std_logic_vector(17 downto 0)
    );

    FIRTaps8 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps5.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(7), -- in std_logic_vector(7 downto 0);
        douta => o_coef(7)      -- out std_logic_vector(17 downto 0)
    );

    FIRTaps9 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps4.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(8), -- in std_logic_vector(7 downto 0);
        douta => o_coef(8)      -- out std_logic_vector(17 downto 0)
    );

    FIRTaps10 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps3.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(9), -- in std_logic_vector(7 downto 0);
        douta => o_coef(9)      -- out std_logic_vector(17 downto 0)
    );

    FIRTaps11 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps2.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(10), -- in std_logic_vector(7 downto 0);
        douta => o_coef(10)      -- out std_logic_vector(17 downto 0)
    );

    FIRTaps12 : entity filterbanks_lib.BROMWrapper
    generic map (
        g_INIT_FILE => "PSTFIRTaps1.mem"
    ) port map (
        -- Single port ROM, 8 bit address, 18 bit data.
        clka  => clk,           -- in std_logic; -- => FIRTapClk,
        addra => romAddrDel(11), -- in std_logic_vector(7 downto 0);
        douta => o_coef(11)      -- out std_logic_vector(17 downto 0)
    );
    
end Behavioral;
