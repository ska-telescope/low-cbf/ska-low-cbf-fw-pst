-------------------------------------------------------------------------------
--
-- File Name: vcu128_gemini_dsp.vhd
-- Contributing Authors: David Humphrey
-- Type: RTL
-- Created: July 2019
--
-- Title: Top Level for the Perentie signal processing code.
--
-- Description: 
--  Includes all the signal processing and data manipulation modules.
--  There are several different "signal chains". The input and output data for a signal chain
--  is either an optical port or the interconnect module.
--  The interconnect module routes data between the signal chains and the FPGA to FPGA optical network.
--  
--  Signal Chains:
--   (1) Timing
--   This has a single module, "timingControl". Timing control has following tasks:
--      - Keep track of "wall_time"
--      - Synchonise wall_time to either MACE or another FPGAs wall_time
--      - Control the OCXO via the SPI interface to the DAC (Note this is not available on the VCU128 board)
--      - Provide wall_time in other clock domains
--
--    (2) Station Processing Ingest:
--    Takes data from the 40GE LFAA input, and sends it to the interconnect module.
--      40GE fiber -> LFAADecode -> Capture128Bit -> LocalDoppler -> Interconnect
--    Submodules:
--     - LFAADecode
--        Extracts data from the 40GE UDP/SPEAD packets, matches to the virtual channel table, and forwards packets
--     - Capture128bit
--        Capture packets at the output of the LFAADecode module
--     + LocalDoppler
--        Applies doppler correction.
--
--    (3) Station Processing filterbanks
--    Interconnect -> Coarse Corner Turn -> filterbanks -> Fine Delay -> interconnect
--    Submodules
--     - Coarse corner turn (CTC) 
--        Uses HBM memory to rearrange the data into 0.9 second bursts for each station
--     - Filterbanks
--     - Fine Delay
--     
--    (4)... more to come.
--
-------------------------------------------------------------------------------

LIBRARY IEEE, common_lib, axi4_lib, DSP_top_lib, signal_processing_common, ethernet_lib;
library LFAADecode100G_lib, DSP_top_lib, filterbanks_lib, ct_lib, bf_lib, PSR_Packetiser_lib;
use ct_lib.all;
use DSP_top_lib.DSP_top_pkg.all;
--use DSP_top_lib.DSP_top_reg_pkg.all;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
USE common_lib.common_mem_pkg.ALL;
USE axi4_lib.axi4_lite_pkg.ALL;
USE axi4_lib.axi4_stream_pkg.ALL;
USE axi4_lib.axi4_full_pkg.ALL;

use ethernet_lib.ethernet_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;

library technology_lib;
USE technology_lib.tech_mac_100g_pkg.ALL;

library xpm;
use xpm.vcomponents.all;

-------------------------------------------------------------------------------
entity DSP_top_BF is
    generic (
        g_PIPE_INSTANCE                 : integer := 0;
        g_DEBUG_ILA                     : boolean := false;
        g_BEAM_ILA                      : boolean := false;
        g_PST_BEAMS                     : integer := 16;
        g_USE_META : boolean := FALSE  -- Put meta data into the memory in place of the actual data, to make it easier to find bugs in the corner turn. 
    );
    port (
        -----------------------------------------------------------------------
        -- Received data from 100GE
        i_axis_tdata        : in std_logic_vector(511 downto 0); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
        i_axis_tkeep        : in std_logic_vector(63 downto 0);  -- one bit per byte in i_axi_tdata
        i_axis_tlast        : in std_logic;
        i_axis_tuser        : in std_logic_vector(79 downto 0);  -- Timestamp for the packet.
        i_axis_tvalid       : in std_logic;
        
        i_clk_100GE         : in std_logic;
        i_eth100G_locked    : in std_logic;
        -----------------------------------------------------------------------
        -- Other processing clocks.
        i_clk450 : in std_logic; -- 450 MHz
        i_clk400 : in std_logic; -- 400 MHz
        -----------------------------------------------------------------------
        -- Debug signal used in the testbench.
        o_validMemRstActive : out std_logic;  -- reset of the valid memory is in progress.
        -----------------------------------------------------------------------
        -- MACE AXI slave interfaces for modules
        -- The 300MHz MACE_clk is also used for some of the signal processing
        i_MACE_clk  : in std_logic;
        i_MACE_rst  : in std_logic;
        -- LFAADecode, lite + full slave
        i_LFAALite_axi_mosi : in t_axi4_lite_mosi;  -- => mc_lite_mosi(c_LFAADecode_lite_index),
        o_LFAALite_axi_miso : out t_axi4_lite_miso; -- => mc_lite_miso(c_LFAADecode_lite_index),
        i_LFAAFull_axi_mosi : in  t_axi4_full_mosi; -- => mc_full_mosi(c_LFAAdecode_full_index),
        o_LFAAFull_axi_miso : out t_axi4_full_miso; -- => mc_full_miso(c_LFAAdecode_full_index),

        -- Corner Turn between LFAA Ingest and the filterbanks.
        i_LFAA_CT_axi_mosi : in t_axi4_lite_mosi;  --
        o_LFAA_CT_axi_miso : out t_axi4_lite_miso; --
        i_SPS_CT_full_axi_mosi : in  t_axi4_full_mosi; -- => mc_full_mosi(c_pst_ct1_full_index),
        o_SPS_CT_full_axi_miso : out t_axi4_full_miso; -- => mc_full_miso(c_pst_ct1_full_index),

        -- Registers for the beamformer corner turn 
        i_ct2_lite_axi_mosi : in t_axi4_lite_mosi;  --
        o_ct2_lite_axi_miso : out t_axi4_lite_miso; --
        i_ct2_full_axi_mosi : in t_axi4_full_mosi;
        o_ct2_full_axi_miso : out t_axi4_full_miso;
        -- PST Beamformer
        i_BF_axi_mosi : in  t_axi4_full_mosi;
        o_BF_axi_miso : out t_axi4_full_miso;
        
        o_beamData        : out std_logic_vector(63 downto 0);
        o_beamPacketCount : out std_logic_vector(39 downto 0); -- count of the number of PST output packets since the SKA epoch. Each packet is 32 time samples = 6.635520 ms of data.
        o_beamBeam        : out std_logic_vector(7 downto 0);
        o_beamFreqIndex   : out std_logic_vector(10 downto 0);
        o_beamValid       : out std_logic;
        o_beamJonesStatus : out std_logic_vector(1 downto 0);  -- bit 0 = used default, bit 1 = jones valid
        o_beamPoly_ok     : out std_logic_vector(1 downto 0); -- The polynomials used for beamforming are in their valid time range.
        -----------------------------------------------------------------------
        -- AXI interfaces to shared memory
        -- Uses the same clock as MACE (300MHz)
        -----------------------------------------------------------------------
        -- Corner Turn between LFAA ingest and the filterbanks
        -- 1 GByte = 4 x 256 MByte buffers.
        -- AXI4 master interface for accessing HBM for the LFAA ingest corner turn : m01_axi
        -- aw bus = write address
        m01_axi_awvalid  : out std_logic;
        m01_axi_awready  : in std_logic;
        m01_axi_awaddr   : out std_logic_vector(30 downto 0);
        m01_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m01_axi_wvalid    : out std_logic;
        m01_axi_wready    : in std_logic;
        m01_axi_wdata     : out std_logic_vector(511 downto 0);
        m01_axi_wlast     : out std_logic;
        -- b bus - write response
        m01_axi_bvalid    : in std_logic;
        m01_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m01_axi_arvalid   : out std_logic;
        m01_axi_arready   : in std_logic;
        m01_axi_araddr    : out std_logic_vector(30 downto 0);
        m01_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m01_axi_rvalid    : in std_logic;
        m01_axi_rready    : out std_logic;
        m01_axi_rdata     : in std_logic_vector(511 downto 0);
        m01_axi_rlast     : in std_logic;
        m01_axi_rresp     : in std_logic_vector(1 downto 0);
        -----------------------------------------------------------------------------
        -- Corner turn between filterbanks and beamformer
        -- 1 Gbyte (2 x 512 MByte buffers)
        -- aw bus = write address
        m02_axi_awvalid  : out std_logic;
        m02_axi_awready  : in std_logic;
        m02_axi_awaddr   : out std_logic_vector(29 downto 0);
        m02_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m02_axi_wvalid    : out std_logic;
        m02_axi_wready    : in std_logic;
        m02_axi_wdata     : out std_logic_vector(511 downto 0);
        m02_axi_wlast     : out std_logic;
        -- b bus - write response
        m02_axi_bvalid    : in std_logic;
        m02_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m02_axi_arvalid   : out std_logic;
        m02_axi_arready   : in std_logic;
        m02_axi_araddr    : out std_logic_vector(29 downto 0);
        m02_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m02_axi_rvalid    : in std_logic;
        m02_axi_rready    : out std_logic;
        m02_axi_rdata     : in std_logic_vector(511 downto 0);
        m02_axi_rlast     : in std_logic;
        m02_axi_rresp     : in std_logic_vector(1 downto 0);
        
        ---------------------------------------------------------------------------
        -- HBM reset
        o_hbm_reset       : out std_logic_vector(1 downto 0);
        i_hbm_status      : in t_slv_8_arr(1 downto 0)
    );
end DSP_top_BF;

-------------------------------------------------------------------------------
ARCHITECTURE structure OF DSP_top_BF IS

    ---------------------------------------------------------------------------
    -- SIGNAL DECLARATIONS  --
    --------------------------------------------------------------------------- 
    
    signal dbg_timer_expire : std_logic;
    signal dbg_timer  :  std_logic_vector(15 downto 0);    
    signal LFAADecode_dbg : std_logic_vector(13 downto 0);
    signal LFAA_tx_fsm, LFAA_stats_fsm, LFAA_rx_fsm : std_logic_vector(3 downto 0);
    signal LFAA_goodpacket, LFAA_nonSPEAD : std_logic;
    
    signal gnd : std_logic_vector(199 downto 0);
    
    signal clk_LFAA40GE_wallTime : t_wall_time;
    signal clk_HBM_wallTime : t_wall_time;
    --signal clk_wall_wallTime : t_wall_time;
    
    signal CTCDataIn : std_logic_vector(63 downto 0);
    signal CTCValidIn : std_logic;
    signal CTCSOPIn   : std_logic;
    
  
    signal CTFCorDataIn : std_logic_vector(63 downto 0);
    signal CTFCorSOPIn  : std_logic;
    signal CTFCorValid  : std_logic;
    
    signal MACE_clk_vec : std_logic_vector(0 downto 0);
    signal MACE_clk_rst : std_logic_vector(0 downto 0);
    
    --signal dsp_top_rw  : t_statctrl_rw;
    
    signal CTC_CorSof : std_logic; -- single cycle pulse: this cycle is the first of 204*4096
    --signal CTC_CorHeader : t_ctc_output_header_a(pc_CTC_OUTPUT_NUMBER-1 downto 0);    -- meta data belonging to the data coming out
    signal CTC_CorHeaderValid : std_logic;                                            -- new meta data (every output packet, aka 4096 cycles) 
    --signal CTC_CorData : t_ctc_output_data_a(pc_CTC_OUTPUT_NUMBER-1 downto 0);      -- the actual output data
    signal CTC_CorDataValid : std_logic;

    signal CTC_CorSof_del1 : std_logic; -- single cycle pulse: this cycle is the first of 204*4096
    --signal CTC_CorHeader_del1 : t_ctc_output_header_a(pc_CTC_OUTPUT_NUMBER-1 downto 0);    -- meta data belonging to the data coming out
    signal CTC_CorHeaderValid_del1 : std_logic;                                            -- new meta data (every output packet, aka 4096 cycles) 
    --signal CTC_CorData_del1 : t_ctc_output_data_a(pc_CTC_OUTPUT_NUMBER-1 downto 0);      -- the actual output data
    signal CTC_CorDataValid_del1 : std_logic;

    signal CTC_CorSof_del2 : std_logic; -- single cycle pulse: this cycle is the first of 204*4096
    --signal CTC_CorHeader_del2  : t_ctc_output_header_a(pc_CTC_OUTPUT_NUMBER-1 downto 0);    -- meta data belonging to the data coming out
    signal CTC_CorHeaderValid_del2  : std_logic;                                            -- new meta data (every output packet, aka 4096 cycles) 
    --signal CTC_CorData_del2  : t_ctc_output_data_a(pc_CTC_OUTPUT_NUMBER-1 downto 0);      -- the actual output data
    signal CTC_CorDataValid_del2  : std_logic;

    signal CTC_CorSof_del3  : std_logic; -- single cycle pulse: this cycle is the first of 204*4096
    --signal CTC_CorHeader_del3 : t_ctc_output_header_a(pc_CTC_OUTPUT_NUMBER-1 downto 0);    -- meta data belonging to the data coming out
    signal CTC_CorHeaderValid_del3 : std_logic;                                            -- new meta data (every output packet, aka 4096 cycles) 
    --signal CTC_CorData_del3 : t_ctc_output_data_a(pc_CTC_OUTPUT_NUMBER-1 downto 0);      -- the actual output data
    signal CTC_CorDataValid_del3 : std_logic;

    signal CTC_PSSPSTSof : std_logic; 
    --signal CTC_PSSPSTData : t_ctc_output_data_a(2 downto 0);  -- 3 different streams, each 32 bits (8 bits each for VpolRe, VpolIm, HpolRe, HpolIm)
    signal CTC_PSSPSTDataValid : std_logic;
    --signal CTC_PSSPSTHeader : t_ctc_output_header_a(2 downto 0); -- one header per stream
    signal CTC_PSSPSTHeaderValid : std_logic;

    signal CTC_PSSPSTSof_del1 : std_logic; 
    --signal CTC_PSSPSTData_del1 : t_ctc_output_data_a(2 downto 0);  -- 3 different streams, each 32 bits (8 bits each for VpolRe, VpolIm, HpolRe, HpolIm)
    signal CTC_PSSPSTDataValid_del1 : std_logic;
    --signal CTC_PSSPSTHeader_del1 : t_ctc_output_header_a(2 downto 0); -- one header per stream
    signal CTC_PSSPSTHeaderValid_del1 : std_logic;
    
    signal CTC_PSSPSTSof_del2 : std_logic; 
    --signal CTC_PSSPSTData_del2 : t_ctc_output_data_a(2 downto 0);  -- 3 different streams, each 32 bits (8 bits each for VpolRe, VpolIm, HpolRe, HpolIm)
    signal CTC_PSSPSTDataValid_del2 : std_logic;
    --signal CTC_PSSPSTHeader_del2 : t_ctc_output_header_a(2 downto 0); -- one header per stream
    signal CTC_PSSPSTHeaderValid_del2 : std_logic;
    
    signal CTC_PSSPSTSof_del3 : std_logic; 
    --signal CTC_PSSPSTData_del3 : t_ctc_output_data_a(2 downto 0);  -- 3 different streams, each 32 bits (8 bits each for VpolRe, VpolIm, HpolRe, HpolIm)
    signal CTC_PSSPSTDataValid_del3 : std_logic;
    --signal CTC_PSSPSTHeader_del3 : t_ctc_output_header_a(2 downto 0); -- one header per stream
    signal CTC_PSSPSTHeaderValid_del3 : std_logic;    
        
    
    signal CTC_HBM_clk_rst : std_logic;          -- reset going to the HBM core
    signal CTC_HBM_mosi : t_axi4_full_mosi;   -- data going to the HBM core
    signal CTC_HBM_miso : t_axi4_full_miso;   -- data coming from the HBM core
    signal CTC_HBM_ready : std_logic;
    
    --signal FB_PSSHeader      : t_ctc_output_header_a(2 downto 0);
    signal FB_PSSHeaderValid : std_logic;
    --signal FB_PSSData        : t_ctc_output_data_a(2 downto 0);
    signal FB_PSSDataValid   : std_logic;
    -- PST filterbank data output
    --signal FB_PSTHeader      : t_ctc_output_header_a(2 downto 0);
    signal FB_PSTHeaderValid : std_logic;
    --signal FB_PSTData        : t_ctc_output_data_a(2 downto 0);
    signal FB_PSTDataValid   : std_logic;
    signal HBMPage : std_logic_vector(9 downto 0);
    signal hbm_width_rst : std_logic := '0';
    
    signal MACE_HBM_mosi : t_axi4_full_mosi;
    signal MACE_HBM_miso : t_axi4_full_miso;
    
    signal hbm_axi_rst, hbm_axi_rst_del1, hbm_axi_rst_del2, hbm_axi_rst_del3 : std_logic := '0';
    signal IC_rst, IC_rst_del1, IC_rst_del2, IC_rst_del3 : std_logic_vector(31 downto 0);
    
    signal fineDelayDisable : std_logic;
    signal RFI_scale_reg : std_logic_vector(4 downto 0);
   
    COMPONENT ila_0
    PORT (
   	    clk : IN STD_LOGIC;
   	    probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
    END COMPONENT;
    
    component ila_beamData
    port (
        clk : in std_logic;
        probe0 : in std_logic_vector(119 downto 0)); 
    end component;
    
    signal mac100G : std_logic_vector(47 downto 0);
    signal clk100GE_wallTime : t_wall_time;
    
    signal LFAAingest_virtualChannel : std_logic_vector(15 downto 0);  -- single number to uniquely identify the channel+station for this packet.
    signal LFAAingest_packetCount    : std_logic_vector(39 downto 0);  -- Packet count from the SPEAD header.
    signal CT1_ingest_packetCount    : std_logic_vector(47 downto 0);
    signal LFAAingest_valid          : std_logic;                      -- out std_logic
    
    signal LFAAingest_wvalid : std_logic;
    signal LFAAingest_wready : std_logic;
    signal LFAAingest_wdata  : std_logic_vector(511 downto 0);
    signal LFAAingest_wstrb  : std_logic_vector(63 downto 0);
    signal LFAAingest_wlast  : std_logic;
    
    signal FB_sof : std_logic;
    
    signal FB_data0 : t_slv_8_arr(1 downto 0);
    signal FB_data1 : t_slv_8_arr(1 downto 0);
    signal FB_meta01 : t_atomic_CT_pst_META_out; 
    signal FB_data2 : t_slv_8_arr(1 downto 0);
    signal FB_data3 : t_slv_8_arr(1 downto 0);
    signal FB_meta23 : t_atomic_CT_pst_META_out;
    signal FB_data4 : t_slv_8_arr(1 downto 0);
    signal FB_data5 : t_slv_8_arr(1 downto 0);
    signal FB_meta45 : t_atomic_CT_pst_META_out;
    signal FB_valid : std_logic;
    
    signal FD_frameCount :  std_logic_vector(36 downto 0); -- frame count is the same for all simultaneous output streams.
    signal FD_virtualChannel : t_slv_16_arr(2 downto 0); -- 3 virtual channels, one for each of the PST data streams.
    signal FD_headerValid : std_logic_vector(2 downto 0);
    signal FD_data : t_slv_64_arr(2 downto 0); 
    signal FD_dataValid : std_logic;
    
    signal ct1_totalStations : std_logic_vector(11 downto 0);
    signal ct1_totalCoarse   : std_logic_vector(11 downto 0);
    signal ct1_totalChannels : std_logic_vector(11 downto 0); 
    
    signal BFdataIn : std_logic_vector(191 downto 0); -- 3 consecutive fine channels delivered every clock.
    signal BFflagged : std_logic_vector(2 downto 0);
    signal BFFine : std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
    signal BFCoarse : std_logic_vector(9 downto 0); -- coarse channel.
    signal BFPacketCount : std_logic_vector(39 downto 0);
    signal BFValidIn, BFPktOdd : std_logic;
    signal BFBeamsEnabled : std_logic_vector(7 downto 0);
    signal ct_rst : std_logic;
    signal ct_sof : std_logic;
    signal CT_sofCount : std_logic_vector(11 downto 0) := (others => '0');
    signal CT_sofFinal : std_logic := '0';
    
    signal BFFirstStation : std_logic;
    signal BFLastStation : std_logic;
    signal BFtimeStep : std_logic_vector(4 downto 0);
    signal BFVirtualChannel : std_logic_vector(9 downto 0);
    
    signal BFJonesBuffer : std_logic;
    
    signal beamData : std_logic_vector(63 downto 0);
    signal beamPacketCount : std_logic_vector(39 downto 0);
    signal beamBeam : std_logic_vector(7 downto 0);
    signal beamFreqIndex : std_logic_vector(10 downto 0);
    signal beamValid : std_logic;
    
    -- copy of the beam bus with breakout of some signals so names are easier to see in the ILA core.
    signal bDataReal0, bDataImag0, bDataReal1, bDataImag1 : std_logic_vector(15 downto 0);
    signal bVirtualChannel : std_logic_vector(9 downto 0);  -- 10 bits
    signal bBeam : std_logic_vector(7 downto 0);  -- 8 bits
    signal bPacketCount : std_logic_vector(39 downto 0); -- 40 bits
    signal bValid : std_logic;
    
    signal m01_axi_resp_int     : std_logic_vector(1 downto 0);
    
    signal m02_axi_awvalid_dbg : std_logic;
    signal m02_axi_awready_dbg : std_logic;
    signal m02_axi_awaddr_dbg  : std_logic_vector(29 downto 0);
    signal m02_axi_awlen_dbg   : std_logic_vector(3 downto 0);
    -- w bus - write data
    signal m02_axi_wvalid_dbg  : std_logic;
    signal m02_axi_wready_dbg  : std_logic;
    signal m02_axi_wdata_dbg   : std_logic_vector(511 downto 0);
    signal m02_axi_wlast_dbg   : std_logic;
    -- b bus - write response
    signal m02_axi_bvalid_dbg  : std_logic;
    signal m02_axi_bresp_dbg   : std_logic_vector(1 downto 0);
    -- ar bus - read address
    signal m02_axi_arvalid_dbg  : std_logic;
    signal m02_axi_arready_dbg  : std_logic;
    signal m02_axi_araddr_dbg   : std_logic_vector(29 downto 0);
    signal m02_axi_arlen_dbg    : std_logic_vector(3 downto 0);
    -- r bus - read data
    signal m02_axi_rvalid_dbg   : std_logic;
    signal m02_axi_rready_dbg   : std_logic;
    signal m02_axi_rdata_dbg    : std_logic_vector(511 downto 0);
    signal m02_axi_rlast_dbg    : std_logic;
    signal m02_axi_rresp_dbg    : std_logic_vector(1 downto 0);
    --
    signal dbg_ILA_trigger, bdbg_ILA_triggerDel1, bdbg_ILA_trigger, bdbg_ILA_triggerDel2 : std_logic;
    signal dataMismatch_dbg, dataMismatch, datamismatchBFclk : std_logic;
    
    signal m01_axi_aw : t_axi4_full_addr;
    signal m01_axi_b : t_axi4_full_b;
    signal m01_axi_ar : t_axi4_full_addr;
    signal m01_axi_r : t_axi4_full_data;
    signal ct_rst_in : std_logic;
    signal BF_scale_exp_frac : std_logic_vector(7 downto 0);
    
    signal BF_phase_virtualChannel : std_logic_vector(9 downto 0);
    signal BF_phase_timeStep : std_logic_vector(7 downto 0);
    signal BF_phase_beam : std_logic_vector(3 downto 0);
    signal BF_phase : std_logic_vector(23 downto 0);      -- Phase at the start of the coarse channel.
    signal BF_phase_step : std_logic_vector(23 downto 0); -- Phase step per fine channel.
    signal BF_phase_valid : std_logic;
    
    signal BFjones_status : std_logic_vector(1 downto 0); -- bit 0 = used default, bit 1 = jones valid;
    signal BFpoly_ok : std_logic_vector(1 downto 0);  -- The polynomials used are within their valid time range;
    
    signal beamJonesStatus : std_logic_vector(1 downto 0);
    signal beamPoly_ok : std_logic_vector(1 downto 0);
    signal BF_phase_clear : std_logic;
    
    signal cmac_reset : std_logic;
    signal bad_polynomials : std_logic;
    signal vct_table_select : std_logic;
    signal ct2_totalChannels, ct2_totalStations, ct2_totalCoarse : std_logic_vector(11 downto 0);
    
begin
    
    gnd <= (others => '0');

    CMAC_100G_reset_proc : process(i_clk_100GE)
    begin
        if rising_edge(i_clk_100GE) then
            cmac_reset  <= NOT i_eth100G_locked;
        end if;
    end process;

    --------------------------------------------------------------------------
    -- Signal Processing signal Chains
    --------------------------------------------------------------------------
   
    
    -- Takes in data from the 100GE port, checks it is a valid SPEAD packet, then
    --  - Notifies the corner turn, which generates the write address part of the AXI memory interface.
    --  - Outputs the data part of the packet on the wdata part of the AXI memory interface.
    LFAAin : entity LFAADecode100G_lib.LFAADecodeTop100G
    port map(
        -- Data in from the 100GE MAC
        i_axis_tdata        => i_axis_tdata, --  in (511:0); 64 bytes of data, 1st byte in the packet is in bits 7:0.
        i_axis_tkeep        => i_axis_tkeep, --  in (63:0);  one bit per byte in i_axi_tdata
        i_axis_tlast        => i_axis_tlast, --  in std_logic;                      
        i_axis_tuser        => i_axis_tuser, --  in (79:0);  -- Timestamp for the packet, from the PTP core
        i_axis_tvalid       => i_axis_tvalid, -- in std_logic;
        i_100GE_clk         => i_clk_100GE,    -- in std_logic;  322 MHz for 100GE MAC
        i_100GE_rst         => cmac_reset,            -- in std_logic;

        -- Data to the corner turn. This is just some header information about each LFAA packet, needed to generate the address the data is to be written to.
        o_virtualChannel => LFAAingest_virtualChannel,  -- out(15:0), single number to uniquely identify the channel+station for this packet.
        o_packetCount    => LFAAingest_packetCount,     -- out(31:0). Packet count from the SPEAD header.
        o_totalChannels  => ct1_totalChannels, -- out (11:0); Total channels for the table currently being used in this module
        o_totalStations  => ct1_totalStations,    -- out (11:0);
        o_totalCoarse    => ct1_totalCoarse,      -- out (11:0);
        o_valid          => LFAAingest_valid, -- out std_logic; o_virtualChannel and o_packetCount are valid.
        o_totalChannelsTable0 => open, -- out (11:0); Total channels for vct table 0
        o_totalChannelsTable1 => open, -- out (11:0); Total channels for vct table 1
        o_tableSelect         => open, -- out std_logic;
        -- wdata portion of the AXI-full external interface (should go directly to the external memory)
        i_axi_wready  => m01_axi_wready, -- in std_logic;
        o_axi_w.valid => m01_axi_wvalid, -- out std_logic;
        o_axi_w.data  => m01_axi_wdata,  -- out std_logic_vector(511 downto 0);
        o_axi_w.last  => m01_axi_wlast,  -- out std_logic;
        o_axi_w.resp  => m01_axi_resp_int,  -- out std_logic;

        --AXI lite Interface
        i_s_axi_mosi => i_LFAALite_axi_mosi, -- in t_axi4_lite_mosi; at the top level use mc_lite_mosi(c_LFAADecode_lite_index)
        o_s_axi_miso => o_LFAALite_axi_miso, -- out t_axi4_lite_miso;
        i_s_axi_clk  => i_MACE_clk,         
        i_s_axi_rst  => i_MACE_rst,
        -- registers AXI Full interface
        i_vcstats_MM_IN  => i_LFAAFull_axi_mosi, -- in  t_axi4_full_mosi; At the top level use mc_full_mosi(c_LFAAdecode_full_index),
        o_vcstats_MM_OUT => o_LFAAFull_axi_miso, -- out t_axi4_full_miso;

        -- control signals in to select which virtual channel table to use
        i_vct_table_select => vct_table_select, -- in std_logic;
        
        -- hbm reset   
        o_hbm_reset      => o_hbm_reset(0),
        i_hbm_status     => i_hbm_status(0),
        
        o_reset_to_ct    => ct_rst_in,
        -- debug
        o_dbg            => LFAADecode_dbg
    );

--    timing : entity timingControl_lib.timing_control_atomic
--    port map (
--        -- Registers - Uses 300 MHz clock
--        mm_rst    => i_MACE_rst,        -- in std_logic;
--        i_sla_in  => i_timing_axi_mosi, -- in t_axi4_lite_mosi;
--        o_sla_out => o_timing_axi_miso, -- out t_axi4_lite_miso;
--        -------------------------------------------------------
--        -- clocks :
--        -- THe 300MHz clock must be 300MHz, since it is used to track the time in ns. However this module will still work if the other clocks are not the frequency implied by their name.
--        i_clk300        => i_MACE_clk,   -- 300 MHz processing clock, used for interfaces in the vitis core. This clock is used for tracking the time (3 clocks = 10 ns)
--        i_clk400        => i_clk400,     -- in std_logic;  -- 400 MHz processing clock.
--        i_clk450        => i_clk450,     -- in std_logic;  -- 450 MHz processing clock.
--        i_LFAA100GE_clk => i_clk_100GE,  -- in std_logic;  -- 322 MHz clock from the 100GE core. 
--        -- Wall time outputs in each clock domain
--        o_clk300_wallTime => clk300_walltime, -- out(63:0); -- wall time in clk300 domain, in nanoseconds
--        o_clk400_wallTime => clk400_walltime, -- out(63:0); -- wall time in clk400 domain, in nanoseconds
--        o_clk450_wallTime => clk450_walltime, -- out(63:0); -- wall time in the clk450 domain, in nanoseconds
--        o_clk100GE_wallTime => clk322_walltime, -- out(63:0); -- wall time in clk322 domain, in nanoseconds
--        --------------------------------------------------------
--        -- Timing notifications from LFAA ingest module.
--        -- This is the wall time according to timing packets coming in on the 100G network. This is in the i_LFAA100GE_clk domain.
--        i_100GE_timing_valid => timingPacketValid, -- in std_logic;
--        i_100GE_timing       => timingPacketData   -- in(63:0)  -- current time in nanoseconds according to UDP timing packets from the switch
--    );
    
    
    -- pad out packetcount until SPS interface is changed to 48 bits.
    CT1_ingest_packetCount(39 downto 0) <= LFAAingest_packetCount;
    CT1_ingest_packetCount(47 downto 40) <= x"00";
    
    --LFAA_FB_CT : entity CT_lib.ct_atomic_pst_in
    LFAA_FB_CT : entity CT_lib.pst_ct1_top
    port map(
        -- shared memory interface clock (300 MHz)
        i_shared_clk => i_MACE_clk, -- in std_logic;
        i_shared_rst => i_MACE_rst, -- in std_logic;

        --AXI Lite Interface for registers
        i_saxi_mosi => i_LFAA_CT_axi_mosi, -- in t_axi4_lite_mosi;
        o_saxi_miso => o_LFAA_CT_axi_miso, -- out t_axi4_lite_miso;
        -- AXI full interface for the polynomial configuration
        i_poly_full_axi_mosi => i_SPS_CT_full_axi_mosi, --  in  t_axi4_full_mosi; -- => mc_full_mosi(c_pst_ct1_full_index),
        o_poly_full_axi_miso => o_SPS_CT_full_axi_miso, --  out t_axi4_full_miso; -- => mc_full_miso(c_pst_ct1_full_index),

        -- other config (from LFAA ingest config, must be the same for the corner turn)
        i_rst => ct_rst_in,
        o_rst => ct_rst, -- reset output from a register in the corner turn; used to reset downstream modules.
        o_validMemRstActive => o_validMemRstActive, -- out std_logic;  -- reset is in progress, don't send data; Only used in the testbench. Reset takes about 20us.
        --------------------------------------------------------------
        -- Headers for each valid packet received by the LFAA ingest.
        -- LFAA packets are about 8300 bytes long, so at 100Gbps each LFAA packet is about 660 ns long. This is about 200 of the interface clocks (@300MHz)
        -- These signals use i_shared_clk
        i_virtualChannel => LFAAingest_virtualChannel, -- in (15:0); -- Single number which incorporates both the channel and station; this module supports values in the range 0 to 1023.
        i_packetCount    => CT1_ingest_packetCount,    -- in (47:0);
        i_totalChannels  => ct1_totalChannels,    -- out (11:0); Total channels for the table currently being used in this module
        i_totalStations  => ct1_totalStations,    -- out (11:0);
        i_totalCoarse    => ct1_totalCoarse,      -- out (11:0);
        i_valid          => LFAAingest_valid,     -- in std_logic;
        -- Select table to use in the ingest module
        o_vct_table_select => vct_table_select, -- out std_logic;
        ---------------------------------------------------------------------------------------
        -- Data bus output to the Filterbanks
        -- 6 Outputs, each complex data, 8 bit real, 8 bit imaginary.
        FB_clk  => i_clk450,     -- in std_logic;
        o_rfi_scale => rfi_scale_reg, -- out (4:0)
        o_sof   => FB_sof,     -- out std_logic; start of data for a set of 3 virtual channels.
        o_sofFull => CT_sof,   -- out std_logic; start of the full frame, i.e. a burst of 53 ms of data.
        o_data0 => FB_data0,   -- out t_slv_8_arr(1 downto 0);
        o_data1 => FB_data1,   -- out t_slv_8_arr(1 downto 0);
        o_meta01 => FB_meta01, -- out 
        o_data2 => FB_data2,   -- out t_slv_8_arr(1 downto 0);
        o_data3 => FB_data3,   -- out t_slv_8_arr(1 downto 0);
        o_meta23 => FB_meta23, -- out 
        o_data4 => FB_data4,   -- out t_slv_8_arr(1 downto 0);
        o_data5 => FB_data5,   -- out t_slv_8_arr(1 downto 0);
        o_meta45 => FB_meta45, -- out 
        o_valid => FB_valid,   -- out std_logic;
        -- o_bad_polynomials uses shared_clk; pass direct to corner turn 2.
        -- Likewise totalChannel, totalStations and totalCoarse. These should be captured by ct2 at the start of each 53ms corner turn frame
        o_bad_polynomials => bad_polynomials, --  out std_logic;
        o_totalChannels   => ct2_totalChannels, -- out (11:0); Total channels for the table currently being used in this module
        o_totalStations   => ct2_totalStations, -- out (11:0);
        o_totalCoarse     => ct2_totalCoarse,   -- out (11:0);
        -------------------------------------------------------------
        -- AXI bus to the shared memory. 
        -- This has the aw, b, ar and r buses (the w bus is on the output of the LFAA decode module)
        -- aw bus - write address
        o_m01_axi_aw      => m01_axi_aw,      -- out t_axi4_full_addr; -- write address bus : out t_axi4_full_addr (.valid, .addr(39:0), .len(7:0))
        i_m01_axi_awready => m01_axi_awready, -- in std_logic;
        -- b bus - write response
        i_m01_axi_b  => m01_axi_b, -- in t_axi4_full_b;   -- (.valid, .resp); resp of "00" or "01" means ok, "10" or "11" means the write failed.
        -- ar bus - read address
        o_m01_axi_ar      => m01_axi_ar,      -- out t_axi4_full_addr; -- read address bus : out t_axi4_full_addr (.valid, .addr(39:0), .len(7:0))
        i_m01_axi_arready => m01_axi_arready, -- in std_logic;
        -- r bus - read data
        i_m01_axi_r       => m01_axi_r,     -- in  t_axi4_full_data;
        o_m01_axi_rready  => m01_axi_rready -- out std_logic
    );
    
    m01_axi_awvalid <= m01_axi_aw.valid;
    m01_axi_awaddr <= m01_axi_aw.addr(30 downto 0);
    m01_axi_awlen <= m01_axi_aw.len;
    
    m01_axi_b.valid <= m01_axi_bvalid;
    m01_axi_b.resp <= m01_axi_bresp;
    
    m01_axi_arvalid <= m01_axi_ar.valid;
    m01_axi_araddr <= m01_axi_ar.addr(30 downto 0);
    m01_axi_arlen <= m01_axi_ar.len;
    
    m01_axi_r.valid <= m01_axi_rvalid;
    m01_axi_r.data <= m01_axi_rdata;
    m01_axi_r.last <= m01_axi_rlast;
    m01_axi_r.resp <= m01_axi_rresp;
    
    
    -- PST filterbank and fine delay.
    PSTFB_i : entity filterbanks_lib.FB_top_PST
    port map (
        -- clock, 450 MHz to process 1024 virtual channels.
        i_data_clk => i_clk450, -- in std_logic;
        i_data_rst => FB_sof, -- in std_logic;
        -- Configuration (on i_data_clk)
        i_fineDelayDisable => '0',     -- in std_logic;
        i_RFIScale         => RFI_scale_reg, --"10100", -- in(4:0);
        -- Data input, common valid signal, expects packets of 64 samples. 
        -- Requires at least 2 clocks idle time between packets.
        -- Due to oversampling, also requires on average 86 clocks between packets - specifically, no more than 3 packets in 258 clocks. 
        i_SOF    => FB_sof,
        i_data0  => FB_data0, -- in t_slv_8_arr(1 downto 0);  -- 6 Inputs, each complex data, 8 bit real, 8 bit imaginary.
        i_data1  => FB_data1, -- in t_slv_8_arr(1 downto 0);
        i_meta01 => FB_meta01,
        i_data2  => FB_data2, -- in t_slv_8_arr(1 downto 0);
        i_data3  => FB_data3, -- in t_slv_8_arr(1 downto 0);
        i_meta23 => FB_meta23,
        i_data4  => FB_data4, -- in t_slv_8_arr(1 downto 0);
        i_data5  => FB_data5, -- in t_slv_8_arr(1 downto 0);
        i_meta45 => FB_meta45,
        i_dataValid => FB_valid, -- in std_logic;
        -- Data out; bursts of 216 clocks for each channel.
        -- PST filterbank data output
        o_frameCount     => FD_frameCount,     -- out std_logic_vector(36 downto 0); -- frame count is the same for all simultaneous output streams.
        o_virtualChannel => FD_virtualChannel, -- out t_slv_16_arr(2 downto 0); -- 3 virtual channels, one for each of the PST data streams.
        o_HeaderValid    => FD_headerValid,    -- out std_logic_vector(2 downto 0);
        o_Data           => FD_data,           -- out t_slv_64_arr(2 downto 0);
        o_DataValid      => FD_dataValid       -- out std_logic
    );
    
    -- Corner turn between filterbanks and beamformer
    process(i_clk450)
    begin
        if rising_edge(i_clk450) then
            -- add a delay to CT_sof, to allow time for the previous filterbank output to complete.
            -- There are 11 frames processed before data is output by the filterbank, each taking a minimum of (4/3)*256 = 340 clocks.
            -- So we can safely add up to 11*340 = 3740 clocks.
            if CT_sof = '1' then
                CT_sofCount <= std_logic_vector(to_unsigned(3072,12));
            elsif unsigned(CT_sofCount) /= 0 then
                CT_sofCount <= std_logic_vector(unsigned(CT_sofCount) - 1);
            end if;
            if unsigned(CT_sofCount) = 1 then
                CT_sofFinal <= '1';
            else
                CT_sofFinal <= '0';
            end if;
        end if;
    end process;
    
    
    BFCT : entity ct_lib.ct2_wrapper
    generic map (
        g_PST_BEAMS => g_PST_BEAMS,
        g_USE_META => g_USE_META
    ) port map (
        -- Parameters, in the i_axi_clk domain.
        i_stations => ct2_totalStations(10 downto 0), -- in (10:0); Up to 1024 stations
        i_coarse   => ct2_totalCoarse(9 downto 0),    -- in (9:0);  Number of coarse channels.
        i_virtualChannels => ct2_totalChannels(10 downto 0), -- in (10:0); Total virtual channels (= i_stations * i_coarse)
        i_bad_polynomials => bad_polynomials, -- in std_logic;
        -- Registers AXI Lite Interface (uses i_axi_clk)
        i_axi_mosi => i_ct2_lite_axi_mosi, -- in t_axi4_lite_mosi;
        o_axi_miso => o_ct2_lite_axi_miso, -- out t_axi4_lite_miso;
        i_axi_rst  => i_MACE_rst, --  in std_logic;
        -- Polynomial memory axi full interface
        i_axi_full_mosi => i_ct2_full_axi_mosi, -- in t_axi4_full_mosi;
        o_axi_full_miso => o_ct2_full_axi_miso, -- out t_axi4_full_miso;
        -- Reset passed in from upstream.
        i_rst => ct_rst,
        -- Data in from the PST filterbanks; bursts of 216 clocks for each channel.
        -- 
        i_sof          => CT_sofFinal,     -- in std_logic; Pulse high at the start of every frame. (1 frame is typically 60ms of data).
        i_FB_clk       => i_clk450,        -- in std_logic; Filterbank clock, expected to be 450 MHz
        i_frameCount     => FD_frameCount, -- in (36:0); Frame count is the same for all simultaneous output streams.
        i_virtualChannel => FD_virtualChannel, -- in t_slv_16_arr(2:0); 3 virtual channels, one for each of the PST data streams.
        i_HeaderValid => FD_headerValid, -- in (2:0);
        i_data        => FD_data,        -- in t_slv_64_arr(2:0); (2 pol)x(16+16 bit complex) = 64 bits, for each of 3 virtual channels.
        i_dataValid   => FD_dataValid,   -- in std_logic;
        
        -- Data out to the beamformer
        i_BF_clk  => i_clk400,  -- in std_logic; beamformer clock, expected to be 400 MHz
        o_data    => BFdataIn,  -- out (191:0); 3 consecutive fine channels delivered every clock.
        o_flagged => BFflagged, -- out (2:0);  "o_flagged" aligns with "o_data"
        o_fine    => BFFine,    -- out (7:0);  fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        o_coarse  => BFCoarse,  -- out (9:0);
        o_firstStation => BFFirstStation, -- out std_logic;
        o_lastStation => BFLastStation,   -- out std_logic;
        o_timeStep => BFtimeStep,         -- out (4:0)
        o_virtualChannel => BFVirtualChannel,  -- out (9:0); Coarse channel count.
        o_packetCount => BFPacketCount, -- out (39:0); The PST output packet count for this packet, based on the original packet count from LFAA. Each PST output packet is 6.63552 ms
        o_outputPktOdd => BFPktOdd,     -- in std_logic;
        o_valid   => BFValidIn,  -- out std_logic;
        -- Polynomial data 
        o_phase_virtualChannel => BF_phase_virtualChannel, -- out (9:0);
        o_phase_timeStep       => BF_phase_timeStep,       -- out (7:0);
        o_phase_beam           => BF_phase_beam,           -- out (3:0);
        o_phase                => BF_phase,                -- out (23:0); Phase at the start of the coarse channel.
        o_phase_step           => BF_phase_step,           -- out (23:0); Phase step per fine channel.
        o_phase_valid          => BF_phase_valid,          -- out std_logic;
        o_phase_clear          => BF_phase_clear,          -- out std_logic;
        -- Configuration to the beamformers
        o_jonesBuffer => BFJonesBuffer,   -- out std_logic;
        o_jones_status => BFjones_status, -- out (1:0); bit 0 = used default, bit 1 = jones valid;
        o_poly_ok      => BFpoly_ok,      -- out (1:0); The polynomials used are within their valid time range; bit (0) = 
        o_beamsEnabled => BFBeamsEnabled, -- out (7:0)
        o_scale_exp_frac => BF_scale_exp_frac, -- out (7:0); bit 3 = 0 indicates firmware should calculate the scale factor.

        -- AXI interface to the HBM
        -- Corner turn between filterbanks and beamformer
        -- aw bus = write address
        i_axi_clk => i_MACE_clk, -- in std_logic;
        -- 
        m0_axi_awvalid  => m02_axi_awvalid, --  out std_logic;
        m0_axi_awready  => m02_axi_awready, -- in std_logic;
        m0_axi_awaddr   => m02_axi_awaddr,  -- out (29:0);
        m0_axi_awlen    => m02_axi_awlen,   -- out (7:0);
        -- w bus - write data
        m0_axi_wvalid   => m02_axi_wvalid, -- out std_logic;
        m0_axi_wready   => m02_axi_wready, -- in std_logic;
        m0_axi_wdata    => m02_axi_wdata,  -- out (511:0);
        m0_axi_wlast    => m02_axi_wlast,  -- out std_logic;
        -- b bus - write response
        m0_axi_bvalid    => m02_axi_bvalid, -- in std_logic;
        m0_axi_bresp     => m02_axi_bresp,  -- in (1:0);
        -- ar bus - read address
        m0_axi_arvalid   => m02_axi_arvalid, -- out std_logic;
        m0_axi_arready   => m02_axi_arready, -- in std_logic;
        m0_axi_araddr    => m02_axi_araddr,  -- out (29:0);
        m0_axi_arlen     => m02_axi_arlen,   -- out (7:0);
        -- r bus - read data
        m0_axi_rvalid    => m02_axi_rvalid, -- in std_logic;
        m0_axi_rready    => m02_axi_rready, -- out std_logic;
        m0_axi_rdata     => m02_axi_rdata,  -- in (511:0);
        m0_axi_rlast     => m02_axi_rlast,  -- in std_logic;
        m0_axi_rresp     => m02_axi_rresp,   -- in (1:0)
        --
        o_dataMismatch      => dataMismatch,  -- out std_logic;
        o_dataMismatchBFclk => datamismatchBFclk, -- out std_logic;
        
                -- hbm reset   
        o_hbm_reset        => o_hbm_reset(1),
        i_hbm_status       => i_hbm_status(1)
    );
    
    -- Beamformer
    BFi : entity bf_lib.PSTbeamformerTop_dp
    generic map (
        g_PIPE_INSTANCE => g_PIPE_INSTANCE,
        g_PST_BEAMS     => g_PST_BEAMS -- integer := 16
    ) port map (
        -- Registers axi full interface
        i_MACE_clk => i_MACE_clk, -- in std_logic;
        i_MACE_rst => i_MACE_rst, -- in std_logic;
        i_axi_mosi => i_BF_axi_mosi, -- in  t_axi4_full_mosi;
        o_axi_miso => o_BF_axi_miso, -- out t_axi4_full_miso;
        -- Beamformer data from the corner turn
        i_BF_clk         => i_clk400,         -- in std_logic;
        i_data           => BFdataIn,         -- in (191:0);  -- 3 consecutive fine channels delivered every clock.
        i_flagged        => BFflagged,        -- in (2:0);
        i_fine           => BFFine,           -- in (7:0);   -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        i_coarse         => BFCoarse,         -- in (9:0);
        i_firstStation   => BFFirstStation,   -- in std_logic; -- First station (used to trigger a new accumulator cycle in the beamformers).
        i_lastStation    => BFLastStation,    -- in std_logic;
        i_timeStep       => BFTimeStep,       -- in (4:0);  -- Timestep, runs from 0 to 31. There are 32 timesteps per output packet.
        i_virtualChannel => BFvirtualChannel, -- in (9:0);  -- virtual channel
        i_packetCount    => BFPacketCount,    -- in (36:0); -- The packet count for this packet, based on the original packet count from LFAA.
        i_outputPktOdd   => BFPktOdd,         -- in std_logic;
        i_valid          => BFValidIn,        -- in std_logic;
        -- Polynomial data 
        i_phase_virtualChannel => BF_phase_virtualChannel, -- in (9:0);
        i_phase_timeStep       => BF_phase_timeStep,       -- in (7:0);
        i_phase_beam           => BF_phase_beam,           -- in (3:0);
        i_phase                => BF_phase,                -- in (23:0);      -- Phase at the start of the coarse channel.
        i_phase_step           => BF_phase_step,           -- in (23:0); -- Phase step per fine channel.
        i_phase_valid          => BF_phase_valid,          -- in std_logic;
        i_phase_clear          => BF_phase_clear,          -- in std_logic;
        -- Other data from the corner turn
        i_jonesBuffer    => BFJonesBuffer,    -- in std_logic;
        i_jones_status   => BFjones_status,   -- in (1:0); bit 0 = used default, bit 1 = jones valid;
        i_poly_ok        => BFpoly_ok,        -- in (1:0); The polynomials used are within their valid time range;
        i_beamsEnabled   => BFBeamsEnabled,   -- in (7:0)
        i_scale_exp_frac => BF_scale_exp_frac, -- in (7:0)
        -- 64 bit bus out to the 100GE Packetiser
        o_BFdata         => beamData,         -- out (63:0);
        o_BFpacketCount  => beamPacketCount,  -- out (39:0);
        o_BFBeam         => beamBeam,         -- out (7:0);
        o_BFFreqIndex    => beamFreqIndex,    -- out (10:0);
        o_BFvalid        => beamValid,        -- out std_logic
        o_BFjones_status => beamJonesStatus,  -- out (1:0);
        o_BFpoly_ok      => beamPoly_ok,      -- out (1:0);
        --
        i_badPacket => bdbg_ILA_trigger
    );
    
    o_beamData        <= beamData;
    o_beamPacketCount <= beamPacketCount;
    o_beamBeam        <= beamBeam;
    o_beamFreqIndex   <= beamFreqIndex;
    o_beamValid       <= beamValid;
    o_beamJonesStatus <= beamJonesStatus;
    o_beamPoly_ok     <= beamPoly_ok;

    ILA_GEN : if g_BEAM_ILA GENERATE
    
        process(i_clk400)
        begin
            if rising_edge(i_clk400) then
                bDataReal0 <= beamData(15 downto 0);
                bDataImag0 <= beamData(31 downto 16);
                bDataReal1 <= beamData(47 downto 32);
                bDataImag1 <= beamData(63 downto 48);
                bVirtualChannel <= beamFreqIndex(9 downto 0);  -- 10 bits
                bBeam <= beamBeam;  -- 8 bits
                bPacketCount <= beamPacketCount; -- 40 bits
                bValid <= beamValid;
                bdbg_ILA_trigger <= dbg_ILA_trigger;
                bdbg_ILA_triggerDel1 <= bdbg_ILA_trigger;
                bdbg_ILA_triggerDel2 <= bdbg_ILA_triggerDel1;
            end if;
        end process;
    
        u_BF_ila : ila_beamData
        port map (
            clk => i_clk400,
            probe0(15 downto 0) => bDataReal0,       -- 16 bits
            probe0(31 downto 16) => bDataImag0,      -- 16 bits
            probe0(47 downto 32) => bDataReal1,      -- 16 bits
            probe0(63 downto 48) => bDataImag1,      -- 16 bits
            probe0(73 downto 64) => bVirtualChannel, -- 10 bits
            probe0(80 downto 74) => bBeam(6 downto 0), -- 7 bits
            probe0(81)           => bdbg_ILA_trigger,   
            probe0(118 downto 82) => bPacketCount(36 downto 0),   -- 37 bits
            probe0(119) => bValid                    -- 1 bit
        );
        
        u_BFIn_ila : ila_beamData
        port map (
            clk => i_clk400,
            probe0(63 downto 0)    => BFdataIn(63 downto 0), -- in (95:0);  -- 3 consecutive fine channels delivered every clock.
            probe0(64)             => bdbg_ILA_triggerDel2,  -- bad frame according to the packet generator.
            probe0(74 downto 65)   => BFCoarse,
            probe0(75)             => dataMismatchBFclk,
            probe0(95 downto 76)   => (others => '0'),
            probe0(103 downto 96)  => BFFine,           -- in (7:0);   -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
            probe0(104)            => BFFirstStation,   -- in std_logic; -- First station (used to trigger a new accumulator cycle in the beamformers).
            probe0(105)            => BFLastStation,    -- in std_logic;
            probe0(110 downto 106) => BFTimeStep,       -- in (4:0);  -- Timestep, runs from 0 to 31. There are 32 timesteps per output packet.
            probe0(115 downto 111) => BFvirtualChannel(4 downto 0), -- in (9:0);  -- virtual channel
            probe0(118 downto 116) => BFPacketCount(2 downto 0),    -- in (36:0); -- The packet count for this packet, based on the original packet count from LFAA.
            probe0(119)            => BFValidIn
        );
        
        process(i_MACE_clk)
        begin
            if rising_edge(i_MACE_clk) then
                m02_axi_awvalid_dbg  <= m02_axi_awvalid; --  out std_logic;
                m02_axi_awready_dbg  <= m02_axi_awready; -- in std_logic;
                m02_axi_awaddr_dbg   <= m02_axi_awaddr;  -- out std_logic_vector(29 downto 0);
                m02_axi_awlen_dbg    <= m02_axi_awlen(3 downto 0);   -- out std_logic_vector(7 downto 0);
                -- w bus - write data
                m02_axi_wvalid_dbg   <= m02_axi_wvalid; -- out std_logic;
                m02_axi_wready_dbg   <= m02_axi_wready; -- in std_logic;
                m02_axi_wdata_dbg    <= m02_axi_wdata;  -- out std_logic_vector(511 downto 0);
                m02_axi_wlast_dbg    <= m02_axi_wlast;  -- out std_logic;
                -- b bus - write response
                m02_axi_bvalid_dbg   <= m02_axi_bvalid; -- in std_logic;
                m02_axi_bresp_dbg    <= m02_axi_bresp;  -- in std_logic_vector(1 downto 0);
                -- ar bus - read address
                m02_axi_arvalid_dbg  <= m02_axi_arvalid; -- out std_logic;
                m02_axi_arready_dbg  <= m02_axi_arready; -- in std_logic;
                m02_axi_araddr_dbg   <= m02_axi_araddr;  -- out std_logic_vector(29 downto 0);
                m02_axi_arlen_dbg    <= m02_axi_arlen(3 downto 0);   -- out std_logic_vector(7 downto 0);
                -- r bus - read data
                m02_axi_rvalid_dbg   <= m02_axi_rvalid; -- in std_logic;
                m02_axi_rready_dbg   <= m02_axi_rready; -- out std_logic;
                m02_axi_rdata_dbg    <= m02_axi_rdata;  -- in std_logic_vector(511 downto 0);
                m02_axi_rlast_dbg    <= m02_axi_rlast;  -- in std_logic;
                m02_axi_rresp_dbg    <= m02_axi_rresp;   -- in std_logic_vector(1 downto 0)
                
                dataMismatch_dbg <= dataMismatch;
            end if;
        end process;
        
        
        u_m02345_ila : ila_beamData
        port map (
            clk => i_MACE_clk,
            probe0(0)             => m02_axi_awvalid_dbg,
            probe0(1)             => m02_axi_awready_dbg,
            probe0(29 downto 2)   => m02_axi_awaddr_dbg(29 downto 2),
            probe0(31 downto 30)  => "00",
            probe0(32)            => m02_axi_wvalid_dbg,
            probe0(33)            => m02_axi_wready_dbg,
            probe0(34)            => m02_axi_wlast_dbg,
            probe0(35)            => m02_axi_arvalid_dbg,
            probe0(36)            => m02_axi_arready_dbg,
            probe0(62 downto 37)  => m02_axi_araddr_dbg(29 downto 4),
            probe0(63)            => m02_axi_rvalid_dbg,
            probe0(64)            => m02_axi_rready_dbg,
            probe0(65)            => m02_axi_rlast_dbg,
            probe0(117 downto 66) => (others => '0'),            
            probe0(118) => dataMismatch_dbg,
            probe0(119) => '0'
        );
        
        
    END GENERATE;
    
    
END structure;
